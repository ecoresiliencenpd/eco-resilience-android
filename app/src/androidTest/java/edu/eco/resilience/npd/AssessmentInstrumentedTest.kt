package edu.eco.resilience.npd

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.components.DaggerApplicationComponent
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.useCases.GetDetailApplicationAssessmentUseCase
import edu.eco.resilience.npd.domain.useCases.GetHistoryAssessmentUseCase
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentModule
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class AssessmentInstrumentedTest {

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(
                ApplicationModule(
                    InstrumentationRegistry.getTargetContext(),
                    SharedPreferencesManager.FILE_USER_DATA
                )
            )
            .netModule(NetModule("http://209.159.146.43:8080/"))
            .databaseModule(DatabaseModule("eco_db"))
            .build()
    }

    private val assessmentComponent: AssessmentTestComponent by lazy {
        DaggerAssessmentInstrumentedTest_AssessmentTestComponent.builder()
            .applicationComponent(this.applicationComponent)
            .assessmentModule(AssessmentModule())
            .build()
    }


    @Inject
    lateinit var getHistoryAssessment: GetHistoryAssessmentUseCase

    @Inject
    lateinit var getDetailApplicationAssessmentUseCase: GetDetailApplicationAssessmentUseCase

    @Before
    fun injectDependencies() {
        this.assessmentComponent.inject(this)
    }

    @Test
    fun getHistoryAssessment() {
        this.getHistoryAssessment.createObservable("7")
            .test().assertNoErrors()
            .assertValue { t: List<HistoryAssessment> ->
                t[0].assessment.description.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getDetailApplicationAssessment() {
        this.getDetailApplicationAssessmentUseCase.createObservable("2")
            .test().assertNoErrors()
            .assertValue { t: DetailApplicationAssessment ->
                t.questions.isNotEmpty()
            }
            .assertComplete()
    }


    @MainScope
    @Component(dependencies = [ApplicationComponent::class], modules = [AssessmentModule::class])
    interface AssessmentTestComponent {

        fun inject(assessmentInstrumentedTest: AssessmentInstrumentedTest)
    }
}