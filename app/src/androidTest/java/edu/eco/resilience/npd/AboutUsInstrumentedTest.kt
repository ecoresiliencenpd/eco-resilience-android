package edu.eco.resilience.npd

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.components.DaggerApplicationComponent
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.useCases.GetAppDetailUseCase
import edu.eco.resilience.npd.presentation.ui.aboutus.di.AboutUsModule
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class AboutUsInstrumentedTest {

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(
                ApplicationModule(
                    InstrumentationRegistry.getTargetContext(),
                    SharedPreferencesManager.FILE_USER_DATA
                )
            )
            .netModule(NetModule("http://209.159.146.43:8080/"))
            .databaseModule(DatabaseModule("eco_db"))
            .build()
    }

    private val aboutUsTestComponent: AboutUsTestComponent by lazy {
        DaggerAboutUsInstrumentedTest_AboutUsTestComponent.builder()
            .applicationComponent(this.applicationComponent)
            .aboutUsModule(AboutUsModule())
            .build()
    }

    @Inject
    lateinit var getAppDetailUseCase: GetAppDetailUseCase

    @Before
    fun injectDependencies() {
        this.aboutUsTestComponent.inject(this)
    }

    @Test
    fun getAppDetail() {
        getAppDetailUseCase.createObservable("")
            .test().assertNoErrors()
            .assertValue { appDetail ->
                appDetail.appPolicyLink.isNotEmpty()
            }
            .assertComplete()
    }

    @MainScope
    @Component(dependencies = [ApplicationComponent::class], modules = [AboutUsModule::class])
    interface AboutUsTestComponent {

        fun inject(aboutUsInstrumentedTest: AboutUsInstrumentedTest)
    }
}