package edu.eco.resilience.npd

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.components.DaggerApplicationComponent
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.User
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.UserProfile
import edu.eco.resilience.npd.domain.useCases.*
import edu.eco.resilience.npd.presentation.ui.login.di.LoginModule
import edu.eco.resilience.npd.presentation.ui.signup.di.SignUpModule
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class SignUpInstrumentedTest {

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(
                ApplicationModule(
                    InstrumentationRegistry.getTargetContext(),
                    SharedPreferencesManager.FILE_USER_DATA
                )
            )
            .netModule(NetModule("http://209.159.146.43:8080/WSEcoResilenceNPD_V2/"))
            .databaseModule(DatabaseModule("eco_db"))
            .build()
    }

    private val signUpTestComponent: SignUpTestComponent by lazy {
        DaggerSignUpInstrumentedTest_SignUpTestComponent.builder()
            .applicationComponent(this.applicationComponent)
            .signUpModule(SignUpModule())
            .build()
    }

    @Inject
    lateinit var statesUseCase: GetStatesUseCase

    @Inject
    lateinit var districtsUseCase: GetDistrictUseCase

    @Inject
    lateinit var schoolsUseCase: GetSchoolsUseCase

    @Inject
    lateinit var signUpUseCase: SignUpUseCase

    @Inject
    lateinit var educationLevelsUseCase: GetEducationLevelsUseCase

    @Before
    fun injectDependencies() {
        this.signUpTestComponent.inject(this)
    }

    @Test
    fun getStatesTest() {
        this.statesUseCase.createObservable("") //params null
            .test()
            .assertNoErrors()
            .assertValue { states ->
                states.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getDistricts() {
        this.districtsUseCase.createObservable("j") //id state
            .test()
            .assertNoErrors()
            .assertValue { districts ->
                districts.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getSchools() {
        this.schoolsUseCase.createObservable(GetSchoolsRequest("1", "1", "3"))
            .test()
            .assertNoErrors()
            .assertValue { schools ->
                schools.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getEducationLevels() {
        this.educationLevelsUseCase.createObservable("")
            .test()
            .assertNoErrors()
            .assertValue { educationLevels ->
                educationLevels.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun signUp() {
        this.signUpUseCase.execute(
            SignUpRequest(
                UserProfile(1, 1, 1),
                User("Jhon", "Doe", "string", 1, 1, 1, "user3@mail.com", "user_password", 4)
            )
        )
            .test()
            .assertNoErrors()
            .assertValue { success ->
                success.statusCode == 200
            }
            .assertComplete()
    }

    @MainScope
    @Component(dependencies = [ApplicationComponent::class], modules = [SignUpModule::class])
    interface SignUpTestComponent {

        fun inject(loginInstrumentedTest: SignUpInstrumentedTest)
    }
}