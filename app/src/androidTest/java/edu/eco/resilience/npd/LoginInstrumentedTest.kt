package edu.eco.resilience.npd

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.components.DaggerApplicationComponent
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.useCases.GetSectionUseCase
import edu.eco.resilience.npd.domain.useCases.GetSectionsUseCase
import edu.eco.resilience.npd.domain.useCases.LoginUseCase
import edu.eco.resilience.npd.domain.useCases.GetUserUseCase
import edu.eco.resilience.npd.presentation.ui.login.di.LoginModule
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class LoginInstrumentedTest {

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(
                ApplicationModule(
                    InstrumentationRegistry.getTargetContext(),
                    SharedPreferencesManager.FILE_USER_DATA
                )
            )
            .netModule(NetModule("http://209.159.146.43:8080/"))
            .databaseModule(DatabaseModule("eco_db"))
            .build()
    }

    private val loginTestComponent: LoginTestComponent by lazy {
        DaggerLoginInstrumentedTest_LoginTestComponent.builder()
            .applicationComponent(this.applicationComponent)
            .loginModule(LoginModule())
            .build()
    }

    @Inject
    lateinit var loginUsecases: LoginUseCase

    @Inject
    lateinit var userUseCase: GetUserUseCase

    @Inject
    lateinit var sectionsUseCase: GetSectionsUseCase

    @Inject
    lateinit var sectionUseCase: GetSectionUseCase

    @Before
    fun injectDependencies() {
        this.loginTestComponent.inject(this)
    }

    @Test
    fun loginTest() {
        val loginReques = LoginRequest("luckytics", "luckytics@gmail.com")
        loginUsecases.createObservable(loginReques)
            .test()
            .assertNoErrors()
            .assertValue { login ->
                login.user.firstName.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getUser() {
        userUseCase.createObservable("")
            .test()
            .assertNoErrors()
            .assertValue { user ->
                user.firstName.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getSections() {
        sectionsUseCase.createObservable("")
            .test()
            .assertValue { t: List<Section> ->
                t[0].name.isNotEmpty()
            }
            .assertComplete()
    }

    @Test
    fun getSection() {
        sectionUseCase.createObservable(2)
            .test()
            .assertValue { t: Section ->
                t.name.isNotEmpty()
            }
            .assertComplete()
    }

    @MainScope
    @Component(dependencies = [ApplicationComponent::class], modules = [LoginModule::class])
    interface LoginTestComponent {

        fun inject(loginInstrumentedTest: LoginInstrumentedTest)
    }
}