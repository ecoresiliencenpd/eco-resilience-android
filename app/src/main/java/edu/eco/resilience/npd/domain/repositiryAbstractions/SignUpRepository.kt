package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import io.reactivex.Observable

interface SignUpRepository {

    fun getStates(): Observable<List<StateResponse>>

    fun getDistricts(idState: String): Observable<List<DistrictsResponse>>

    fun getSchools(getSchoolsRequest: GetSchoolsRequest): Observable<List<SchoolsResponse>>

    fun signUp(signUpRequest: SignUpRequest): Observable<SignUpResponse>

    fun getEducationLevels(): Observable<List<EducationLevelsResponse>>

}