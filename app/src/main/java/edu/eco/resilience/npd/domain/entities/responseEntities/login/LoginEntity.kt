package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class LoginEntity(

    @field:SerializedName("userDetail")
    val userDetail: UserDetail? = null,

    @field:SerializedName("catalog")
    val catalog: Catalog? = null
)