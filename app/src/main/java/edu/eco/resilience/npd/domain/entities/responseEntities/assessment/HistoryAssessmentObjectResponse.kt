package edu.eco.resilience.npd.domain.entities.responseEntities.assessment

import com.google.gson.annotations.SerializedName

data class HistoryAssessmentObjectResponse(

    @field:SerializedName("assessment")
    val assessment: Assessment? = null,

    @field:SerializedName("pdf")
    val pdf: Pdf? = null,

    @field:SerializedName("application")
    val application: Application? = null
)


data class Assessment(

    @field:SerializedName("days_of_life")
    val daysOfLife: Int? = null,

    @field:SerializedName("pass_score")
    val passScore: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("active")
    val active: Boolean? = null,

    @field:SerializedName("id_language")
    val idLanguage: Int? = null,

    @field:SerializedName("id_parent")
    val idParent: Int? = null,

    @field:SerializedName("multiple_applications")
    val multipleApplications: Int? = null,

    @field:SerializedName("repeatable")
    val repeatable: Int? = null,

    @field:SerializedName("last_update")
    val lastUpdate: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("publication_date")
    val publicationDate: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_profile")
    val idProfile: Int? = null
)

data class Pdf(

    @field:SerializedName("id_assessment")
    val idAssessment: Int? = null,

    @field:SerializedName("end_range")
    val endRange: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("start_range")
    val startRange: Int? = null,

    @field:SerializedName("pdfName")
    val pdfName: String? = null,

    @field:SerializedName("url")
    val url: String? = null
)

data class Application(

    @field:SerializedName("score")
    val score: Float? = null,

    @field:SerializedName("id_pdf")
    val idPdf: Int? = null,

    @field:SerializedName("id_assessment")
    val idAssessment: Int? = null,

    @field:SerializedName("id_application")
    val idApplication: Int? = null,

    @field:SerializedName("id_user")
    val idUser: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null
)