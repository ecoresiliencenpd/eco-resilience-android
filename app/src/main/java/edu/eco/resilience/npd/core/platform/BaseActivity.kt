/**
 * Copyright (C) 2018 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.eco.resilience.npd.core.platform

import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import edu.eco.resilience.npd.AndroidApplication
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.data.SharedPreferencesManager
import java.util.*

/**
 * Base Activity class with helper methods for handling fragment transactions and back button
 * events.
 *
 * @see AppCompatActivity
 */
abstract class BaseActivity : AppCompatActivity() {

    val applicationComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as AndroidApplication).appComponent
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        val languageToLoad = if ((applicationContext as AndroidApplication).sharedPreferencesManager.getInt(
                SharedPreferencesManager.LENGUAGE,
                1
            ) == 1
        ) "en" else "es" // your language
        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
        super.onCreate(savedInstanceState)
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId())
        }
        initView(savedInstanceState)
    }

    abstract fun getLayoutResId(): Int

    open fun initView(savedInstanceState: Bundle?) {

    }
}
