package edu.eco.resilience.npd.domain.dataSourceAbstractions

import edu.eco.resilience.npd.domain.models.AppDetail
import io.reactivex.Observable

interface AboutUsDataSource {

    fun getAppDetail(): Observable<AppDetail>
}