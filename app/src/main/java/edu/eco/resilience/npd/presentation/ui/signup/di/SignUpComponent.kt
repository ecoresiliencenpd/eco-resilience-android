package edu.eco.resilience.npd.presentation.ui.signup.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.signup.SignUpActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [SignUpModule::class])
interface SignUpComponent {

    fun inject(signUpActivity: SignUpActivity)
}