package edu.eco.resilience.npd.presentation.utils

interface Section<out I : Section.Item> {

    fun getHeaderViewType(): Int

    fun getItems(): List<I>

    interface Item {

        fun getItemViewType(): Int
    }
}