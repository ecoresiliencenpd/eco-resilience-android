package edu.eco.resilience.npd.presentation.ui.login.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.core.platform.SingleLiveEvent
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SaveDevice
import edu.eco.resilience.npd.domain.useCases.LoginUseCase
import edu.eco.resilience.npd.domain.useCases.SaveDevicesUseCase

class LoginViewModel(
    private val loginUseCase: LoginUseCase,
    private val saveDevicesUseCase: SaveDevicesUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<LoginViewState> = MutableLiveData()

    var errorState: SingleLiveEvent<String> = SingleLiveEvent()

    fun login(email: String, password: String) {
        this.viewState.value = LoginViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            loginUseCase.execute(LoginRequest(password, email))
                .subscribe(::handleLogin, ::handleError)
        )
    }

    fun saveDevices(saveDeviceRequest: SaveDeviceRequest) {
        this.viewState.value = LoginViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            saveDevicesUseCase.execute(saveDeviceRequest)
                .subscribe(::handleSaveDevice, ::handleError)
        )
    }

    private fun handleLogin(login: Login) {
        this.viewState.value = LoginViewState.LoginState(login)
        this.viewState.value = LoginViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleSaveDevice(saveDevice: SaveDevice) {
        this.viewState.value = LoginViewState.SaveDeviceState(saveDevice)
        this.viewState.value = LoginViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleError(it: Throwable) {
        this.errorState.value = it.message
        this.viewState.value = LoginViewState.ProgressIndicator(View.INVISIBLE)
    }
}