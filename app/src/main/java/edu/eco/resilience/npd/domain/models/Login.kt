package edu.eco.resilience.npd.domain.models

data class Login(
    val user: UserDetail,
    val section: List<Section>
)

data class UserDetail(
    val idSchool: Int,
    val idEducationLevel: Int,
    val mail: String,
    val profile: String,
    val lastName: String,
    val idLanguage: Int,
    val idDistrict: Int,
    val language: String,
    val idUser: Int,
    val creationDate: String,
    val middleName: String,
    val school: String,
    val district: String,
    val name: String,
    val educationLevel: String,
    val state: String,
    val idState: Int,
    val firstName: String,
    val idProfile: Int
)
