package edu.eco.resilience.npd.presentation.ui.sections

import edu.eco.resilience.npd.domain.models.Theme
import edu.eco.resilience.npd.presentation.utils.Section

class SectionModel(val module: String, val themes: List<Theme> = emptyList()) : Section<Theme> {

    override fun getHeaderViewType(): Int = SectionAdapter.TYPE_HEADER

    override fun getItems(): List<Theme> = this.themes
}