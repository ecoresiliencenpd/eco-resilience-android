/**
 * Copyright (C) 2018 Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.eco.resilience.npd

import android.app.Application
import android.content.Context
import com.squareup.leakcanary.LeakCanary
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.components.DaggerApplicationComponent
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.data.SharedPreferencesManager.Companion.FILE_USER_DATA

class AndroidApplication : Application() {

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
            .builder()
            .netModule(NetModule("http://209.159.146.43:8080/"))
            .applicationModule(ApplicationModule(this, FILE_USER_DATA))
            .databaseModule(DatabaseModule("eco_db"))
            .build()
    }

    val sharedPreferencesManager: SharedPreferencesManager by lazy {
        SharedPreferencesManager(
            applicationContext.getSharedPreferences(FILE_USER_DATA, Context.MODE_PRIVATE)
        )
    }

    override fun onCreate() {
        super.onCreate()
        this.initializeLeakDetection()
    }

    private fun initializeLeakDetection() {
        if (BuildConfig.DEBUG) LeakCanary.install(this)
    }
}
