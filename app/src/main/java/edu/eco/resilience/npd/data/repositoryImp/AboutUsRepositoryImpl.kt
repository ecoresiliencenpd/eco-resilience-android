package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.AboutUsDataSource
import edu.eco.resilience.npd.domain.models.AppDetail
import edu.eco.resilience.npd.domain.repositiryAbstractions.AboutUsRepository
import io.reactivex.Observable
import javax.inject.Inject

class AboutUsRepositoryImpl
@Inject constructor(private val aboutUsDataSource: AboutUsDataSource) :
    AboutUsRepository {

    override fun getAppDetail(): Observable<AppDetail> = this.aboutUsDataSource.getAppDetail()

}