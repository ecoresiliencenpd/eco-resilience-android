package edu.eco.resilience.npd.domain.entities.databaseEntities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey @ColumnInfo(name = "id_user") val idUser: Int,
    @ColumnInfo(name = "mail") val mail: String,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @ColumnInfo(name = "middle_name") val middleName: String,
    @ColumnInfo(name = "id_education_level") val idEducationLevel: Int,
    @ColumnInfo(name = "education_level") val educationLevel: String,
    @ColumnInfo(name = "id_language") val idLanguage: Int,
    @ColumnInfo(name = "language") val language: String,
    @ColumnInfo(name = "id_profile") val idProfile: Int,
    @ColumnInfo(name = "profile") val profile: String,
    @ColumnInfo(name = "id_state") val idState: Int,
    @ColumnInfo(name = "state") val state: String,
    @ColumnInfo(name = "id_district") val idDistrict: Int,
    @ColumnInfo(name = "district") val district: String,
    @ColumnInfo(name = "id_school") val idSchool: Int,
    @ColumnInfo(name = "school") val school: String,
    @ColumnInfo(name = "creation_date") val creationDate: String
)