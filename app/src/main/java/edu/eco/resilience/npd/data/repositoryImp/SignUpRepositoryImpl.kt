package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.SignUpDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import io.reactivex.Observable
import javax.inject.Inject

class SignUpRepositoryImpl
@Inject constructor(private val signUpDataSource: SignUpDataSource) : SignUpRepository {

    override fun getStates(): Observable<List<StateResponse>> =
        this.signUpDataSource.getStates()


    override fun getDistricts(idState: String): Observable<List<DistrictsResponse>> =
        this.signUpDataSource.getDistricts(idState)

    override fun getSchools(
        getSchoolsRequest: GetSchoolsRequest
    ): Observable<List<SchoolsResponse>> = this.signUpDataSource.getSchools(
        getSchoolsRequest.idState,
        getSchoolsRequest.idDistrict,
        getSchoolsRequest.idEducationLevel
    )

    override fun signUp(signUpRequest: SignUpRequest): Observable<SignUpResponse> =
        this.signUpDataSource.signUp(signUpRequest)

    override fun getEducationLevels(): Observable<List<EducationLevelsResponse>> =
        this.signUpDataSource.getEducationLevels()

}