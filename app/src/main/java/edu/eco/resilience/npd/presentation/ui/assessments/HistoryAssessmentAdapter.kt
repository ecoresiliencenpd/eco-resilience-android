package edu.eco.resilience.npd.presentation.ui.assessments

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.presentation.utils.getDateString
import edu.eco.resilience.npd.presentation.utils.parseDate
import kotlinx.android.synthetic.main.item_history_assessment.view.*
import java.util.*
import kotlin.properties.Delegates

class HistoryAssessmentAdapter : RecyclerView.Adapter<HistoryAssessmentAdapter.ViewHolder>() {

    internal var list: List<HistoryAssessment> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListenerDetailAssessment: (HistoryAssessment) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_history_assessment, parent, false
            )
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], clickListenerDetailAssessment)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItems(
            historyAssessment: HistoryAssessment,
            clickListenerDetailAssessment: (HistoryAssessment) -> Unit
        ) = with(itemView) {
            name.text = historyAssessment.assessment.name
            rating.rating = historyAssessment.application.score.toFloat()


            var calendar = Calendar.getInstance()
            calendar.time = parseDate(historyAssessment.assessment.publicationDate, "mm/dd/yyyy")
            dateStart.text = String.format(context.getString(R.string.history_start_on, getDateString(calendar.time)))
            calendar.set(Calendar.DATE, +historyAssessment.assessment.daysOfLife)
            dateEnd.text = String.format(context.getString(R.string.history_finishe_on, getDateString(calendar.time)))

            viewDetail.text =
                if (historyAssessment.assessment.new) context.getString(R.string.history_assessment_text_tap_here_to_begin) else context.getString(
                    R.string.assessment_detail_text_view_detail
                )
            if (historyAssessment.assessment.new) {
                rating.visibility = GONE
                dateStart.visibility = View.VISIBLE
                score.text = String.format(
                    context.getString(
                        R.string.history_finishe_in_days,
                        historyAssessment.assessment.daysOfLife.toString()
                    )
                )
            } else {
                score.text = "Score: ${historyAssessment.application.score}"
                dateStart.visibility = View.GONE
            }
            viewDetail.setOnClickListener { clickListenerDetailAssessment(historyAssessment) }
            label.visibility = if (historyAssessment.assessment.new) View.VISIBLE else View.GONE
        }
    }

}