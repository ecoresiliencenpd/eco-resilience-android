package edu.eco.resilience.npd.core.domain.executors

import java.util.concurrent.Executor

interface ThreadExecutor : Executor