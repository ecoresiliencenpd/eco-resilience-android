package edu.eco.resilience.npd.data.dataSourceImp.api

import edu.eco.resilience.npd.data.service.ApiResponseHandler
import edu.eco.resilience.npd.data.service.api.LoginService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.LoginDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.login.LoginEntity
import edu.eco.resilience.npd.domain.entities.responseEntities.login.SaveDeviceObjectResponse
import edu.eco.resilience.npd.domain.mappers.LoginMapper
import edu.eco.resilience.npd.domain.mappers.SaveDeviceMapper
import edu.eco.resilience.npd.domain.models.SaveDevice
import io.reactivex.Observable
import javax.inject.Inject

class LoginDataSourceImp
@Inject constructor(
    private val loginService: LoginService,
    private val loginMapper: LoginMapper,
    private val saveDeviceMapper: SaveDeviceMapper,
    private val apiResponseHandler: ApiResponseHandler
) : LoginDataSource {

    override fun saveDevices(request: SaveDeviceRequest, basic : String): Observable<SaveDevice> =
        this.loginService.saveDevices(request, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .map { t: SaveDeviceObjectResponse -> this.saveDeviceMapper.mapFromApi(t) }

    override fun login(loginRequest: LoginRequest) =
        this.loginService.login(loginRequest)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .map { t: LoginEntity -> this.loginMapper.mapFromApi(t) }

}