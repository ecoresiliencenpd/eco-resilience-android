package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.login.LoginEntity
import edu.eco.resilience.npd.domain.entities.responseEntities.login.SaveDeviceObjectResponse
import edu.eco.resilience.npd.domain.models.*
import javax.inject.Inject

class SaveDeviceMapper
@Inject constructor() : BaseMapper<SaveDevice, SaveDeviceObjectResponse> {
    override fun mapFromApi(responseObject: SaveDeviceObjectResponse): SaveDevice =
        SaveDevice(
            responseObject.message ?: "",
            responseObject.statusCode ?: 0
        )

}