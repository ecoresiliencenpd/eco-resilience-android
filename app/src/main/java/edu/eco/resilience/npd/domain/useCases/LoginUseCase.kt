package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.repositiryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginUseCase
@Inject constructor(
    private val loginRepository: LoginRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<Login, LoginRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: LoginRequest): Observable<Login> =
            this.loginRepository.login(params)


}