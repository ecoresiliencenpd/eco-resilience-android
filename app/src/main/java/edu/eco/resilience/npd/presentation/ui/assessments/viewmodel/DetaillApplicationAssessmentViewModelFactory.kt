package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.EvaluationUseCase
import edu.eco.resilience.npd.domain.useCases.GetDetailApplicationAssessmentUseCase
import edu.eco.resilience.npd.domain.useCases.GetNewestAssessmentDetailUseCase
import javax.inject.Inject

class DetaillApplicationAssessmentViewModelFactory
@Inject constructor(
    private val getDetailApplicationAssessmentUseCase: GetDetailApplicationAssessmentUseCase,
    private val getNewestAssessmentDetailUseCase: GetNewestAssessmentDetailUseCase,
    private val evaluationUseCase: EvaluationUseCase
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(
            GetDetailApplicationAssessmentUseCase::class.java,
            GetNewestAssessmentDetailUseCase::class.java,
            EvaluationUseCase::class.java
        ).newInstance(getDetailApplicationAssessmentUseCase, getNewestAssessmentDetailUseCase, evaluationUseCase)
    }

}