package edu.eco.resilience.npd.presentation.ui.aboutus

data class AboutUs(
    val item: String,
    val link: String
)