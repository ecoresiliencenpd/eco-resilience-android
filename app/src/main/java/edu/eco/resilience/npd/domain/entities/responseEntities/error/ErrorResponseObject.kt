package edu.eco.resilience.npd.domain.entities.responseEntities.error

import com.google.gson.annotations.SerializedName

data class ErrorResponseObject(
    @SerializedName("statusCode")
    val statusCode: Int,
    @SerializedName("message")
    val message: String
)