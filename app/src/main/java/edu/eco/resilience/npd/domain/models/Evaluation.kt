package edu.eco.resilience.npd.domain.models

data class Evaluation(
    val message: String,
    val statusCode: Int
)