package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class AnswersItem(

    @SerializedName("id_answer")
    val _idAnswer: Int? = null,

    @SerializedName("last_update")
    val _lastUpdate: String? = null,

    @SerializedName("name")
    val _name: String? = null,

    @SerializedName("url_doc")
    val _urlDoc: String? = null,

    @SerializedName("url_image")
    val _urlImage: String? = null

) {
    val idAnswer: Int
        get() = this._idAnswer ?: 0

    val lastUpdate: String
        get() = this._lastUpdate ?: ""

    val name: String
        get() = this._name ?: ""

    val urlDoc: String
        get() = this._urlDoc ?: ""

    val urlImage: String
        get() = this._urlImage ?: ""
}