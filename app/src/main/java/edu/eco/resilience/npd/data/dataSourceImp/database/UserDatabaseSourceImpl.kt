package edu.eco.resilience.npd.data.dataSourceImp.database

import edu.eco.resilience.npd.data.service.daos.UserDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.entities.databaseEntities.UserEntity
import edu.eco.resilience.npd.domain.mappers.UserDatabaseMapper
import edu.eco.resilience.npd.domain.mappers.UserMapper
import edu.eco.resilience.npd.domain.models.UserDetail
import io.reactivex.Observable
import javax.inject.Inject

class UserDatabaseSourceImpl
@Inject constructor(
    private val userDao: UserDao,
    private val userMapper: UserMapper,
    private val userDatabaseMapper: UserDatabaseMapper
) : UserDataSource {

    override fun insertUser(user: UserDetail) {
        this.userDao.insertUser(this.userMapper.mapFromApi(user))
    }

    override fun deleteUsers() {
        this.userDao.deleteUsers()
    }

    override fun getUser(): Observable<UserDetail> =
        this.userDao.getUser()
            .map { t: UserEntity -> this.userDatabaseMapper.mapFromApi(t) }
            .toObservable()

}