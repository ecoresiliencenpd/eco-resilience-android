package edu.eco.resilience.npd.data.service.api

import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.login.LoginEntity
import edu.eco.resilience.npd.domain.entities.responseEntities.login.SaveDeviceObjectResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    companion object {
        private const val LOGIN = "WSEcoResilenceNPD_V2/rest/npdProgram/user/login"
        private const val SAVE_DEVICES = "WSEcoResilenceNPD_V2/rest/npdProgram/user/save/device"
    }

    @POST(LOGIN)
    fun login(@Body request: LoginRequest): Observable<Response<LoginEntity>>

    @POST(SAVE_DEVICES)
    fun saveDevices(
        @Body request: SaveDeviceRequest,
        @Header("Authorization") basic: String
    ): Observable<Response<SaveDeviceObjectResponse>>


}