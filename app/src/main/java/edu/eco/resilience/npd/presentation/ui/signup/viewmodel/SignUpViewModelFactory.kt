package edu.eco.resilience.npd.presentation.ui.signup.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.GetDistrictUseCase
import edu.eco.resilience.npd.domain.useCases.GetSchoolsUseCase
import edu.eco.resilience.npd.domain.useCases.GetStatesUseCase
import edu.eco.resilience.npd.domain.useCases.SignUpUseCase
import javax.inject.Inject

class SignUpViewModelFactory
@Inject constructor(
    private val getStatesUseCase: GetStatesUseCase,
    private val getDistrictUseCase: GetDistrictUseCase,
    private val getSchoolsUseCase: GetSchoolsUseCase,
    private val signUpUseCase: SignUpUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SignUpViewModel(getStatesUseCase, getDistrictUseCase, getSchoolsUseCase, signUpUseCase) as T
    }
}