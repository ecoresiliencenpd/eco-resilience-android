package edu.eco.resilience.npd.presentation.ui.sections.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.GetSectionUseCase
import javax.inject.Inject

class SectionViewModelFactory
@Inject constructor(
    private val getSectionUseCase: GetSectionUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SectionViewModel(getSectionUseCase) as T
    }
}