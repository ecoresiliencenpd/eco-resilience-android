package edu.eco.resilience.npd.presentation.ui.login

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.hardware.usb.UsbDevice.getDeviceName
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceId.*
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.presentation.ui.home.HomeActivity
import edu.eco.resilience.npd.presentation.ui.login.di.DaggerLoginComponent
import edu.eco.resilience.npd.presentation.ui.login.di.LoginComponent
import edu.eco.resilience.npd.presentation.ui.login.di.LoginModule
import edu.eco.resilience.npd.presentation.ui.login.viewmodel.LoginViewModel
import edu.eco.resilience.npd.presentation.ui.login.viewmodel.LoginViewModelFactory
import edu.eco.resilience.npd.presentation.ui.login.viewmodel.LoginViewState
import edu.eco.resilience.npd.presentation.ui.signup.SignUpActivity
import edu.eco.resilience.npd.presentation.utils.encodeString
import edu.eco.resilience.npd.presentation.utils.getDeviceName
import edu.eco.resilience.npd.presentation.utils.getUUID
import formatToViewDateDefaults
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.design.snackbar
import java.util.*
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    private val REQUEST_SIGNUP = 0

    private var token: String? = null

    override fun getLayoutResId(): Int = R.layout.activity_login

    private val loginComponent: LoginComponent by lazy {
        DaggerLoginComponent.builder()
            .applicationComponent(applicationComponent)
            .loginModule(LoginModule())
            .build()
    }

    @Inject
    lateinit var factory: LoginViewModelFactory

    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, this.factory).get(LoginViewModel::class.java)
    }

    @Inject
    internal lateinit var sharedPreferencesManager: SharedPreferencesManager

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.window.setBackgroundDrawableResource(R.drawable.bg_signin)

        this.loginComponent.inject(this)

        this.initFirebase()
        this.observeViewState()

        this.email.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.toString().filterNot { it.isWhitespace() }
        })
        this.password.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.toString().filterNot { it.isWhitespace() }
        })

        this.email.setText(
            this.sharedPreferencesManager.getString(
                SharedPreferencesManager.USER_EMAIL,
                SharedPreferencesManager.DEFAULT
            )
        )

        this.password.setText(
            this.sharedPreferencesManager.getString(
                SharedPreferencesManager.USER_PASSWORD,
                SharedPreferencesManager.DEFAULT
            )
        )
        this.checkBox_savePassword.isChecked =
            this.sharedPreferencesManager.getBoolean(SharedPreferencesManager.USER_CHECK_SAVE_PASSOWRD, false)

        this.btnLogin.setOnClickListener {
            if (this.validLogin()) {
                this.viewModel.login(
                    this.email.text.toString(),
                    this.password.text.toString()
                )
                this.sharedPreferencesManager.saveString(
                    SharedPreferencesManager.USER_EMAIL,
                    this.email.text.toString()
                )
                this.sharedPreferencesManager.saveBoolean(
                    SharedPreferencesManager.USER_CHECK_SAVE_PASSOWRD,
                    this.checkBox_savePassword.isChecked
                )
                if (this.checkBox_savePassword.isChecked) {
                    this.sharedPreferencesManager.saveString(
                        SharedPreferencesManager.USER_PASSWORD,
                        this.password.text.toString()
                    )
                }
            }
        }

        this.btnSignup.setOnClickListener {
            this.startActivityForResult(Intent(this, SignUpActivity::class.java), REQUEST_SIGNUP)
        }


    }

    private fun initFirebase() {
        getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                token = task.result?.token

            })
    }


    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is LoginViewState.LoginState -> {
                    this.sharedPreferencesManager.saveString(
                        SharedPreferencesManager.USER_ID,
                        viewState.login.user.idUser.toString()
                    )
                    this.sharedPreferencesManager.saveString(
                        SharedPreferencesManager.TOKEN_BASIC,
                        encodeString(this.email.text.toString() + ":" + this.password.text.toString())
                    )
                    this.sharedPreferencesManager.saveInt(
                        SharedPreferencesManager.LENGUAGE,
                        viewState.login.user.idLanguage
                    )
                    this.viewModel.saveDevices(
                        SaveDeviceRequest(
                            Date().formatToViewDateDefaults(),
                            1,
                            token ?: "",
                            viewState.login.user.idUser,
                            getDeviceName(),
                            getUUID()
                        )
                    )
                }
                is LoginViewState.SaveDeviceState -> {
                    this.startActivity(Intent(this, HomeActivity::class.java))
                    this.finish()
                }
                is LoginViewState.ProgressIndicator -> {
                    this.loading.visibility = viewState.visibility

                }
            }
        })
        this.viewModel.errorState.observe(this, Observer { message ->
            message?.let {
                content.snackbar(it).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SIGNUP && resultCode == Activity.RESULT_OK) {
            this.viewModel.login(
                data?.getStringExtra("email") ?: "",
                data?.getStringExtra("password") ?: ""
            )
        }

    }

    private fun validLogin(): Boolean {
        var valid = true
        if (this.email.text.toString().isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(this.email.text.toString()).matches()) {
            this.textEmail.error = getString(R.string.error_invalid_email)
            valid = false
        } else {
            this.textEmail.error = null
            this.textEmail.isErrorEnabled = false
        }
        if (this.password.text.toString().isEmpty() || this.password.text.toString().length < 4 || this.password.text.toString().length > 10) {
            this.textPassword.error = getString(R.string.error_password_length)
            valid = false
        } else {
            this.textPassword.error = null
            this.textPassword.isErrorEnabled = false

        }
        return valid
    }

}
