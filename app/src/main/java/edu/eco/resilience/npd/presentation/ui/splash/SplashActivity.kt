package edu.eco.resilience.npd.presentation.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.presentation.ui.home.HomeActivity
import edu.eco.resilience.npd.presentation.ui.login.LoginActivity
import edu.eco.resilience.npd.presentation.ui.splash.di.DaggerSplashComponent
import edu.eco.resilience.npd.presentation.ui.splash.di.SplashComponent
import javax.inject.Inject

class SplashActivity : BaseActivity() {


    private val splashComponent: SplashComponent by lazy {
        DaggerSplashComponent.builder()
            .applicationComponent(applicationComponent)
            .build()
    }


    private var delayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds

    @Inject
    internal lateinit var sharedPreferencesManager: SharedPreferencesManager

    private val runnable: Runnable = Runnable {
        if (!isFinishing) {
            if (this.sharedPreferencesManager.isUserLogged == true) {
                this.startActivity(Intent(this, HomeActivity::class.java))
                this.finish()
            } else {
                val intent = Intent(applicationContext, LoginActivity::class.java)
                startActivity(intent)
                this.finish()
            }
        }
    }

    override fun getLayoutResId(): Int = 0

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.splashComponent.inject(this)

        //Initialize the Handler
        delayHandler = Handler()

        //Navigate with delay
        delayHandler!!.postDelayed(runnable, SPLASH_DELAY)

    }

    public override fun onDestroy() {

        if (delayHandler != null) {
            delayHandler!!.removeCallbacks(runnable)
        }

        super.onDestroy()
    }

}
