package edu.eco.resilience.npd.presentation.ui.myprofile.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import edu.eco.resilience.npd.data.dataSourceImp.database.UserDatabaseSourceImpl
import edu.eco.resilience.npd.data.repositoryImp.UserRepositoryImpl
import edu.eco.resilience.npd.data.service.daos.UserDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.UserRepository

@Module
class MyProfileModule {

    @Provides
    @MainScope
    fun provideUserDao(ecoResilienceDataBase: EcoResilienceDataBase): UserDao = ecoResilienceDataBase.getUserDao()

    @Provides
    @MainScope
    fun provideUserDatabaseSource(userDatabaseSourceImpl: UserDatabaseSourceImpl): UserDataSource =
        userDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository = userRepositoryImpl

}