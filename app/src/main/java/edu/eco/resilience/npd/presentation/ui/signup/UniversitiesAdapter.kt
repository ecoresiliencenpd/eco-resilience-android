package edu.eco.resilience.npd.presentation.ui.signup

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import kotlinx.android.synthetic.main.item_university.view.*

class UniversitiesAdapter(val universities: List<String>) : RecyclerView.Adapter<UniversitiesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_university, parent, false
        )
    )

    override fun getItemCount(): Int = universities.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(universities[position])
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: String) = with(itemView) {
            collegeName.text = item
        }
    }
}