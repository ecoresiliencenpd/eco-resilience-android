package edu.eco.resilience.npd.presentation.ui.aboutus.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.aboutus.AboutUsActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [AboutUsModule::class])
interface AboutUsComponent {

    fun inject(aboutUsActivity: AboutUsActivity)
}