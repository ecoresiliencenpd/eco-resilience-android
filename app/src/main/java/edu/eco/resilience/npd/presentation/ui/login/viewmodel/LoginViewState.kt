package edu.eco.resilience.npd.presentation.ui.login.viewmodel

import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SaveDevice

sealed class LoginViewState {
    data class LoginState(val login: Login) : LoginViewState()
    data class SaveDeviceState(val saveDevice: SaveDevice) : LoginViewState()
    data class ProgressIndicator(val visibility: Int) : LoginViewState()
}