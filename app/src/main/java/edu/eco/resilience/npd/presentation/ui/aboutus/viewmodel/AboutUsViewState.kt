package edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel

import edu.eco.resilience.npd.domain.models.AppDetail

sealed class AboutUsViewState {

    data class AppDetailState(val appdetail: AppDetail) : AboutUsViewState()
    data class ProgressIndicator(val visibility: Int) : AboutUsViewState()
}