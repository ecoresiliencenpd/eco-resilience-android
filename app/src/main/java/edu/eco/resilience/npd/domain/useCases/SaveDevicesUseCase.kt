package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SaveDevice
import edu.eco.resilience.npd.domain.repositiryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class SaveDevicesUseCase
@Inject constructor(
    private val loginRepository: LoginRepository,
    private val sharedPreferencesManager: SharedPreferencesManager,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<SaveDevice, SaveDeviceRequest>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: SaveDeviceRequest): Observable<SaveDevice> =
        this.loginRepository.saveDevices(
            params,
            sharedPreferencesManager.getString(SharedPreferencesManager.TOKEN_BASIC)
        )

}