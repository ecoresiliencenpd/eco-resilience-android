package edu.eco.resilience.npd.core.di.components

import android.content.Context
import dagger.Component
import edu.eco.resilience.npd.core.di.modules.ApplicationModule
import edu.eco.resilience.npd.core.di.modules.DatabaseModule
import edu.eco.resilience.npd.core.di.modules.NetModule
import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import edu.eco.resilience.npd.data.SharedPreferencesManager
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class, DatabaseModule::class])
interface ApplicationComponent {

    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun sharedPreferencesManager(): SharedPreferencesManager

    fun retrofit(): Retrofit

    fun ecoResilience(): EcoResilienceDataBase

}