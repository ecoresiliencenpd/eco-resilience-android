package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.HistoryAssessmentObjectResponse
import edu.eco.resilience.npd.domain.models.Application
import edu.eco.resilience.npd.domain.models.Assessment
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.models.Pdf
import javax.inject.Inject

class HistoryAssessmentMapper @Inject constructor() :
    BaseMapper<HistoryAssessment, HistoryAssessmentObjectResponse> {


    override fun mapFromApi(historyAssessmentObjectResponse: HistoryAssessmentObjectResponse): HistoryAssessment {
        val historyAssessment = HistoryAssessment(

            Assessment(
                historyAssessmentObjectResponse.assessment?.daysOfLife ?: 0,
                historyAssessmentObjectResponse.assessment?.passScore ?: "",
                historyAssessmentObjectResponse.assessment?.description ?: "",
                historyAssessmentObjectResponse.assessment?.active ?: false,
                historyAssessmentObjectResponse.assessment?.idLanguage ?: 0,
                historyAssessmentObjectResponse.assessment?.idParent ?: 0,
                historyAssessmentObjectResponse.assessment?.multipleApplications ?: 0,
                historyAssessmentObjectResponse.assessment?.repeatable ?: 0,
                historyAssessmentObjectResponse.assessment?.lastUpdate ?: "",
                historyAssessmentObjectResponse.assessment?.name ?: "",
                historyAssessmentObjectResponse.assessment?.publicationDate ?: "",
                historyAssessmentObjectResponse.assessment?.id ?: 0,
                historyAssessmentObjectResponse.assessment?.idProfile ?: 0,
                false
            ),
            Pdf(
                historyAssessmentObjectResponse.pdf?.idAssessment ?: 0,
                historyAssessmentObjectResponse.pdf?.endRange ?: 0,
                historyAssessmentObjectResponse.pdf?.id ?: 0,
                historyAssessmentObjectResponse.pdf?.startRange ?: 0,
                historyAssessmentObjectResponse.pdf?.pdfName ?: "",
                historyAssessmentObjectResponse.pdf?.url ?: ""
            ),
            Application(
                historyAssessmentObjectResponse.application?.score ?: 0f,
                historyAssessmentObjectResponse.application?.idPdf ?: 0,
                historyAssessmentObjectResponse.application?.idAssessment ?: 0,
                historyAssessmentObjectResponse.application?.idApplication ?: 0,
                historyAssessmentObjectResponse.application?.idUser ?: 0,
                historyAssessmentObjectResponse.application?.creationDate ?: 0L

            )

        )
        return historyAssessment
    }


}