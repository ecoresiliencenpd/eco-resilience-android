package edu.eco.resilience.npd.presentation.ui.questions

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import edu.eco.resilience.npd.domain.models.Answer
import edu.eco.resilience.npd.domain.models.Question

class QuestionVPAdapter(fm: FragmentManager, val answers: List<Answer>) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int = this.answers.size

    override fun getItem(p0: Int): Fragment = QuestionFragment.newInstance(answers[p0])
}