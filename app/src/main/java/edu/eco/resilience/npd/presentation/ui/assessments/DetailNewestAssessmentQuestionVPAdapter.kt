package edu.eco.resilience.npd.presentation.ui.assessments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.domain.models.QuestionsItem

class DetailNewestAssessmentQuestionVPAdapter(fm: FragmentManager, val question: List<NewestAssessmentDetail>) :
    FragmentPagerAdapter(fm) {

    override fun getCount(): Int = this.question.size

    override fun getItem(p0: Int): Fragment = DetailNewestAssessmentQuestionFragment.newInstance(question[p0], p0)
}