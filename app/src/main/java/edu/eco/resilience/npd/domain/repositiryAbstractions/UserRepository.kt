package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.models.UserDetail
import io.reactivex.Observable

interface UserRepository {
    fun getUser(): Observable<UserDetail>
}