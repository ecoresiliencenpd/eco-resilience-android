package edu.eco.resilience.npd.presentation.ui.assessments.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.assessments.AssessmentDetailStepsFragment
import edu.eco.resilience.npd.presentation.ui.assessments.AssessmentsActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [AssessmentModule::class])
interface AssessmentComponent {

    fun inject(assessmentsActivity: AssessmentsActivity)
    fun inject(assessmentDetailStepsFragment: AssessmentDetailStepsFragment)
}