package edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.core.platform.SingleLiveEvent
import edu.eco.resilience.npd.domain.models.AppDetail
import edu.eco.resilience.npd.domain.useCases.GetAppDetailUseCase

class AboutUsViewModel(
    private val getAppDetailUseCase: GetAppDetailUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<AboutUsViewState> = MutableLiveData()
    var errorState: SingleLiveEvent<Throwable?> = SingleLiveEvent()

    fun getAppDetail() {
        this.addDisposable(
            this.getAppDetailUseCase.execute("").subscribe(::handleGetAppDetail, ::handleError)
        )
    }

    private fun handleGetAppDetail(appDetail: AppDetail) {
        this.viewState.value = AboutUsViewState.AppDetailState(appDetail)
        this.viewState.value = AboutUsViewState.ProgressIndicator(View.VISIBLE)
    }

    private fun handleError(it: Throwable) {
        this.errorState.value = it
        this.viewState.value = AboutUsViewState.ProgressIndicator(View.INVISIBLE)
    }
}