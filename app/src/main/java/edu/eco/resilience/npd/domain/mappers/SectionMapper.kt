package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.databaseEntities.SectionEntity
import edu.eco.resilience.npd.domain.models.Section
import javax.inject.Inject

class SectionMapper @Inject constructor() : BaseMapper<List<SectionEntity>, List<Section>> {

    override fun mapFromApi(responseObject: List<Section>): List<SectionEntity> =

        responseObject.map { section ->
            this.mapFromData(section)
        }

    fun mapFromData(responseObject: Section): SectionEntity {
        val sectionEntity = SectionEntity(
            responseObject.idSection,
            responseObject.name
        )

        sectionEntity.modules = responseObject.modules.map { module ->
            val moduleEntity = SectionEntity.SectionModuleEntity(module.idModule, module.name)
            moduleEntity.themes = module.themes.map { theme ->
                val themeEntity = SectionEntity.SectionModuleEntity.SectionThemeEntity(theme.idTheme, theme.name)
                themeEntity.questions = theme.questions.map { question ->
                    val questionEntity = SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity(
                        question.idQuestion,
                        question.name
                    )
                    questionEntity.answers = question.answers.map { answer ->
                        val answerEntity =
                            SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity.SectionAnswerEntity(
                                answer.idAnswer,
                                answer.name, answer.lastUpdate, answer.urlImage, answer.ulrDoc
                            )
                        answerEntity
                    }
                    questionEntity
                }
                themeEntity
            }
            moduleEntity

        }

        return sectionEntity
    }

}