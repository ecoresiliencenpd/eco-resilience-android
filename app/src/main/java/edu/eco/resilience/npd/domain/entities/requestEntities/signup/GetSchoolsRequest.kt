package edu.eco.resilience.npd.domain.entities.requestEntities.signup

import retrofit2.http.Path

data class GetSchoolsRequest(
    val idState: String,
    val idDistrict: String,
    val idEducationLevel: String
)