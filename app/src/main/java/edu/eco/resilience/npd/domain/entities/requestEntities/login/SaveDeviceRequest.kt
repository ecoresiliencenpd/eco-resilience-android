package edu.eco.resilience.npd.domain.entities.requestEntities.login

import com.google.gson.annotations.SerializedName

data class SaveDeviceRequest(

	@field:SerializedName("last_update")
	val lastUpdate: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("regid")
	val regid: String? = null,

	@field:SerializedName("id_user")
	val idUser: Int? = null,

	@field:SerializedName("display_name")
	val displayName: String? = null,

	@field:SerializedName("uuid")
	val uuid: String? = null
)