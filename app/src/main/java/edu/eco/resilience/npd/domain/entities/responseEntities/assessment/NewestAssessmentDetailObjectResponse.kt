package edu.eco.resilience.npd.domain.entities.responseEntities.assessment

import com.google.gson.annotations.SerializedName
import edu.eco.resilience.npd.domain.entities.responseEntities.login.ThemesItem

data class NewestAssessmentDetailObjectResponse(

    @field:SerializedName("id_choice")
    val idChoice: Int? = null,

    @field:SerializedName("question")
    val question: String? = null,

    @field:SerializedName("id_assessment")
    val idAssessment: Int? = null,

    @field:SerializedName("question_type")
    val questionType: String? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("id_type")
    val idType: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null,

    @field:SerializedName("choices")
    val _choices: List<ChoicesItemNewestAsessmentDetailObjectResponse>? = null,

    @field:SerializedName("value")
    val value: Int? = null,

    @field:SerializedName("id_question")
    val idQuestion: Int? = null,

    @field:SerializedName("order")
    val order: Int? = null
) {
    val choices: List<ChoicesItemNewestAsessmentDetailObjectResponse>
        get() = this._choices ?: emptyList()
}

data class ChoicesItemNewestAsessmentDetailObjectResponse(

    @field:SerializedName("id_branch")
    val idBranch: Int? = null,

    @field:SerializedName("branches")
    val _branches: List<BranchesItem>? = null
){
    val branches: List<BranchesItem>
    get() = this._branches ?: emptyList()
}

data class BranchesItem(

    @field:SerializedName("id_choice")
    val idChoice: Int? = null,

    @field:SerializedName("branch_name")
    val branchName: String? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null,

    @field:SerializedName("choice")
    val choice: String? = null,

    @field:SerializedName("value")
    val value: Int? = null,

    @field:SerializedName("branch")
    val branch: Int? = null,

    @field:SerializedName("id_question")
    val idQuestion: Int? = null,

    @field:SerializedName("order")
    val order: Int? = null
)
