package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.LoginDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.models.SaveDevice
import edu.eco.resilience.npd.domain.repositiryAbstractions.LoginRepository
import io.reactivex.Observable
import javax.inject.Inject

class LoginRepositoryImpl
@Inject constructor(
    private val loginDataSource: LoginDataSource,
    private val userDataSource: UserDataSource,
    private val sectionDataSource: SectionDataSource
) :
    LoginRepository {
    override fun saveDevices(request: SaveDeviceRequest, basic: String): Observable<SaveDevice> =
        this.loginDataSource.saveDevices(request, basic)

    override fun login(loginRequest: LoginRequest) =
        this.loginDataSource.login(loginRequest).doOnNext { login ->
            this.userDataSource.deleteUsers()
            this.userDataSource.insertUser(login.user)

            this.sectionDataSource.deleteSections()
            this.sectionDataSource.insertSection(login.section)

        }

}