package edu.eco.resilience.npd.presentation.ui.myprofile

data class MyProfile(
    val labelProfile: String,
    val profile: String
)