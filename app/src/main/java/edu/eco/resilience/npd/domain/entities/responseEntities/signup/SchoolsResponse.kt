package edu.eco.resilience.npd.domain.entities.responseEntities.signup

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class SchoolsResponse(

	@field:SerializedName("id_education_level")
	val idEducationLevel: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id_district")
	val idDistrict: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("id_state")
	val idState: Int? = null
)