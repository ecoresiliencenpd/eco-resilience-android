package edu.eco.resilience.npd.presentation.ui.sections.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import edu.eco.resilience.npd.data.dataSourceImp.database.SectionDatabaseSourceImpl
import edu.eco.resilience.npd.data.repositoryImp.SectionRepositoryImpl
import edu.eco.resilience.npd.data.service.daos.SectionDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.SectionRepository

@Module
class SectionModule {

    @Provides
    @MainScope
    fun provideSectionDao(ecoResilienceDataBase: EcoResilienceDataBase): SectionDao =
        ecoResilienceDataBase.getSectionDao()

    @Provides
    @MainScope
    fun provideSectionDatabaseSource(sectionDatabaseSourceImpl: SectionDatabaseSourceImpl): SectionDataSource =
        sectionDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideSectionRepository(sectionRepositoryImpl: SectionRepositoryImpl): SectionRepository =
        sectionRepositoryImpl

}