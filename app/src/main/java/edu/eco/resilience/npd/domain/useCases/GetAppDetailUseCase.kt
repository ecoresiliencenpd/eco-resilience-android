package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.models.AppDetail
import edu.eco.resilience.npd.domain.repositiryAbstractions.AboutUsRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetAppDetailUseCase
@Inject constructor(
    private val aboutUsRepository: AboutUsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<AppDetail, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<AppDetail> =
        this.aboutUsRepository.getAppDetail()

}