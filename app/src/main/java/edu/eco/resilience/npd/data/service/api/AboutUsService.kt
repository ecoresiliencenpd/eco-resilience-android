package edu.eco.resilience.npd.data.service.api

import edu.eco.resilience.npd.domain.entities.responseEntities.aboutus.AppDetailResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface AboutUsService {

    companion object {
        private const val GET_APP_DETAIL = "/WSEcoResilenceNPD/rest/npdProgram/getAppDetail"
    }

    @GET(GET_APP_DETAIL)
    fun getAppDetail(): Observable<AppDetailResponse>

}