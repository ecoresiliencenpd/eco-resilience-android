package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.NewestAssessmentObjectResponse
import edu.eco.resilience.npd.domain.models.*
import javax.inject.Inject

class NewestAssessmentMapper @Inject constructor() :
    BaseMapper<NewestAssessment, NewestAssessmentObjectResponse> {

    override fun mapFromApi(responseObject: NewestAssessmentObjectResponse): NewestAssessment {
        val newestAssessment = NewestAssessment(
            responseObject.daysOfLife ?: 0,
            responseObject.passScore ?: "",
            responseObject.description ?: "",
            responseObject.active ?: false,
            responseObject.idLanguage ?: 0,
            responseObject.idParent ?: 0,
            responseObject.multipleApplications ?: 0,
            responseObject.repeatable ?: 0,
            responseObject.lastUpdate ?: "",
            responseObject.name ?: "",
            responseObject.publicationDate ?: "",
            responseObject.id ?: 0,
            responseObject.idProfile ?: 0
        )
        return newestAssessment
    }


}