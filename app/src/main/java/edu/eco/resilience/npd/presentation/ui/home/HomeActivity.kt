package edu.eco.resilience.npd.presentation.ui.home

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.extension.GridItemDecoration
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.presentation.ui.aboutus.AboutUsActivity
import edu.eco.resilience.npd.presentation.ui.assessments.AssessmentsActivity
import edu.eco.resilience.npd.presentation.ui.home.di.DaggerHomeComponent
import edu.eco.resilience.npd.presentation.ui.home.di.HomeComponent
import edu.eco.resilience.npd.presentation.ui.home.di.HomeModule
import edu.eco.resilience.npd.presentation.ui.home.viewmodel.HomeViewModel
import edu.eco.resilience.npd.presentation.ui.home.viewmodel.HomeViewModelFactory
import edu.eco.resilience.npd.presentation.ui.home.viewmodel.HomeViewState
import edu.eco.resilience.npd.presentation.ui.login.LoginActivity
import edu.eco.resilience.npd.presentation.ui.myprofile.MyProfileActivity
import edu.eco.resilience.npd.presentation.ui.sections.SectionsActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.okButton
import javax.inject.Inject


class HomeActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val homeComponent: HomeComponent by lazy {
        DaggerHomeComponent.builder()
            .applicationComponent(applicationComponent)
            .homeModule(HomeModule())
            .build()
    }

    @Inject
    internal lateinit var sharedPreferencesManager: SharedPreferencesManager

    @Inject
    lateinit var factory: HomeViewModelFactory

    private lateinit var viewModel: HomeViewModel

    private val adapter: HomeAdapter by lazy { HomeAdapter() }

    override fun getLayoutResId(): Int = R.layout.activity_home

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.toolbar.title = getString(R.string.title_activity_home)
        this.setSupportActionBar(toolbar)

        this.homeComponent.inject(this)

        this.viewModel = ViewModelProviders.of(this, this.factory).get(HomeViewModel::class.java)

        this.collapsing_toolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        this.collapsing_toolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)

        this.initAdapter()

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        this.drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        this.nav_view.setNavigationItemSelectedListener(this)

        this.viewModel.getUser()
        this.viewModel.getSections()
        this.observeViewState()
    }


    @SuppressLint("SetTextI18n")
    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is HomeViewState.User -> {
                    this.nameUser.text = String.format(getString(R.string.home_text_welcome), viewState.user?.firstName)
                }
                is HomeViewState.SectionsState -> {
                    this.adapter.list = viewState.sections

                }
            }

        })
    }

    private fun initAdapter() {
        this.profiles.layoutManager = GridLayoutManager(this, 2)
        this.profiles.addItemDecoration(GridItemDecoration(10, 2))
        this.profiles.adapter = this.adapter
        //this.profiles.setOnTouchListener { _, _ -> true }
        this.adapter.clickListener = { section ->
            val intent = Intent(this, SectionsActivity::class.java)
            intent.putExtra("id_section", section.idSection)
            intent.putExtra("title", section.name)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            alert {
                title = getString(R.string.app_name)
                message = getString(R.string.action_logout)
                okButton {
                    this@HomeActivity.sharedPreferencesManager.clearSession()
                    this@HomeActivity.startActivity(
                        intentFor<LoginActivity>().setFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                    Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                    )
                    finish()
                }
                onCancelled {

                }
            }.show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_my_profile -> {
                startActivity(Intent(this, MyProfileActivity::class.java))
            }

            R.id.nav_about_us -> {
                this@HomeActivity.startActivity(intentFor<AboutUsActivity>())
            }

            R.id.nav_contact_us -> {
                val TO = arrayOf("app.npd@gmail.com")
                val mailClient = Intent(Intent.ACTION_SEND)
                mailClient.data = Uri.parse("mailto:")
                mailClient.type = "text/plain"
                mailClient.putExtra(Intent.EXTRA_EMAIL, TO)
                mailClient.setPackage("com.google.android.gm")
                startActivity(mailClient)
            }

            R.id.nav_logout -> {
                this@HomeActivity.sharedPreferencesManager.clearSession()
                this@HomeActivity.startActivity(
                    intentFor<LoginActivity>().setFlags(
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                Intent.FLAG_ACTIVITY_NEW_TASK
                    )
                )
                finish()
            }

            R.id.nav_assesment_history -> {
                this@HomeActivity.startActivity(intentFor<AssessmentsActivity>())
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun getProfiles(profileType: String): ArrayList<Profile> {
        return when (profileType) {
            "Studen" ->
                arrayListOf(
                    Profile(
                        resources.getInteger(R.integer.home_id_intrapersonal_competency),
                        getString(R.string.home_text_intrapersonal_competency),
                        R.drawable.img_student
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_interpersonal_competency),
                        getString(R.string.home_text_interpersonal_competency),
                        R.drawable.img_parent
                    )
                )

            "Principal" ->
                arrayListOf(
                    Profile(
                        resources.getInteger(R.integer.home_id_self),
                        getString(R.string.home_text_self),
                        R.drawable.img_self
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_teacher),
                        getString(R.string.home_text_teacher),
                        R.drawable.img_teacher
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_teacher),
                        getString(R.string.home_text_parent),
                        R.drawable.img_parent
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_student),
                        getString(R.string.home_text_student),
                        R.drawable.img_student
                    )
                )
            "Parent" ->
                arrayListOf(
                    Profile(
                        resources.getInteger(R.integer.home_id_self),
                        getString(R.string.home_text_self),
                        R.drawable.img_self
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_principal),
                        getString(R.string.home_text_principal),
                        R.drawable.img_teacher
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_teacher),
                        getString(R.string.home_text_teacher),
                        R.drawable.img_teacher
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_student),
                        getString(R.string.home_text_student),
                        R.drawable.img_student
                    )
                )
            "Teacher" ->
                arrayListOf(
                    Profile(
                        resources.getInteger(R.integer.home_id_self),
                        getString(R.string.home_text_self),
                        R.drawable.img_self
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_teacher),
                        getString(R.string.home_text_teacher),
                        R.drawable.img_teacher
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_parent),
                        getString(R.string.home_text_parent),
                        R.drawable.img_parent
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_student),
                        getString(R.string.home_text_student),
                        R.drawable.img_student
                    )
                )
            else ->
                arrayListOf(
                    Profile(
                        resources.getInteger(R.integer.home_id_self),
                        getString(R.string.home_text_self),
                        R.drawable.img_self
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_teacher),
                        getString(R.string.home_text_teacher),
                        R.drawable.img_teacher
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_parent),
                        getString(R.string.home_text_parent),
                        R.drawable.img_parent
                    ),
                    Profile(
                        resources.getInteger(R.integer.home_id_student),
                        getString(R.string.home_text_student),
                        R.drawable.img_student
                    )
                )
        }

    }

}
