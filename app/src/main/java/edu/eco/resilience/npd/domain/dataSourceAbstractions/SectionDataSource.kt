package edu.eco.resilience.npd.domain.dataSourceAbstractions

import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.models.UserDetail
import io.reactivex.Observable

interface SectionDataSource {

    fun insertSection(sections: List<Section>)

    fun deleteSections()

    fun getSection(idSection: Int): Observable<Section>

    fun getSections(): Observable<List<Section>>
}