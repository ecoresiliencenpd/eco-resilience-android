package edu.eco.resilience.npd.data.dataSourceImp.api

import edu.eco.resilience.npd.data.service.api.AboutUsService
import edu.eco.resilience.npd.data.service.api.SignUpService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.AboutUsDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SignUpDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.aboutus.AppDetailResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import edu.eco.resilience.npd.domain.mappers.AboutUsMapper
import edu.eco.resilience.npd.domain.models.AppDetail
import io.reactivex.Observable
import javax.inject.Inject

class AboutUsDataSourceImp
@Inject constructor(
    private val aboutUsService: AboutUsService,
    private val aboutUsMapper: AboutUsMapper
) : AboutUsDataSource {

    override fun getAppDetail(): Observable<AppDetail> =
        this.aboutUsService.getAppDetail().map { t: AppDetailResponse -> this.aboutUsMapper.mapFromApi(t) }
}