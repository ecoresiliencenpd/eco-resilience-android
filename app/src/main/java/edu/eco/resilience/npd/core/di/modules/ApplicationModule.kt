package edu.eco.resilience.npd.core.di.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.domain.executors.JobExecutor
import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.executors.UIThread
import edu.eco.resilience.npd.data.SharedPreferencesManager
import javax.inject.Singleton

@Module
class ApplicationModule(private val applicationContext: Context, private val sharedPreferencesName: String) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = this.applicationContext

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun provideSharedPreferences(applicationContext: Context): SharedPreferences =
        applicationContext.getSharedPreferences(this.sharedPreferencesName, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideSharedPreferencesManager(sharedPreferences: SharedPreferences): SharedPreferencesManager =
        SharedPreferencesManager(sharedPreferences)

}