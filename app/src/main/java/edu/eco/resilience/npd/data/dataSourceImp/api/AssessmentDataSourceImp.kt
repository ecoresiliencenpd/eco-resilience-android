package edu.eco.resilience.npd.data.dataSourceImp.api

import edu.eco.resilience.npd.data.service.ApiResponseHandler
import edu.eco.resilience.npd.data.service.api.AssessmentService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.AssessmentDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.mappers.DetailApplicationAssessmentMapper
import edu.eco.resilience.npd.domain.mappers.HistoryAssessmentMapper
import edu.eco.resilience.npd.domain.mappers.NewestAssessmentDetailMapper
import edu.eco.resilience.npd.domain.mappers.NewestAssessmentMapper
import edu.eco.resilience.npd.domain.models.*
import io.reactivex.Observable
import io.reactivex.functions.Function3
import javax.inject.Inject

class AssessmentDataSourceImp
@Inject constructor(
    private val assessmentService: AssessmentService,
    private val historyAssessmentMapper: HistoryAssessmentMapper,
    private val detailApplicationAssessmentMapper: DetailApplicationAssessmentMapper,
    private val apiResponseHandler: ApiResponseHandler,
    private val newestAssessmentMapper: NewestAssessmentMapper,
    private val newestAssessmentDetailMapper: NewestAssessmentDetailMapper
) : AssessmentDataSource {

    override fun evaluation(evaluationObjectRequest: EvaluationObjectRequest, basic: String): Observable<Evaluation> =
        this.assessmentService.evaluation(evaluationObjectRequest, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .map { reponse ->
                Evaluation(
                    reponse.message ?: ""
                    , reponse.statusCode ?: 0
                )
            }
    

    override fun newestAssessmentDetail(idAssessment: String, basic: String): Observable<List<NewestAssessmentDetail>> =
        this.assessmentService.newestAssessmentDetail(idAssessment, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .flatMapIterable { it }
            .map(this.newestAssessmentDetailMapper::mapFromApi)
            .toList()
            .toObservable()


    override fun historyAssessment(idUser: String, basic: String): Observable<List<HistoryAssessment>> =
        Observable.zip(
            this.history(idUser, basic),
            this.newEstAssessment(idUser, basic),
            this.newEstAssessment(idUser, basic),
            Function3 { t1, t2, _ ->
                val list: MutableList<HistoryAssessment> = mutableListOf()
                if (t2.name.isNotEmpty()) {
                    list.add(
                        HistoryAssessment(
                            Assessment(
                                t2.daysOfLife,
                                t2.passScore,
                                t2.description,
                                t2.active,
                                t2.idLanguage,
                                t2.idParent,
                                t2.multipleApplications,
                                t2.repeatable,
                                t2.lastUpdate,
                                t2.name,
                                t2.publicationDate,
                                t2.id,
                                t2.idProfile
                                , true
                            ),
                            Pdf(0, 0, 0, 0, "", ""),
                            Application(0f, 0, 0, 0, 0, 0)
                        )
                    )
                }
                list.addAll(t1)

                list

            }
        )


    private fun history(idUser: String, basic: String) =
        this.assessmentService.historyAssessment(idUser, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .flatMapIterable { it }
            .map(this.historyAssessmentMapper::mapFromApi)
            .toList()
            .toObservable()


    private fun newEstAssessment(idUser: String, basic: String) =
        this.assessmentService.newestAssessment(idUser, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .map(this.newestAssessmentMapper::mapFromApi)


    override fun detailApplicationAssessment(
        idApplication: String,
        basic: String
    ): Observable<DetailApplicationAssessment> =
        this.assessmentService.detailApplicationAssessment(idApplication, basic)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }
            .map(this.detailApplicationAssessmentMapper::mapFromApi)
}