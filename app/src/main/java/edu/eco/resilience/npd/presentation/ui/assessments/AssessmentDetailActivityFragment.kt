package edu.eco.resilience.npd.presentation.ui.assessments

import android.os.Bundle
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import kotlinx.android.synthetic.main.fragment_assessment_detail.*

private const val ARG_ASSESSMENT = "arg_assessment"

class AssessmentDetailActivityFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_assessment_detail

    private var assessment: HistoryAssessment? = null

    companion object {

        @JvmStatic
        fun newInstance(assessment: HistoryAssessment?) =
            AssessmentDetailActivityFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ASSESSMENT, assessment)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            assessment = it.getParcelable(ARG_ASSESSMENT)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.nameAssessment.text = this.assessment?.assessment?.name
        this.descriptionAssessment.text = this.assessment?.assessment?.description
        this.btnViewDetail.text =
            if (assessment?.assessment?.new == true) getString(R.string.assessment_detail_text_take_assessment) else getString(
                R.string.assessment_detail_text_view_detail
            )

        this.btnViewDetail.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragment, AssessmentDetailStepsFragment.newInstance(this.assessment))
                ?.addToBackStack(null)
                ?.commit()
        }
    }
}
