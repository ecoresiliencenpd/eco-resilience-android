package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.Evaluation
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import io.reactivex.Observable

interface AssessmentRepository {
    fun historyAssessment(idUser: String, basic: String): Observable<List<HistoryAssessment>>
    fun detailApplicationAssessment(idApplication: String, basic: String): Observable<DetailApplicationAssessment>
    fun newestAssessmentDetail(idAssessment: String, basic: String): Observable<List<NewestAssessmentDetail>>
    fun evaluation(evaluationObjectRequest: EvaluationObjectRequest, basic: String): Observable<Evaluation>

}