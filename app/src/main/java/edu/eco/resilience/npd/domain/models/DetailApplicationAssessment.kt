package edu.eco.resilience.npd.domain.models

import android.os.Parcel
import android.os.Parcelable

data class DetailApplicationAssessment(
    val idBranch: Int,
    val score: Float,
    val questions: List<QuestionsItem>,
    val idApplication: Int,
    val creationDate: Long
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readFloat(),
        parcel.createTypedArrayList(QuestionsItem),
        parcel.readInt(),
        parcel.readLong()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idBranch)
        parcel.writeFloat(score)
        parcel.writeTypedList(questions)
        parcel.writeInt(idApplication)
        parcel.writeLong(creationDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DetailApplicationAssessment> {
        override fun createFromParcel(parcel: Parcel): DetailApplicationAssessment {
            return DetailApplicationAssessment(parcel)
        }

        override fun newArray(size: Int): Array<DetailApplicationAssessment?> {
            return arrayOfNulls(size)
        }
    }
}

data class QuestionsItem(
    val idChoice: Int,
    val question: String,
    val idAssessment: Int,
    val questionType: String,
    val active: Int,
    val idType: Int,
    val creationDate: Long,
    val choices: List<ChoicesItem>,
    val value: Int,
    val idQuestion: Int,
    val order: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.createTypedArrayList(ChoicesItem),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idChoice)
        parcel.writeString(question)
        parcel.writeInt(idAssessment)
        parcel.writeString(questionType)
        parcel.writeInt(active)
        parcel.writeInt(idType)
        parcel.writeLong(creationDate)
        parcel.writeTypedList(choices)
        parcel.writeInt(value)
        parcel.writeInt(idQuestion)
        parcel.writeInt(order)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<QuestionsItem> {
        override fun createFromParcel(parcel: Parcel): QuestionsItem {
            return QuestionsItem(parcel)
        }

        override fun newArray(size: Int): Array<QuestionsItem?> {
            return arrayOfNulls(size)
        }
    }

}

data class ChoicesItem(
    val idChoice: Int,
    val active: Int,
    val creationDate: Long,
    val choice: String,
    val value: Int,
    val branch: Int,
    val idQuestion: Int,
    val order: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idChoice)
        parcel.writeInt(active)
        parcel.writeLong(creationDate)
        parcel.writeString(choice)
        parcel.writeInt(value)
        parcel.writeInt(branch)
        parcel.writeInt(idQuestion)
        parcel.writeInt(order)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChoicesItem> {
        override fun createFromParcel(parcel: Parcel): ChoicesItem {
            return ChoicesItem(parcel)
        }

        override fun newArray(size: Int): Array<ChoicesItem?> {
            return arrayOfNulls(size)
        }
    }

}