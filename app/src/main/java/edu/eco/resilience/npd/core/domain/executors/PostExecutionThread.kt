package edu.eco.resilience.npd.core.domain.executors

import io.reactivex.Scheduler

interface PostExecutionThread {

    fun getScheduler(): Scheduler
}