package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.repositiryAbstractions.SectionRepository
import io.reactivex.Observable
import javax.inject.Inject

class SectionRepositoryImpl
@Inject constructor(private val sectionDataSource: SectionDataSource) :
    SectionRepository {

    override fun getSections(): Observable<List<Section>> =
        sectionDataSource.getSections()

    override fun getSection(idSection: Int): Observable<Section> =
        sectionDataSource.getSection(idSection)

}