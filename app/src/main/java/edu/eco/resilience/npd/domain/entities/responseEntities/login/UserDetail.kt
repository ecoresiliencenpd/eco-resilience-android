package edu.eco.resilience.npd.domain.entities.responseEntities.login

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class UserDetail(

	@field:SerializedName("id_school")
	val idSchool: Int? = null,

	@field:SerializedName("id_education_level")
	val idEducationLevel: Int? = null,

	@field:SerializedName("mail")
	val mail: String? = null,

	@field:SerializedName("profile")
	val profile: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id_language")
	val idLanguage: Int? = null,

	@field:SerializedName("id_district")
	val idDistrict: Int? = null,

	@field:SerializedName("language")
	val language: String? = null,

	@field:SerializedName("id_user")
	val idUser: Int? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("middle_name")
	val middleName: String? = null,

	@field:SerializedName("school")
	val school: String? = null,

	@field:SerializedName("district")
	val district: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("education_level")
	val educationLevel: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("id_state")
	val idState: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("id_profile")
	val idProfile: Int? = null
)