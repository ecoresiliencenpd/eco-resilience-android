package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.aboutus.AppDetailResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.login.LoginEntity
import edu.eco.resilience.npd.domain.models.AppDetail
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SocialMedia
import edu.eco.resilience.npd.domain.models.UserDetail
import javax.inject.Inject

class AboutUsMapper
@Inject constructor() : BaseMapper<AppDetail, AppDetailResponse> {
    override fun mapFromApi(responseObject: AppDetailResponse): AppDetail =
        AppDetail(responseObject.version?.versionApp ?: "",
            responseObject.contactSupport?.name ?: "",
            responseObject.contactSupport?.mail ?: "",
            responseObject.appPolicy?.name ?: "",
            responseObject.appPolicy?.link ?: "",
            responseObject.socialMedia.map { socialMediaItem ->
                SocialMedia(
                    socialMediaItem.name ?: "",
                    socialMediaItem.link ?: ""
                )
            }
        )


}