package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.databaseEntities.UserEntity
import edu.eco.resilience.npd.domain.models.UserDetail
import javax.inject.Inject

class UserDatabaseMapper @Inject constructor() : BaseMapper<UserDetail, UserEntity> {

    override fun mapFromApi(responseObject: UserEntity): UserDetail =
        UserDetail(
            responseObject.idSchool,
            responseObject.idEducationLevel,
            responseObject.mail,
            responseObject.profile,
            responseObject.lastName,
            responseObject.idLanguage,
            responseObject.idDistrict,
            responseObject.language,
            responseObject.idUser,
            responseObject.creationDate,
            responseObject.middleName,
            responseObject.school,
            responseObject.district,
            responseObject.firstName,
            responseObject.educationLevel,
            responseObject.state,
            responseObject.idState,
            responseObject.firstName,
            responseObject.idProfile
        )
}