package edu.eco.resilience.npd.presentation.ui.home.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.*
import javax.inject.Inject

class HomeViewModelFactory
@Inject constructor(
    private val getUserUseCase: GetUserUseCase,
    private val getSectionsUseCase: GetSectionsUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(getUserUseCase, getSectionsUseCase) as T
    }
}