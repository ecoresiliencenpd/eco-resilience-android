package edu.eco.resilience.npd.domain.entities.responseEntities.assessment

import com.google.gson.annotations.SerializedName

data class DetailApplicationAssessmentObjectResponse(

    @field:SerializedName("id_branch")
    val idBranch: Int? = null,

    @field:SerializedName("score")
    val score: Float? = null,

    @field:SerializedName("questions")
    val questions: List<QuestionsItem>? = null,

    @field:SerializedName("id_application")
    val idApplication: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null
)

data class QuestionsItem(

    @field:SerializedName("id_choice")
    val idChoice: Int? = null,

    @field:SerializedName("question")
    val question: String? = null,

    @field:SerializedName("id_assessment")
    val idAssessment: Int? = null,

    @field:SerializedName("question_type")
    val questionType: String? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("id_type")
    val idType: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null,

    @field:SerializedName("choices")
    val choices: List<ChoicesItem>? = null,

    @field:SerializedName("value")
    val value: Int? = null,

    @field:SerializedName("id_question")
    val idQuestion: Int? = null,

    @field:SerializedName("order")
    val order: Int? = null
)

data class ChoicesItem(

    @field:SerializedName("id_choice")
    val idChoice: Int? = null,

    @field:SerializedName("active")
    val active: Int? = null,

    @field:SerializedName("creation_date")
    val creationDate: Long? = null,

    @field:SerializedName("choice")
    val choice: String? = null,

    @field:SerializedName("value")
    val value: Int? = null,

    @field:SerializedName("branch")
    val branch: Int? = null,

    @field:SerializedName("id_question")
    val idQuestion: Int? = null,

    @field:SerializedName("order")
    val order: Int? = null
)