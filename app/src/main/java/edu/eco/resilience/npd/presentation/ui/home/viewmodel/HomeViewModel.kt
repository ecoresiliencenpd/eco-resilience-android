package edu.eco.resilience.npd.presentation.ui.home.viewmodel

import android.arch.lifecycle.MutableLiveData
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.models.UserDetail
import edu.eco.resilience.npd.domain.useCases.GetSectionsUseCase
import edu.eco.resilience.npd.domain.useCases.GetUserUseCase

class HomeViewModel(
    private val getUserUseCase: GetUserUseCase,
    private val getSectionsUseCase: GetSectionsUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<HomeViewState> = MutableLiveData()

    fun getUser() {
        this.addDisposable(
            this.getUserUseCase.execute("").subscribe(::handleUser)
        )
    }

    fun getSections() {
        this.addDisposable(
            this.getSectionsUseCase.execute("").subscribe(::handleGetSections)
        )
    }

    private fun handleUser(user: UserDetail) {
        this.viewState.value = HomeViewState.User(user)

    }

    private fun handleGetSections(sections: List<Section>) {
        this.viewState.value = HomeViewState.SectionsState(sections)
    }

}