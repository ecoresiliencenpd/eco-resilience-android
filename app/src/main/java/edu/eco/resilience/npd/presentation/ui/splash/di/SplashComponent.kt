package edu.eco.resilience.npd.presentation.ui.splash.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.splash.SplashActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class])
interface SplashComponent {

    fun inject(splashActivity: SplashActivity)
}