package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SaveDevice
import io.reactivex.Observable

interface LoginRepository {
    fun login(loginRequest: LoginRequest): Observable<Login>

    fun saveDevices(request: SaveDeviceRequest, basic: String): Observable<SaveDevice>

}