package edu.eco.resilience.npd.core

import io.reactivex.observers.DisposableObserver

abstract class DefaultObserver<T, V>(val view: V?) : DisposableObserver<T>()