<<<<<<< HEAD
package edu.eco.resilience.npd.presentation.ui.assessments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.Application
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationsItem
import edu.eco.resilience.npd.domain.models.*
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentModule
import edu.eco.resilience.npd.presentation.ui.assessments.di.DaggerAssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetailApplicationAssessmentViewModel
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetailApplicationAssessmentViewState
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetaillApplicationAssessmentViewModelFactory
import kotlinx.android.synthetic.main.fragment_assessment_detail_steps.*
import javax.inject.Inject

private const val ARG_ASSESSMENT = "arg_assessment"

class AssessmentDetailStepsFragment : BaseFragment(), DetailNewestAssessmentQuestionFragment.Callback {

    override fun layoutId(): Int = R.layout.fragment_assessment_detail_steps

    private var assessment: HistoryAssessment? = null
    private var newestAssessmentDetail: MutableList<NewestAssessmentDetail> = mutableListOf()
    private var detailApplicationAssessment: DetailApplicationAssessment? = null
    private val assessmentComponent: AssessmentComponent by lazy {
        DaggerAssessmentComponent.builder()
            .applicationComponent(applicationComponent)
            .assessmentModule(AssessmentModule())
            .build()
    }
    @Inject
    lateinit var factory: DetaillApplicationAssessmentViewModelFactory
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    private val viewModel: DetailApplicationAssessmentViewModel by lazy {
        ViewModelProviders.of(this, this.factory).get(DetailApplicationAssessmentViewModel::class.java)
    }

    private val evaluations: MutableList<EvaluationsItem> = mutableListOf()

    companion object {

        @JvmStatic
        fun newInstance(assessment: HistoryAssessment?) =
            AssessmentDetailStepsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ASSESSMENT, assessment)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.assessmentComponent.inject(this)
        this.arguments?.let {
            this.assessment = it.getParcelable(ARG_ASSESSMENT)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (this.assessment?.assessment?.new == true) {
            this.viewModel.getNewestAssessmentDetail(this.assessment?.assessment?.id.toString())
        } else {
            this.viewModel.getDetailApplicationAssessment(this.assessment?.application?.idApplication.toString())
        }
        this.observeViewState()
        this.imageSteps.setupWithViewPager(this.viewPagerQuestions)
        this.imageSteps.scaleUp = 2.0f
        this.imageSteps.animationDuration = 500
        this.toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        this.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        this.btnNext.setOnClickListener {
            moveNextQuestion()
        }
        this.btnPrevious.setOnClickListener {
            movePreviousQuestion()
        }

    }

    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is DetailApplicationAssessmentViewState.DetailApplicationAssessmentState -> {
                    this.detailApplicationAssessment = viewState.detailApplicationAssessment
                    this.imageSteps.setSteps(viewState.detailApplicationAssessment.questions.size)
                    this.imageSteps.visibility =
                        if (viewState.detailApplicationAssessment.questions.size > 1) View.VISIBLE else View.GONE
                    this.viewPagerQuestions?.adapter = DetailApplicationAssessmentQuestionVPAdapter(
                        childFragmentManager,
                        viewState.detailApplicationAssessment.questions
                    )
                }

                is DetailApplicationAssessmentViewState.NewestAsessmentDetailState -> {
                    this.newestAssessmentDetail.clear()
                    this.evaluations.clear()
                    for (item in viewState.newestAssessmentDetail) {
                        this.evaluations.add(EvaluationsItem(0, item.idChoice, 0f, item.idQuestion))
                    }
                    this.newestAssessmentDetail.addAll(viewState.newestAssessmentDetail)
                    this.imageSteps.setSteps(viewState.newestAssessmentDetail.size)
                    this.imageSteps.visibility =
                        if (viewState.newestAssessmentDetail.size > 1) View.VISIBLE else View.GONE
                    this.viewPagerQuestions?.adapter =
                        DetailNewestAssessmentQuestionVPAdapter(childFragmentManager, viewState.newestAssessmentDetail)
                }

                is DetailApplicationAssessmentViewState.EvaluationState -> {
                    Toast.makeText(context, viewState.evaluation.message, Toast.LENGTH_LONG).show()
                    completeAssessment(getScore(this.evaluations))
                }

                is DetailApplicationAssessmentViewState.ProgressIndicator -> {
                    this.loading.visibility = viewState.visibility

                }
            }

        })

    }

    private fun moveNextQuestion() {
        if (this.viewPagerQuestions.currentItem == (this.newestAssessmentDetail.size - 1)
            || this.viewPagerQuestions.currentItem == ((this.detailApplicationAssessment?.questions?.size ?: 0) - 1)
        ) {
            if (this.assessment?.assessment?.new == true) {
                this.viewModel.evaluation(
                    EvaluationObjectRequest(
                        this.evaluations,
                        Application(
                            getScore(this.evaluations),
                            null,
                            1,
                            null,
                            sharedPreferencesManager.getString(SharedPreferencesManager.USER_ID).toInt(),
                            null
                        )
                    )
                )
            } else {
                val score = detailApplicationAssessment?.score ?: 0f
                completeAssessment(score)

            }
        } else {
            viewPagerQuestions.currentItem = this.viewPagerQuestions.currentItem + 1
            if (this.viewPagerQuestions.currentItem != 0) {
                btnPrevious.visibility = View.VISIBLE
            }
        }
    }

    private fun movePreviousQuestion() {
        viewPagerQuestions.currentItem = this.viewPagerQuestions.currentItem - 1
        if (this.viewPagerQuestions.currentItem == 0) {
            btnPrevious.visibility = View.GONE
        }
    }

    private fun completeAssessment(score: Float) {
        val intent = Intent(context, AssessmentCompleteActivity::class.java)
        intent.putExtra("score", score)
        startActivity(intent)
        activity?.finish()
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        if (childFragment is DetailNewestAssessmentQuestionFragment) {
            childFragment.callback = this
        }
    }

    override fun onSeekbarValuesChange(pos: Int, choice: BranchesItem) {
        this.evaluations[pos].score = choice.value.toFloat()
        this.evaluations[pos].idBranch = choice.branch
        this.evaluations[pos].idChoice = choice.idChoice
        this.evaluations[pos].idQuestion = choice.idQuestion

    }

    private fun getScore(evaluations: MutableList<EvaluationsItem>): Float {
        var score = 0f
        for (item in evaluations) {
            score += item.score!!
        }

        return score
    }

}
<<<<<<< HEAD
=======
package edu.eco.resilience.npd.presentation.ui.assessments


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.Application
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationsItem
import edu.eco.resilience.npd.domain.models.ChoicesItemNewest
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentModule
import edu.eco.resilience.npd.presentation.ui.assessments.di.DaggerAssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetailApplicationAssessmentViewModel
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetailApplicationAssessmentViewState
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.DetaillApplicationAssessmentViewModelFactory
import kotlinx.android.synthetic.main.fragment_assessment_detail_steps.*
import javax.inject.Inject

private const val ARG_ASSESSMENT = "arg_assessment"

class AssessmentDetailStepsFragment : BaseFragment(), DetailNewestAssessmentQuestionFragment.Callback {

    override fun layoutId(): Int = R.layout.fragment_assessment_detail_steps

    private var assessment: HistoryAssessment? = null
    private var newestAssessmentDetail: MutableList<NewestAssessmentDetail> = mutableListOf()
    private var detailApplicationAssessment: DetailApplicationAssessment? = null
    private val assessmentComponent: AssessmentComponent by lazy {
        DaggerAssessmentComponent.builder()
            .applicationComponent(applicationComponent)
            .assessmentModule(AssessmentModule())
            .build()
    }
    @Inject
    lateinit var factory: DetaillApplicationAssessmentViewModelFactory
    @Inject
    lateinit var sharedPreferencesManager: SharedPreferencesManager

    private val viewModel: DetailApplicationAssessmentViewModel by lazy {
        ViewModelProviders.of(this, this.factory).get(DetailApplicationAssessmentViewModel::class.java)
    }

    private val evaluations: MutableList<EvaluationsItem> = mutableListOf()

    companion object {

        @JvmStatic
        fun newInstance(assessment: HistoryAssessment?) =
            AssessmentDetailStepsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ASSESSMENT, assessment)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.assessmentComponent.inject(this)
        this.arguments?.let {
            this.assessment = it.getParcelable(ARG_ASSESSMENT)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (this.assessment?.assessment?.new == true) {
            this.viewModel.getNewestAssessmentDetail(this.assessment?.assessment?.id.toString())
        } else {
            this.viewModel.getDetailApplicationAssessment(this.assessment?.application?.idApplication.toString())
        }
        this.observeViewState()
        this.imageSteps.setupWithViewPager(this.viewPagerQuestions)
        this.imageSteps.scaleUp = 2.0f
        this.imageSteps.animationDuration = 500
        this.toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        this.toolbar.setNavigationOnClickListener { activity?.onBackPressed() }
        this.btnNext.setOnClickListener {
            moveNextQuestion()
        }
        this.btnPrevious.setOnClickListener {
            movePreviousQuestion()
        }

    }

    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is DetailApplicationAssessmentViewState.DetailApplicationAssessmentState -> {
                    this.detailApplicationAssessment = viewState.detailApplicationAssessment
                    this.imageSteps.setSteps(viewState.detailApplicationAssessment.questions.size)
                    this.imageSteps.visibility =
                        if (viewState.detailApplicationAssessment.questions.size > 1) View.VISIBLE else View.GONE
                    this.viewPagerQuestions?.adapter = DetailApplicationAssessmentQuestionVPAdapter(
                        childFragmentManager,
                        viewState.detailApplicationAssessment.questions
                    )
                }

                is DetailApplicationAssessmentViewState.NewestAsessmentDetailState -> {
                    this.newestAssessmentDetail.clear()
                    this.evaluations.clear()
                    for (item in viewState.newestAssessmentDetail) {
                        this.evaluations.add(EvaluationsItem(0, item.idChoice, 0f, item.idQuestion))
                    }
                    this.newestAssessmentDetail.addAll(viewState.newestAssessmentDetail)
                    this.imageSteps.setSteps(viewState.newestAssessmentDetail.size)
                    this.imageSteps.visibility =
                        if (viewState.newestAssessmentDetail.size > 1) View.VISIBLE else View.GONE
                    this.viewPagerQuestions?.adapter =
                        DetailNewestAssessmentQuestionVPAdapter(childFragmentManager, viewState.newestAssessmentDetail)
                }

                is DetailApplicationAssessmentViewState.EvaluationState -> {
                    Toast.makeText(context, viewState.evaluation.message, Toast.LENGTH_LONG).show()
                    completeAssessment(getScore(this.evaluations))
                }

                is DetailApplicationAssessmentViewState.ProgressIndicator -> {
                    this.loading.visibility = viewState.visibility

                }
            }

        })

    }

    private fun moveNextQuestion() {
        if (this.viewPagerQuestions.currentItem == (this.newestAssessmentDetail.size - 1)
            || this.viewPagerQuestions.currentItem == ((this.detailApplicationAssessment?.questions?.size ?: 0) - 1)
        ) {
            if (this.assessment?.assessment?.new == true) {
                this.viewModel.evaluation(
                    EvaluationObjectRequest(
                        this.evaluations,
                        Application(
                            getScore(this.evaluations),
                            null,
                            1,
                            null,
                            sharedPreferencesManager.getString(SharedPreferencesManager.USER_ID).toInt(),
                            null
                        )
                    )
                )
            } else {
                val score = detailApplicationAssessment?.score ?: 0f
                completeAssessment(score)

            }
        } else {
            viewPagerQuestions.currentItem = this.viewPagerQuestions.currentItem + 1
            if (this.viewPagerQuestions.currentItem != 0) {
                btnPrevious.visibility = View.VISIBLE
            }
        }
    }

    private fun movePreviousQuestion() {
        viewPagerQuestions.currentItem = this.viewPagerQuestions.currentItem - 1
        if (this.viewPagerQuestions.currentItem == 0) {
            btnPrevious.visibility = View.GONE
        }
    }

    private fun completeAssessment(score: Float) {
        val intent = Intent(context, AssessmentCompleteActivity::class.java)
        intent.putExtra("score", score)
        startActivity(intent)
        activity?.finish()
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        if (childFragment is DetailNewestAssessmentQuestionFragment) {
            childFragment.callback = this
        }
    }

    override fun onSeekbarValuesChange(pos: Int, choice: ChoicesItemNewest) {
        this.evaluations[pos].score = choice.value.toFloat()
        this.evaluations[pos].idBranch = choice.branch
        this.evaluations[pos].idChoice = choice.idChoice
        this.evaluations[pos].idQuestion = choice.idQuestion

    }

    private fun getScore(evaluations: MutableList<EvaluationsItem>): Float {
        var score = 0f
        for (item in evaluations) {
            score += item.score!!
        }

        return score/evaluations.size
    }

}
>>>>>>> 465259e... Modificaciones View Pager New Detail y Detail en imageSteps, Progres con titulo de branch, flujo final de NewAssesment Correcto
=======
>>>>>>> 2ed632d... Revert "Modificaciones View Pager New Detail y Detail en imageSteps, Progres con titulo de branch, flujo final de NewAssesment Correcto"
