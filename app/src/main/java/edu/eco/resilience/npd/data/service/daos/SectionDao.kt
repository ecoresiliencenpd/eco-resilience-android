package edu.eco.resilience.npd.data.service.daos

import android.arch.persistence.room.*
import edu.eco.resilience.npd.domain.entities.databaseEntities.SectionEntity
import edu.eco.resilience.npd.domain.entities.responseEntities.databases.SectionDatabaseResponseObject
import io.reactivex.Maybe

@Dao
abstract class SectionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSection(section: SectionEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSectionModule(sectionModuleEntity: SectionEntity.SectionModuleEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSectionTheme(sectionModuleEntity: SectionEntity.SectionModuleEntity.SectionThemeEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSectionQuestion(sectionModuleEntity: SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSectionAnswer(sectionModuleEntity: SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity.SectionAnswerEntity)

    @Transaction
    open fun insertSectionTransation(sections: List<SectionEntity>) {
        for (section in sections) {
            val sectionDbId = this.insertSection(section)
            for (module in section.modules) {
                module.sectionDbId = sectionDbId
                val moduleDbId = this.insertSectionModule(module)
                for (theme in module.themes) {
                    theme.moduleDbId = moduleDbId
                    val themeDbId = this.insertSectionTheme(theme)
                    for (question in theme.questions) {
                        question.themeDbId = themeDbId
                        val questionDbId = this.insertSectionQuestion(question)
                        for (answer in question.answers) {
                            answer.questionDbId = questionDbId
                            this.insertSectionAnswer(answer)
                        }
                    }
                }
            }
        }
    }

    @Query(value = "DELETE FROM section")
    abstract fun deleteSections()

    @Query(value = "SELECT * FROM section")
    abstract fun getSections(): Maybe<List<SectionDatabaseResponseObject>>

    @Query(value = "SELECT * FROM section WHERE id_section = :idSection")
    abstract fun getSection(idSection: Int): Maybe<SectionDatabaseResponseObject>
}