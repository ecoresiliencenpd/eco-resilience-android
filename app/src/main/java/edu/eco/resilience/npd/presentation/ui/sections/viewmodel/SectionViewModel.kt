package edu.eco.resilience.npd.presentation.ui.sections.viewmodel

import android.arch.lifecycle.MutableLiveData
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.useCases.GetSectionUseCase
import edu.eco.resilience.npd.presentation.ui.sections.SectionModel

class SectionViewModel(
    val getSectionUseCase: GetSectionUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<SectionViewState> = MutableLiveData()

    fun getSection(idSection: Int) {
        this.addDisposable(
            this.getSectionUseCase.execute(idSection).subscribe(::handleSection)
        )
    }

    private fun handleSection(section: Section) {
        this.viewState.value = SectionViewState.SectionState(mapperSectionModel(section))
    }


    private fun mapperSectionModel(section: Section): List<SectionModel> {
        val list: MutableList<SectionModel> = mutableListOf()
        for (module in section.modules) {
            for (theme in module.themes) {
                theme.title = module.name
            }
            list.add(SectionModel(module.name, module.themes))
        }
        return list
    }

}