package edu.eco.resilience.npd.domain.dataSourceAbstractions

import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import io.reactivex.Observable
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.EvaluationObjectResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.NewestAssessmentDetailObjectResponse
import edu.eco.resilience.npd.domain.models.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Path

interface AssessmentDataSource {

    fun historyAssessment(idUser: String, basic: String): Observable<List<HistoryAssessment>>

    fun detailApplicationAssessment(idApplication: String, basic: String): Observable<DetailApplicationAssessment>

    fun newestAssessmentDetail(idAssessment: String, basic: String): Observable<List<NewestAssessmentDetail>>

    fun evaluation(evaluationObjectRequest: EvaluationObjectRequest, basic: String): Observable<Evaluation>
}