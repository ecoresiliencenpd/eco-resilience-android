package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import edu.eco.resilience.npd.domain.models.HistoryAssessment

sealed class HistoryAssessmentViewState {
    data class HistoryAssessmentState(val historyAssessment: List<HistoryAssessment>) : HistoryAssessmentViewState()
    data class ProgressIndicator(val visibility: Int) : HistoryAssessmentViewState()
}