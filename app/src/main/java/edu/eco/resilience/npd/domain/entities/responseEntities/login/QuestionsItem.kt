package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class QuestionsItem(

    @SerializedName("name")
    val _name: String? = null,

    @SerializedName("answers")
    val _answers: List<AnswersItem>? = null,

    @SerializedName("id_question")
    val _idQuestion: Int? = null
) {
    val name: String
        get() = this._name ?: ""

    val answers: List<AnswersItem>
        get() = _answers ?: emptyList()

    val idQuestion: Int
    get() = _idQuestion ?: 0
}