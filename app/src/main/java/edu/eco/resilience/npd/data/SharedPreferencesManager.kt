package edu.eco.resilience.npd.data

import android.content.SharedPreferences

class SharedPreferencesManager(private val sharedPreferences: SharedPreferences) {

    companion object {
        val FILE_USER_DATA = "UserData"
        val USER_ID = "user_id"
        val USER_EMAIL = "user_email"
        val USER_PASSWORD = "user_password"
        val USER_CHECK_SAVE_PASSOWRD = "user_check_save_password"
        val TOKEN_BASIC = "token_basic"
        val LENGUAGE = "lenguage"

        val DEFAULT = ""
        val DEFAULT_INT = 0
    }

    fun saveString(key: String, value: String?) {
        val editor = this.sharedPreferences.edit()
        editor.putString(key, value ?: "")
        editor.apply()
    }

    fun getString(key: String, defaultValue: String = ""): String =
        this.sharedPreferences.getString(key, defaultValue) ?: defaultValue

    fun saveInt(key: String, value: Int?) {
        val editor = this.sharedPreferences.edit()
        editor.putInt(key, value ?: -1)
        editor.apply()
    }

    fun getInt(key: String, defaultValue: Int = 0): Int =
        this.sharedPreferences.getInt(key, defaultValue)

    fun saveFloat(key: String, value: Float?) {
        val editor = this.sharedPreferences.edit()
        editor.putFloat(key, value ?: -1f)
        editor.apply()
    }

    fun getFloat(key: String, defaultValue: Float): Float =
        this.sharedPreferences.getFloat(key, defaultValue)

    fun saveDouble(key: String, value: Double?) {
        val editor = this.sharedPreferences.edit()
        editor.putFloat(key, value?.toFloat() ?: -1.0f)
        editor.apply()
    }

    fun getDouble(key: String, defaultValue: Double): Double =
        this.sharedPreferences.getFloat(key, defaultValue.toFloat()).toDouble()

    fun saveLong(key: String, value: Long?) {
        val editor = this.sharedPreferences.edit()
        editor.putLong(key, value ?: -1)
        editor.apply()
    }

    fun getLong(key: String, defaultValue: Long): Long =
        this.sharedPreferences.getLong(key, defaultValue)

    fun saveBoolean(key: String, value: Boolean?) {
        val editor = this.sharedPreferences.edit()
        editor.putBoolean(key, value ?: false)
        editor.apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean =
        this.sharedPreferences.getBoolean(key, defaultValue)

    var isUserLogged: Boolean? = getString(USER_ID, DEFAULT).isNotBlank()

    fun clearSession() {
        this.sharedPreferences.edit().clear().apply()
    }

}