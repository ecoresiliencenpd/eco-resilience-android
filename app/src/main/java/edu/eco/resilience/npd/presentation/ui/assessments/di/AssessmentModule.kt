package edu.eco.resilience.npd.presentation.ui.assessments.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.dataSourceImp.api.AssessmentDataSourceImp
import edu.eco.resilience.npd.data.repositoryImp.AssessmentRepositoryImpl
import edu.eco.resilience.npd.data.service.api.AssessmentService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.AssessmentDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.AssessmentRepository
import retrofit2.Retrofit

@Module
class AssessmentModule {

    @Provides
    @MainScope
    fun provideAssessmentServices(retrofit: Retrofit): AssessmentService =
        retrofit.create(AssessmentService::class.java)

    @Provides
    @MainScope
    fun provideAssessmentRepository(assessmentRepositoryImpl: AssessmentRepositoryImpl): AssessmentRepository =
        assessmentRepositoryImpl

    @Provides
    @MainScope
    fun provideAssessmentDataSource(assessmentDataSourceImp: AssessmentDataSourceImp): AssessmentDataSource =
        assessmentDataSourceImp

}