package edu.eco.resilience.npd.presentation.ui.myprofile

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import kotlinx.android.synthetic.main.item_my_profile.view.*
import kotlin.properties.Delegates

class MyProfileAdapter : RecyclerView.Adapter<MyProfileAdapter.ViewHolder>() {

    internal var list: List<MyProfile> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_my_profile, parent, false
            )
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], position)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(profile: MyProfile, position: Int) = with(itemView) {
            labelProfile.text = profile.labelProfile
            profileText.text = profile.profile

            val bg = context.getDrawable(R.drawable.bg_profile_item)
            bg!!.alpha = 255
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (position % 2 == 0) {
                    bg.alpha = 100
                    bg.setTint(context.resources.getColor(R.color.colorPrimaryDark, context.theme))
                    contentProfile.background = bg
                } else {

                    bg.setTint(context.resources.getColor(R.color.cream, context.theme))
                    contentProfile.background = bg
                }
            } else {
                if (position % 2 == 0) {
                    bg.alpha = 100
                    bg.setTint(context.resources.getColor(R.color.colorPrimaryDark))
                    contentProfile.background = bg
                } else {

                    bg.setTint(context.resources.getColor(R.color.cream))
                    contentProfile.background = bg
                }
            }
        }
    }

}