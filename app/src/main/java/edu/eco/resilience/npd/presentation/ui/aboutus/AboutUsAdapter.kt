package edu.eco.resilience.npd.presentation.ui.aboutus

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.presentation.ui.pdfview.PDFViewActivity
import kotlinx.android.synthetic.main.item_about_us.view.*

class AboutUsAdapter(val list: List<AboutUs>, val context: Context) :
    RecyclerView.Adapter<AboutUsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_about_us, parent, false
        )
    )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
        holder.view.setOnClickListener {
            if (list[position].link.endsWith(".pdf")) {
                val i = Intent(context, PDFViewActivity::class.java)
                i.putExtra("URL", list[position].link)
                context.startActivity(i)
            } else {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse(list[position].link))
                context.startActivity(i)
            }
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var view: View
        fun bind(item: AboutUs) = with(itemView) {
            aboutItem.text = item.item
            view = this
        }
    }
}