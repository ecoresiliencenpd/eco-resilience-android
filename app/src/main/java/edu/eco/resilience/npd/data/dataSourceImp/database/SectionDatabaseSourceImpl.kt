package edu.eco.resilience.npd.data.dataSourceImp.database

import edu.eco.resilience.npd.data.service.daos.SectionDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.entities.responseEntities.databases.SectionDatabaseResponseObject
import edu.eco.resilience.npd.domain.mappers.SectionDatabaseMapper
import edu.eco.resilience.npd.domain.mappers.SectionMapper
import edu.eco.resilience.npd.domain.models.Section
import io.reactivex.Observable
import javax.inject.Inject

class SectionDatabaseSourceImpl
@Inject constructor(
    private val sectionDao: SectionDao,
    private val sectionMapper: SectionMapper,
    private val sectionDatabaseMapper: SectionDatabaseMapper
) : SectionDataSource {


    override fun insertSection(sections: List<Section>) {
        sectionDao.insertSectionTransation(this.sectionMapper.mapFromApi(sections))
    }

    override fun deleteSections() {
        this.sectionDao.deleteSections()
    }

    override fun getSections(): Observable<List<Section>> =
        this.sectionDao.getSections().toObservable()
            .flatMapIterable { section -> section }
            .map { t: SectionDatabaseResponseObject -> sectionDatabaseMapper.mapFromApi(t) }
            .toList()
            .toObservable()


    override fun getSection(idSection: Int): Observable<Section> =
        this.sectionDao.getSection(idSection)
            .map { t: SectionDatabaseResponseObject -> this.sectionDatabaseMapper.mapFromApi(t) }
            .toObservable()

}