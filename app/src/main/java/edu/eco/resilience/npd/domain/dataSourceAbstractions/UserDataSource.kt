package edu.eco.resilience.npd.domain.dataSourceAbstractions

import edu.eco.resilience.npd.domain.models.UserDetail
import io.reactivex.Observable

interface UserDataSource {

    fun insertUser(user: UserDetail)

    fun deleteUsers()

    fun getUser(): Observable<UserDetail>
}