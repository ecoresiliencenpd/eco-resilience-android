package edu.eco.resilience.npd.domain.dataSourceAbstractions

import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import io.reactivex.Observable

interface SignUpDataSource {

    fun getStates(): Observable<List<StateResponse>>

    fun getDistricts(idState: String): Observable<List<DistrictsResponse>>

    fun getSchools(idState: String, idDistrict: String, idEducationLevel: String): Observable<List<SchoolsResponse>>

    fun signUp(signUpRequest: SignUpRequest): Observable<SignUpResponse>

    fun getEducationLevels(): Observable<List<EducationLevelsResponse>>
}