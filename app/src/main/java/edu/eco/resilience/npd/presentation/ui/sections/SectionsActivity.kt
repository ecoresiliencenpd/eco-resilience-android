package edu.eco.resilience.npd.presentation.ui.sections

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.presentation.ui.questions.QuestionsActivity
import edu.eco.resilience.npd.presentation.ui.questions.QuestionsListActivity
import edu.eco.resilience.npd.presentation.ui.sections.di.DaggerSectionComponent
import edu.eco.resilience.npd.presentation.ui.sections.di.SectionComponent
import edu.eco.resilience.npd.presentation.ui.sections.di.SectionModule
import edu.eco.resilience.npd.presentation.ui.sections.viewmodel.SectionViewModel
import edu.eco.resilience.npd.presentation.ui.sections.viewmodel.SectionViewModelFactory
import edu.eco.resilience.npd.presentation.ui.sections.viewmodel.SectionViewState
import kotlinx.android.synthetic.main.activity_sections.*
import kotlinx.android.synthetic.main.content_sections.*
import javax.inject.Inject

class SectionsActivity : BaseActivity() {

    private val sectionComponent: SectionComponent by lazy {
        DaggerSectionComponent.builder()
            .applicationComponent(applicationComponent)
            .sectionModule(SectionModule())
            .build()
    }

    @Inject
    lateinit var factory: SectionViewModelFactory

    private lateinit var viewModel: SectionViewModel

    val adapter: SectionAdapter by lazy { SectionAdapter(this) }

    override fun getLayoutResId(): Int = R.layout.activity_sections

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.toolbar.title = intent.getStringExtra("title")
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.sectionComponent.inject(this)

        this.section?.adapter = this.adapter
        this.adapter.clickListener = { theme ->
            val intent = Intent(this, QuestionsListActivity::class.java)
            intent.putExtra("theme", theme)
            startActivity(intent)
        }

        this.viewModel = ViewModelProviders.of(this, this.factory).get(SectionViewModel::class.java)
        this.viewModel.getSection(intent.getIntExtra("id_section", 0))

        this.observeViewState()
    }


    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is SectionViewState.SectionState -> {
                    this.adapter.updateSections(viewState.section)
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
