package edu.eco.resilience.npd.presentation.ui.login.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.login.LoginActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [LoginModule::class])
interface LoginComponent {

    fun inject(loginActivity: LoginActivity)
}