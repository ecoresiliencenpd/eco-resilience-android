package edu.eco.resilience.npd.presentation.ui.sections.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.sections.SectionsActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [SectionModule::class])
interface SectionComponent {

    fun inject(sectionsActivity: SectionsActivity)
}