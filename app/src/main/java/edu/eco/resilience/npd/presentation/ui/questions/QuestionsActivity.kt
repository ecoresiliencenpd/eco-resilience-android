package edu.eco.resilience.npd.presentation.ui.questions

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.domain.models.Question
import edu.eco.resilience.npd.domain.models.Theme
import edu.eco.resilience.npd.presentation.ui.pdfview.PDFViewActivity
import kotlinx.android.synthetic.main.activity_questions.*
import kotlinx.android.synthetic.main.content_questions.*

class QuestionsActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_questions

    private var question: Question? = null
    private lateinit var menu: Menu
    private var urlDoc: String = ""

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        this.question = intent.getParcelableExtra("question")

        this.toolbar.title = intent.getStringExtra("title")

        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.initData()

    }

    private fun initData() {
        this.textQuestion?.text = question?.name
        this.textHeader?.text = intent.getStringExtra("theme_name")
        this.viewPager?.adapter = QuestionVPAdapter(supportFragmentManager, question?.answers!!)
        this.viewPager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                this@QuestionsActivity.urlDoc = this@QuestionsActivity.question?.answers!![p0].ulrDoc
                this@QuestionsActivity.visibleIconPdf(p0)
            }

        })
        this.dotsIndicator.setViewPager(this.viewPager)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.question_menu, menu)
        menu?.findItem(R.id.action_pdf)?.icon?.setColorFilter(
            resources.getColor(android.R.color.white),
            PorterDuff.Mode.SRC_IN
        )
        this.menu = menu!!
        this.visibleIconPdf(0)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
            R.id.action_pdf -> {
                val i = Intent(this, PDFViewActivity::class.java)
                i.putExtra("URL", this.urlDoc)
                startActivity(i)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun visibleIconPdf(pos: Int) {
        this.menu.findItem(R.id.action_pdf).isVisible = this.question?.answers!![pos].ulrDoc.isNotEmpty()
    }

}
