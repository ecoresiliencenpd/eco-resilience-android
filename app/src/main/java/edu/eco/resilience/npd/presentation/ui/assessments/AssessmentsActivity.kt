package edu.eco.resilience.npd.presentation.ui.assessments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.di.AssessmentModule
import edu.eco.resilience.npd.presentation.ui.assessments.di.DaggerAssessmentComponent
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.HistoryAssessmentViewModel
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.HistoryAssessmentViewModelFactory
import edu.eco.resilience.npd.presentation.ui.assessments.viewmodel.HistoryAssessmentViewState
import kotlinx.android.synthetic.main.activity_assessments.*
import kotlinx.android.synthetic.main.content_assessments.*
import kotlinx.android.synthetic.main.layout_message.*
import javax.inject.Inject

class AssessmentsActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_assessments

    private val assessmentComponent: AssessmentComponent by lazy {
        DaggerAssessmentComponent.builder()
            .applicationComponent(applicationComponent)
            .assessmentModule(AssessmentModule())
            .build()
    }
    @Inject
    lateinit var factory: HistoryAssessmentViewModelFactory

    private val viewModel: HistoryAssessmentViewModel by lazy {
        ViewModelProviders.of(this, this.factory).get(HistoryAssessmentViewModel::class.java)
    }

    private val adapter: HistoryAssessmentAdapter by lazy { HistoryAssessmentAdapter() }

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.assessmentComponent.inject(this)
        this.viewModel.getHistoryAssessment()
        this.initAdapter()
        this.observeViewState()
        this.asset.setAnimation("empty.json")
        this.asset.pauseAnimation()
        this.asset.loop(true)
    }

    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is HistoryAssessmentViewState.HistoryAssessmentState -> {
                    if (viewState.historyAssessment.isEmpty()) {
                        this.asset.playAnimation()
                        this.message.visibility = View.VISIBLE
                        this.historyAssessment.visibility = View.INVISIBLE
                    } else {
                        this.adapter.list = viewState.historyAssessment
                    }
                }
                is HistoryAssessmentViewState.ProgressIndicator -> {
                    this.loading.visibility = viewState.visibility
                    this.historyAssessment.visibility =
                        if (viewState.visibility == View.VISIBLE) View.INVISIBLE else View.VISIBLE
                }
            }

        })

    }

    private fun initAdapter() {
        this.historyAssessment.adapter = adapter
        this.adapter.clickListenerDetailAssessment = { historyAssessment ->
            val intent = Intent(this, AssessmentDetailActivity::class.java)
            intent.putExtra("assessment", historyAssessment)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
