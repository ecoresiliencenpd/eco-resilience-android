package edu.eco.resilience.npd.domain.entities.responseEntities.databases

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import edu.eco.resilience.npd.domain.entities.databaseEntities.SectionEntity

class SectionDatabaseResponseObject {

    @Embedded
    var section: SectionEntity? = null

    @Relation(entity = SectionEntity.SectionModuleEntity::class, parentColumn = "db_id", entityColumn = "section_db_id")
    var modules: List<SectionModuleDatabaseResponseObject> = emptyList()

    class SectionModuleDatabaseResponseObject {
        @Embedded
        var module: SectionEntity.SectionModuleEntity? = null

        @Relation(
            entity = SectionEntity.SectionModuleEntity.SectionThemeEntity::class,
            parentColumn = "db_id",
            entityColumn = "module_db_id"
        )
        var themes: List<SectionModuleThemeDatabaseResponseObject> = emptyList()

        class SectionModuleThemeDatabaseResponseObject {
            @Embedded
            var theme: SectionEntity.SectionModuleEntity.SectionThemeEntity? = null

            @Relation(
                entity = SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity::class,
                parentColumn = "db_id",
                entityColumn = "theme_db_id"
            )
            var questions: List<SectionModuleThemeQuestionResponseObject> = emptyList()

            class SectionModuleThemeQuestionResponseObject {

                @Embedded
                var question: SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity? = null

                @Relation(
                    entity = SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity.SectionAnswerEntity::class,
                    parentColumn = "db_id",
                    entityColumn = "question_db_id"
                )
                var answers: List<SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity.SectionAnswerEntity> =
                    emptyList()

            }
        }
    }
}