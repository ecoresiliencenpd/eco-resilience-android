package edu.eco.resilience.npd.presentation.ui.questions

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.domain.models.Theme
import kotlinx.android.synthetic.main.activity_questions_list.*
import kotlinx.android.synthetic.main.content_questions_list.*

class QuestionsListActivity : BaseActivity() {

    private val adapter: QuestionAdapter by lazy { QuestionAdapter() }

    override fun getLayoutResId(): Int = R.layout.activity_questions_list

    private var theme: Theme? = null

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        this.theme = intent.getParcelableExtra("theme")
        this.toolbar.title = theme?.title

        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.initData()

    }

    private fun initData() {
        this.textHeader.text = this.theme?.name
        this.questions.adapter = this.adapter
        this.adapter.list = theme?.questions ?: emptyList()
        this.adapter.clickListener = { question ->
            val intent = Intent(this, QuestionsActivity::class.java)
            intent.putExtra("title", this.theme?.title)
            intent.putExtra("theme_name", this.theme?.name)
            intent.putExtra("question", question)
            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
            R.id.action_pdf -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }

}
