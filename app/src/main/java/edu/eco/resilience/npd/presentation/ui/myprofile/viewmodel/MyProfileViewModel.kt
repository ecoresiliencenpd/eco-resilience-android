package edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel

import android.arch.lifecycle.MutableLiveData
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.domain.models.UserDetail
import edu.eco.resilience.npd.domain.useCases.GetUserUseCase

class MyProfileViewModel(
    private val getUserUseCase: GetUserUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<MyProfileViewState> = MutableLiveData()

    fun getUser() {
        this.addDisposable(
            this.getUserUseCase.execute("").subscribe(::handleUser)
        )
    }

    private fun handleUser(user: UserDetail) {
        this.viewState.value = MyProfileViewState.User(user)

    }

}