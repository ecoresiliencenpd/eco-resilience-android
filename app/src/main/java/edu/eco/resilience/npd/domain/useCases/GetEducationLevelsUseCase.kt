package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.EducationLevelsResponse
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetEducationLevelsUseCase
@Inject constructor(
    private val signUpRepository: SignUpRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<List<EducationLevelsResponse>, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<List<EducationLevelsResponse>> =
        this.signUpRepository.getEducationLevels()

}