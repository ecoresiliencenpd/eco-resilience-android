package edu.eco.resilience.npd.presentation.ui.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.extension.loadFromUrl
import edu.eco.resilience.npd.domain.models.Section
import kotlinx.android.synthetic.main.item_home.view.*
import kotlin.properties.Delegates

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    internal var list: List<Section> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (Section) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_home, parent, false
            )
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], clickListener)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(section: Section, clickListener: (Section) -> Unit) = with(itemView) {
            setOnClickListener { clickListener(section) }
            image.loadFromUrl(getImage(section.idSection))
            label.text = section.name
        }


        fun getImage(idSection: Int): Int {
            return when (idSection) {
                1 -> R.drawable.img_parent
                2, 13 -> R.drawable.img_self
                3, 14 -> R.drawable.img_student
                4, 15 -> R.drawable.img_teacher
                8, 16 -> R.drawable.img_teacher
                9 -> R.drawable.img_student
                11 -> R.drawable.img_student
                12 -> R.drawable.img_parent
                else -> {
                    R.drawable.img_parent
                }
            }
        }

    }
}