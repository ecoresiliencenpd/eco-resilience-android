package edu.eco.resilience.npd.presentation.ui.login.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.*
import javax.inject.Inject

class LoginViewModelFactory
@Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val saveDevicesUseCase: SaveDevicesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(LoginUseCase::class.java, SaveDevicesUseCase::class.java)
            .newInstance(loginUseCase, saveDevicesUseCase)
    }
}