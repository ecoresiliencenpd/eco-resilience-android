package edu.eco.resilience.npd.presentation.ui.adapters

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import edu.eco.resilience.npd.R

class SpinnerDropDownAdapter(val context: Context, val list: List<String>) : BaseAdapter(), SpinnerAdapter {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val txt = TextView(context)
        txt.gravity = Gravity.CENTER
        txt.setPadding(16, 16, 16, 16)
        txt.textSize = 16f
        txt.text = list[position]
        txt.setTextColor(context.resources.getColor(R.color.black))
        return txt
    }

    override fun getItem(position: Int): Any = this.list[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = list.size
}