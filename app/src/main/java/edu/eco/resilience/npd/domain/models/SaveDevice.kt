package edu.eco.resilience.npd.domain.models

data class SaveDevice(
    val message: String,
    val statusCode: Int
)