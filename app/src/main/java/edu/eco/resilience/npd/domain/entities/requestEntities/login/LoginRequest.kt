package edu.eco.resilience.npd.domain.entities.requestEntities.login

import com.google.gson.annotations.SerializedName

data class LoginRequest(

	@SerializedName("password")
	val password: String? = null,

	@SerializedName("username")
	val username: String? = null
)