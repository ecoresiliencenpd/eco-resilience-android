package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.ChoicesItemNewestAsessmentDetailObjectResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.NewestAssessmentDetailObjectResponse
import edu.eco.resilience.npd.domain.models.BranchesItem
import edu.eco.resilience.npd.domain.models.ChoicesItemNewest
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import javax.inject.Inject

class NewestAssessmentDetailMapper @Inject constructor() :
    BaseMapper<NewestAssessmentDetail, NewestAssessmentDetailObjectResponse> {

    override fun mapFromApi(responseObject: NewestAssessmentDetailObjectResponse): NewestAssessmentDetail {
        val newestAssessmentDetail = NewestAssessmentDetail(
            responseObject.idChoice ?: 0,
            responseObject.question ?: "",
            responseObject.idAssessment ?: 0,
            responseObject.questionType ?: "",
            responseObject.active ?: 0,
            responseObject.idType ?: 0,
            responseObject.creationDate ?: 0,
            responseObject.choices.map { choices ->
                ChoicesItemNewest(
                    choices.idBranch ?: 0,
                    choices.branches.map { branches ->
                        BranchesItem(
                            branches.idChoice ?: 0,
                            branches.branchName ?: "",
                            branches.active ?: 0,
                            branches.creationDate ?: 0L,
                            branches.choice ?: "",
                            branches.value ?: 0,
                            branches.branch ?: 0,
                            branches.idQuestion ?: 0,
                            branches.order ?: 0
                        )
                    }

                )

            },
            responseObject.value ?: 0,
            responseObject.idQuestion ?: 0,
            responseObject.order ?: 0
        )

        return newestAssessmentDetail
    }
}