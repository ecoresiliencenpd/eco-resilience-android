package edu.eco.resilience.npd.presentation.ui.questions


import android.os.Bundle
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.domain.models.Answer
import edu.eco.resilience.npd.domain.models.Question
import kotlinx.android.synthetic.main.fragment_question.*

private const val ARG_ANSWER = "arg_answer"

class QuestionFragment : BaseFragment() {


    private var answer: Answer? = null

    override fun layoutId(): Int = R.layout.fragment_question

    companion object {

        @JvmStatic
        fun newInstance(answer: Answer) =
            QuestionFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_ANSWER, answer)
                }
            }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.textQuestion.text = this.answer?.name
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            answer = it.getParcelable(ARG_ANSWER)
        }
    }
}
