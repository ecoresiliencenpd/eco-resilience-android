package edu.eco.resilience.npd.domain.entities.requestEntities.signup

import com.google.gson.annotations.SerializedName

data class SignUpRequest(

    @SerializedName("user_profile")
    val userProfile: UserProfile? = null,

    @SerializedName("user")
    val user: User? = null
)

data class User(

    @SerializedName("first_name")
    val firstName: String? = null,

    @SerializedName("last_name")
    val lastName: String? = null,

    @SerializedName("middle_name")
    val middleName: String? = null,

    @SerializedName("id_state")
    val idState: Int? = null,

    @SerializedName("id_district")
    val idDistrict: Int? = null,

    @SerializedName("id_school")
    val idSchool: Int? = null,

    @SerializedName("email")
    val email: String? = null,

    @SerializedName("password")
    val password: String? = null,


    @SerializedName("id_education_level")
    val idEducationLevel: Int? = null

)

data class UserProfile(

    @SerializedName("is_main")
    val isMain: Int? = null,

    @SerializedName("id_language")
    val idLanguage: Int? = null,

    @SerializedName("id_profile")
    val idProfile: Int? = null
)