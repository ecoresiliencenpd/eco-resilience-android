package edu.eco.resilience.npd.presentation.ui.assessments

import android.os.Bundle
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import kotlinx.android.synthetic.main.activity_assessment_complte.*

class AssessmentCompleteActivity : BaseActivity() {

    private val scoreAssessment get() = intent.getFloatExtra("score", 0f)

    override fun getLayoutResId(): Int = R.layout.activity_assessment_complte

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)

        this.score.text = scoreAssessment.toString()
        this.btnFinish.setOnClickListener {
            finish()
        }

    }
}
