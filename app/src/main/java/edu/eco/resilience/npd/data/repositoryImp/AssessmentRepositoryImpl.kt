package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.AssessmentDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.Evaluation
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.domain.repositiryAbstractions.AssessmentRepository
import io.reactivex.Observable
import javax.inject.Inject

class AssessmentRepositoryImpl
@Inject constructor(
    private val assessmentDataSource: AssessmentDataSource
) :
    AssessmentRepository {

    override fun evaluation(evaluationObjectRequest: EvaluationObjectRequest, basic: String): Observable<Evaluation> =
        this.assessmentDataSource.evaluation(evaluationObjectRequest, basic)

    override fun newestAssessmentDetail(idAssessment: String, basic: String): Observable<List<NewestAssessmentDetail>> =
        this.assessmentDataSource.newestAssessmentDetail(idAssessment, basic)

    override fun historyAssessment(idUser: String, basic: String): Observable<List<HistoryAssessment>> =
        this.assessmentDataSource.historyAssessment(idUser, basic)

    override fun detailApplicationAssessment(
        idApplication: String,
        basic: String
    ): Observable<DetailApplicationAssessment> =
        this.assessmentDataSource.detailApplicationAssessment(idApplication, basic)
}