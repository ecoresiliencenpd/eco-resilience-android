package edu.eco.resilience.npd.data.service.api

import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface AssessmentService {

    companion object {
        private const val HISTORY_ASSESSMENT =
            "WSEcoResilenceNPD_V2/rest/npdProgram/assessment/get/history/user/{id_user}"
        private const val DETAIL_APPLICATION_ASSESSMENT =
            "WSEcoResilenceNPD_V2/rest/npdProgram/assessment/get/detail/application/{id_application}"
        private const val NEWEST_ASSESSMENT =
            "WSEcoResilenceNPD_V2/rest/npdProgram/assessment/get/newest/{id_user}"
        private const val NEWEST_ASSESSMENT_DETAIL =
            "WSEcoResilenceNPD_V2/rest/npdProgram/assessment/get/newest/detail/{id_assessment}"
        private const val EVALUATION =
            "WSEcoResilenceNPD_V2/rest/npdProgram/assessment/save/evaluation"
    }

    @GET(HISTORY_ASSESSMENT)
    fun historyAssessment(
        @Path("id_user") idUser: String,
        @Header("Authorization") basic: String
    ): Observable<Response<List<HistoryAssessmentObjectResponse>>>

    @GET(DETAIL_APPLICATION_ASSESSMENT)
    fun detailApplicationAssessment(
        @Path("id_application") idApplication: String,
        @Header("Authorization") basic: String
    ): Observable<Response<DetailApplicationAssessmentObjectResponse>>

    @GET(NEWEST_ASSESSMENT)
    fun newestAssessment(
        @Path("id_user") idUser: String,
        @Header("Authorization") basic: String
    ): Observable<Response<NewestAssessmentObjectResponse>>

    @GET(NEWEST_ASSESSMENT_DETAIL)
    fun newestAssessmentDetail(
        @Path("id_assessment") idAssessment: String,
        @Header("Authorization") basic: String
    ): Observable<Response<List<NewestAssessmentDetailObjectResponse>>>

    @POST(EVALUATION)
    fun evaluation(
        @Body evaluationObjectRequest: EvaluationObjectRequest,
        @Header("Authorization") basic: String
    ): Observable<Response<EvaluationObjectResponse>>

}