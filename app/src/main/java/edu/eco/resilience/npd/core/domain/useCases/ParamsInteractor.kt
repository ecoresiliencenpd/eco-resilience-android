package edu.eco.resilience.npd.core.domain.useCases

import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver

interface ParamsInteractor<Result, Params> {

    fun execute(params: Params): Observable<Result>

}