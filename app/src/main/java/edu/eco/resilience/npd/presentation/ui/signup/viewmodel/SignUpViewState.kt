package edu.eco.resilience.npd.presentation.ui.signup.viewmodel

import edu.eco.resilience.npd.domain.entities.responseEntities.signup.DistrictsResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SchoolsResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SignUpResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.StateResponse

sealed class SignUpViewState {
    data class States(val states: List<StateResponse>) : SignUpViewState()
    data class Districts(val districts: List<DistrictsResponse>) : SignUpViewState()
    data class Schools(val schools: List<SchoolsResponse>) : SignUpViewState()
    data class SignUp(val signUp: SignUpResponse) : SignUpViewState()
    data class ProgressIndicator(val visibility: Int) : SignUpViewState()
}