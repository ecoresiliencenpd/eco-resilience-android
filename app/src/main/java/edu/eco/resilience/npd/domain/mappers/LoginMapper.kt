package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.login.LoginEntity
import edu.eco.resilience.npd.domain.models.*
import javax.inject.Inject

class LoginMapper
@Inject constructor() : BaseMapper<Login, LoginEntity> {

    override fun mapFromApi(responseObject: LoginEntity): Login =
        Login(
            UserDetail(
                responseObject.userDetail?.idSchool ?: 0,
                responseObject.userDetail?.idEducationLevel ?: 0,
                responseObject.userDetail?.mail ?: "",
                responseObject.userDetail?.profile ?: "",
                responseObject.userDetail?.lastName ?: "",
                responseObject.userDetail?.idLanguage ?: 0,
                responseObject.userDetail?.idDistrict ?: 0,
                responseObject.userDetail?.language ?: "",
                responseObject.userDetail?.idUser ?: 0,
                responseObject.userDetail?.creationDate ?: "",
                responseObject.userDetail?.middleName ?: "",
                responseObject.userDetail?.school ?: "",
                responseObject.userDetail?.district ?: "",
                responseObject.userDetail?.name ?: "",
                responseObject.userDetail?.educationLevel ?: "",
                responseObject.userDetail?.state ?: "",
                responseObject.userDetail?.idState ?: 0,
                responseObject.userDetail?.firstName ?: "",
                responseObject.userDetail?.idProfile ?: 0

            ),
            responseObject.catalog?.sections!!.map { sectionsItem ->
                Section(
                    sectionsItem.idSection,
                    sectionsItem.name,
                    sectionsItem.modules.map { module ->
                        Module(
                            module.idModule,
                            module.name,
                            module.themes.map { theme ->
                                Theme(theme.idTheme,
                                    theme.name,
                                    theme.questions.map { question ->
                                        Question(
                                            question.idQuestion,
                                            question.name,
                                            question.answers.map { answer ->
                                                Answer(
                                                    answer.idAnswer,
                                                    answer.name,
                                                    answer.lastUpdate,
                                                    answer.urlImage,
                                                    answer.urlDoc
                                                )
                                            }
                                        )
                                    }
                                )
                            }
                        )
                    }
                )
            }
        )

}