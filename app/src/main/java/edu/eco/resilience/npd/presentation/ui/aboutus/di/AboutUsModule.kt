package edu.eco.resilience.npd.presentation.ui.aboutus.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.dataSourceImp.api.AboutUsDataSourceImp
import edu.eco.resilience.npd.data.repositoryImp.AboutUsRepositoryImpl
import edu.eco.resilience.npd.data.service.api.AboutUsService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.AboutUsDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.AboutUsRepository
import retrofit2.Retrofit

@Module
class AboutUsModule {

    @Provides
    @MainScope
    fun provideAboutUsService(retrofit: Retrofit): AboutUsService = retrofit.create(AboutUsService::class.java)

    @Provides
    @MainScope
    fun provideLoginDataSource(aboutUsDataSourceImp: AboutUsDataSourceImp): AboutUsDataSource = aboutUsDataSourceImp

    @Provides
    @MainScope
    fun provideAbourUsRepository(aboutUsRepositoryImpl: AboutUsRepositoryImpl): AboutUsRepository =
        aboutUsRepositoryImpl

}