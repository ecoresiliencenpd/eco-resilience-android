package edu.eco.resilience.npd.domain.models

import android.os.Parcel
import android.os.Parcelable
import edu.eco.resilience.npd.presentation.ui.sections.SectionAdapter
import edu.eco.resilience.npd.presentation.utils.Section

data class Section(
    val idSection: Int,
    val name: String,
    val modules: List<Module>
)

data class Module(
    val idModule: Int,
    val name: String,
    val themes: List<Theme>
)

data class Theme(
    val idTheme: Int,
    val name: String,
    val questions: List<Question>,
    var title: String? = null
) : Parcelable, Section.Item {


    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.createTypedArrayList(Question),
        parcel.readString()
    )

    override fun getItemViewType(): Int = SectionAdapter.TYPE_ITEM
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idTheme)
        parcel.writeString(name)
        parcel.writeTypedList(questions)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Theme> {
        override fun createFromParcel(parcel: Parcel): Theme {
            return Theme(parcel)
        }

        override fun newArray(size: Int): Array<Theme?> {
            return arrayOfNulls(size)
        }
    }


}

data class Question(
    val idQuestion: Int,
    val name: String,
    val answers: List<Answer>
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.createTypedArrayList(Answer)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idQuestion)
        parcel.writeString(name)
        parcel.writeTypedList(answers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Question> {
        override fun createFromParcel(parcel: Parcel): Question {
            return Question(parcel)
        }

        override fun newArray(size: Int): Array<Question?> {
            return arrayOfNulls(size)
        }
    }

}

data class Answer(
    val idAnswer: Int,
    val name: String,
    val lastUpdate: String,
    val urlImage: String,
    val ulrDoc: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idAnswer)
        parcel.writeString(name)
        parcel.writeString(lastUpdate)
        parcel.writeString(urlImage)
        parcel.writeString(ulrDoc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Answer> {
        override fun createFromParcel(parcel: Parcel): Answer {
            return Answer(parcel)
        }

        override fun newArray(size: Int): Array<Answer?> {
            return arrayOfNulls(size)
        }
    }

}