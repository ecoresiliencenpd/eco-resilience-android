package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.Evaluation
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail

sealed class DetailApplicationAssessmentViewState {
    data class DetailApplicationAssessmentState(val detailApplicationAssessment: DetailApplicationAssessment) :
        DetailApplicationAssessmentViewState()

    data class NewestAsessmentDetailState(val newestAssessmentDetail: List<NewestAssessmentDetail>) :
        DetailApplicationAssessmentViewState()

    data class ProgressIndicator(val visibility: Int) : DetailApplicationAssessmentViewState()

    data class EvaluationState(val evaluation: Evaluation) : DetailApplicationAssessmentViewState()
}