package edu.eco.resilience.npd.presentation.ui.signup.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.core.platform.SingleLiveEvent
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.DistrictsResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SchoolsResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SignUpResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.StateResponse
import edu.eco.resilience.npd.domain.useCases.GetDistrictUseCase
import edu.eco.resilience.npd.domain.useCases.GetSchoolsUseCase
import edu.eco.resilience.npd.domain.useCases.GetStatesUseCase
import edu.eco.resilience.npd.domain.useCases.SignUpUseCase

class SignUpViewModel(
    private val getStatesUseCase: GetStatesUseCase,
    private val getDistrictUseCase: GetDistrictUseCase,
    private val getSchoolsUseCase: GetSchoolsUseCase,
    private val signUpUseCase: SignUpUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<SignUpViewState> = MutableLiveData()
    var errorState: SingleLiveEvent<String> = SingleLiveEvent()

    fun signUp(signUpRequest: SignUpRequest) {
        this.viewState.value = SignUpViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            this.signUpUseCase.execute(signUpRequest).subscribe(::handleSignUp, ::handleError)
        )
    }

    fun getStates() {
        this.addDisposable(
            this.getStatesUseCase.execute("").subscribe(::handleGetStates, ::handleError)
        )
    }

    fun getDistricts(idState: String) {
        this.addDisposable(
            this.getDistrictUseCase.execute(idState).subscribe(::handleGetDistrincts, ::handleError)
        )
    }

    fun getSchools(getSchoolsRequest: GetSchoolsRequest) {
        this.addDisposable(
            this.getSchoolsUseCase.execute(getSchoolsRequest).subscribe(::handleGetSchools, ::handleError)
        )
    }

    private fun handleSignUp(signUpResponse: SignUpResponse) {
        this.viewState.value = SignUpViewState.SignUp(signUpResponse)
        this.viewState.value = SignUpViewState.ProgressIndicator(View.INVISIBLE)

    }

    private fun handleGetStates(states: List<StateResponse>) {
        this.viewState.value = SignUpViewState.States(states)
    }

    private fun handleGetDistrincts(districts: List<DistrictsResponse>) {
        this.viewState.value = SignUpViewState.Districts(districts)
    }

    private fun handleGetSchools(schools: List<SchoolsResponse>) {
        this.viewState.value = SignUpViewState.Schools(schools)
    }

    private fun handleError(it: Throwable) {
        this.errorState.value = it.message
        this.viewState.value = SignUpViewState.ProgressIndicator(View.INVISIBLE)
    }

}