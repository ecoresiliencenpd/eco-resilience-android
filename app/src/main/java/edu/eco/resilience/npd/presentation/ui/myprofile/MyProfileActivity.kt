package edu.eco.resilience.npd.presentation.ui.myprofile

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.LinearLayout
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.presentation.ui.myprofile.di.DaggerMyProfileComponent
import edu.eco.resilience.npd.presentation.ui.myprofile.di.MyProfileComponent
import edu.eco.resilience.npd.presentation.ui.myprofile.di.MyProfileModule
import edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel.MyProfileViewModel
import edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel.MyProfileViewModelFactory
import edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel.MyProfileViewState
import kotlinx.android.synthetic.main.activity_my_profile.*
import kotlinx.android.synthetic.main.content_my_profile.*
import javax.inject.Inject

class MyProfileActivity : BaseActivity() {

    private val myProfileComponent: MyProfileComponent by lazy {
        DaggerMyProfileComponent.builder()
            .applicationComponent(applicationComponent)
            .myProfileModule(MyProfileModule())
            .build()
    }

    @Inject
    lateinit var factory: MyProfileViewModelFactory

    private lateinit var viewModel: MyProfileViewModel

    private val adapter: MyProfileAdapter by lazy { MyProfileAdapter() }

    override fun getLayoutResId(): Int = R.layout.activity_my_profile

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.toolbar.title = ""
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.myProfileComponent.inject(this)
        this.initAdapter()

        this.viewModel = ViewModelProviders.of(this, this.factory).get(MyProfileViewModel::class.java)
        this.viewModel.getUser()
        this.observeViewState()

    }

    private fun initAdapter() {
        this.profile.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        this.profile.itemAnimator = DefaultItemAnimator()
        this.profile.adapter = this.adapter
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is MyProfileViewState.User -> {
                    val profileList: MutableList<MyProfile> = mutableListOf()
                    profileList.add(
                        MyProfile(
                            getString(R.string.my_profile_text_full_name), "${viewState.user?.firstName ?: ""} ${viewState.user?.lastName ?: ""}"
                        )
                    )
                    profileList.add(MyProfile(getString(R.string.my_profile_text_email), viewState.user?.mail ?: ""))
                    profileList.add(MyProfile(getString(R.string.my_profile_text_profile), viewState.user?.profile ?: ""))
                    profileList.add(MyProfile(getString(R.string.my_profile_text_residence), viewState.user?.district ?: ""))
                    profileList.add(MyProfile(getString(R.string.my_profile_text_school), viewState.user?.school ?: ""))
                    this.adapter.list = profileList
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
