package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.models.UserDetail
import io.reactivex.Observable

interface SectionRepository {

    fun getSection(idSection: Int): Observable<Section>

    fun getSections(): Observable<List<Section>>
}