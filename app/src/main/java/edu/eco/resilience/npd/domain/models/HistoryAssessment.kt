package edu.eco.resilience.npd.domain.models

import android.os.Parcel
import android.os.Parcelable

data class HistoryAssessment(
    val assessment: Assessment,
    val pdf: Pdf,
    val application: Application
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Assessment::class.java.classLoader),
        parcel.readParcelable(Pdf::class.java.classLoader),
        parcel.readParcelable(Application::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(assessment, flags)
        parcel.writeParcelable(pdf, flags)
        parcel.writeParcelable(application, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HistoryAssessment> {
        override fun createFromParcel(parcel: Parcel): HistoryAssessment {
            return HistoryAssessment(parcel)
        }

        override fun newArray(size: Int): Array<HistoryAssessment?> {
            return arrayOfNulls(size)
        }
    }

}

data class Assessment(
    val daysOfLife: Int,
    val passScore: String,
    val description: String,
    val active: Boolean,
    val idLanguage: Int,
    val idParent: Int,
    val multipleApplications: Int,
    val repeatable: Int,
    val lastUpdate: String,
    val name: String,
    val publicationDate: String,
    val id: Int,
    val idProfile: Int,
    val new: Boolean
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(daysOfLife)
        parcel.writeString(passScore)
        parcel.writeString(description)
        parcel.writeByte(if (active) 1 else 0)
        parcel.writeInt(idLanguage)
        parcel.writeInt(idParent)
        parcel.writeInt(multipleApplications)
        parcel.writeInt(repeatable)
        parcel.writeString(lastUpdate)
        parcel.writeString(name)
        parcel.writeString(publicationDate)
        parcel.writeInt(id)
        parcel.writeInt(idProfile)
        parcel.writeByte(if (active) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Assessment> {
        override fun createFromParcel(parcel: Parcel): Assessment {
            return Assessment(parcel)
        }

        override fun newArray(size: Int): Array<Assessment?> {
            return arrayOfNulls(size)
        }
    }

}

data class Pdf(
    val idAssessment: Int,
    val endRange: Int,
    val id: Int,
    val startRange: Int,
    val pdfName: String,
    val url: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idAssessment)
        parcel.writeInt(endRange)
        parcel.writeInt(id)
        parcel.writeInt(startRange)
        parcel.writeString(pdfName)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pdf> {
        override fun createFromParcel(parcel: Parcel): Pdf {
            return Pdf(parcel)
        }

        override fun newArray(size: Int): Array<Pdf?> {
            return arrayOfNulls(size)
        }
    }

}

data class Application(
    val score: Float,
    val idPdf: Int,
    val idAssessment: Int,
    val idApplication: Int,
    val idUser: Int,
    val creationDate: Long
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readFloat(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeFloat(score)
        parcel.writeInt(idPdf)
        parcel.writeInt(idAssessment)
        parcel.writeInt(idApplication)
        parcel.writeInt(idUser)
        parcel.writeLong(creationDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Application> {
        override fun createFromParcel(parcel: Parcel): Application {
            return Application(parcel)
        }

        override fun newArray(size: Int): Array<Application?> {
            return arrayOfNulls(size)
        }
    }

}