package edu.eco.resilience.npd.data.service.api

import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface SignUpService {

    companion object {
        private const val SIGN_UP = "WSEcoResilenceNPD_V2/rest/npdProgram/user/signup"
        private const val GET_STATES = "WSEcoResilenceNPD_V2/rest/npdProgram/catalogue/get/states"
        private const val GET_DISTRICTS = "WSEcoResilenceNPD_V2/rest/npdProgram/catalogue/get/districts/state/{id_state}"
        private const val GET_SCHOOLS = "WSEcoResilenceNPD_V2/rest/npdProgram/catalogue/get/schools/state/{id_state}/district/{id_district}/educationLevel/{id_education_level}"
        private const val GET_EDUCATION_LEVELS = "WSEcoResilenceNPD_V2/rest/npdProgram/catalogue/get/educationLevels"
    }

    @GET(GET_STATES)
    fun getStates(): Observable<List<StateResponse>>

    @GET(GET_DISTRICTS)
    fun getDistricts(@Path("id_state") idState: String): Observable<List<DistrictsResponse>>

    @GET(GET_SCHOOLS)
    fun getSchools(
        @Path("id_state") idState: String,
        @Path("id_district") idDistrict: String,
        @Path("id_education_level") idEducationLevel: String
    ): Observable<List<SchoolsResponse>>

    @GET(GET_EDUCATION_LEVELS)
    fun getEducationLevels(): Observable<List<EducationLevelsResponse>>

    @POST(SIGN_UP)
    fun signUp(@Body signUpRequest: SignUpRequest): Observable<Response<SignUpResponse>>

}