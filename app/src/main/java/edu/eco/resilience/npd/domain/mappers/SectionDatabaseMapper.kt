package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.databaseEntities.SectionEntity
import edu.eco.resilience.npd.domain.entities.responseEntities.databases.SectionDatabaseResponseObject
import edu.eco.resilience.npd.domain.models.*
import javax.inject.Inject

class SectionDatabaseMapper @Inject constructor() : BaseMapper<Section, SectionDatabaseResponseObject> {

    override fun mapFromApi(responseObject: SectionDatabaseResponseObject): Section = Section(
        responseObject.section?.idSection ?: -1,
        responseObject.section?.name ?: "",
        responseObject.modules.map { module ->
            Module(
                module.module?.idModule ?: -1,
                module.module?.name ?: "",
                module.themes.map { theme ->
                    Theme(theme.theme?.idTheme ?: -1,
                        theme.theme?.name ?: "",
                        theme.questions.map { question ->
                            Question(
                                question.question?.idQuestion ?: -1,
                                question.question?.name ?: "",
                                question.answers.map { answer ->
                                    Answer(
                                        answer.idAnswer,
                                        answer.name,
                                        answer.lastUpdate,
                                        answer.urlImage,
                                        answer.ulrDoc
                                    )
                                }
                            )
                        }
                    )
                }
            )
        }
    )

}