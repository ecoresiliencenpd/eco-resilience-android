package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class SectionsItem(

    @SerializedName("id_section")
    val _idSection: Int? = null,

    @SerializedName("name")
    val _name: String? = null,

    @SerializedName("modules")
    val _modules: List<ModulesItem>? = null

) {
    val idSection: Int
        get() = this._idSection ?: 0

    val name: String
        get() = this._name ?: ""

    val modules: List<ModulesItem>
        get() = this._modules ?: emptyList()
}