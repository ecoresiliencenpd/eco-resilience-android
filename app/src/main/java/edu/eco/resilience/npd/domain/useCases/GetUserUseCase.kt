package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.models.UserDetail
import edu.eco.resilience.npd.domain.repositiryAbstractions.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetUserUseCase
@Inject constructor(
    private val userRepository: UserRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<UserDetail, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<UserDetail> =
        this.userRepository.getUser()


}