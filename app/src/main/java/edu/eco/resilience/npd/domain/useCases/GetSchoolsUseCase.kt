package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.DistrictsResponse
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SchoolsResponse
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetSchoolsUseCase
@Inject constructor(
    private val signUpRepository: SignUpRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<List<SchoolsResponse>, GetSchoolsRequest>(threadExecutor, postExecutionThread) {


    override fun createObservable(params: GetSchoolsRequest): Observable<List<SchoolsResponse>> =
        this.signUpRepository.getSchools(params)


}