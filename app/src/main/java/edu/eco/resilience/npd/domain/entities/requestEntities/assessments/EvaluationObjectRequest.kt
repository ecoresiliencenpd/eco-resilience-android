package edu.eco.resilience.npd.domain.entities.requestEntities.assessments

import com.google.gson.annotations.SerializedName

data class EvaluationObjectRequest(

	@field:SerializedName("evaluations")
	val evaluations: List<EvaluationsItem>? = null,

	@field:SerializedName("application")
	val application: Application? = null
)

data class EvaluationsItem(

	@field:SerializedName("id_branch")
	var idBranch: Int? = null,

	@field:SerializedName("id_choice")
	var idChoice: Int? = null,

	@field:SerializedName("score")
	var score: Float? = null,

	@field:SerializedName("id_question")
	var idQuestion: Int? = null
)

data class Application(

	@field:SerializedName("score")
	val score: Float? = null,

	@field:SerializedName("id_pdf")
	val idPdf: Int? = null,

	@field:SerializedName("id_assessment")
	val idAssessment: Int? = null,

	@field:SerializedName("id_application")
	val idApplication: Int? = null,

	@field:SerializedName("id_user")
	val idUser: Int? = null,

	@field:SerializedName("creation_date")
	val creationDate: Int? = null
)