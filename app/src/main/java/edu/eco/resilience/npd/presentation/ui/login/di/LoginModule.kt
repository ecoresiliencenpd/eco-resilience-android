package edu.eco.resilience.npd.presentation.ui.login.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import edu.eco.resilience.npd.data.dataSourceImp.api.LoginDataSourceImp
import edu.eco.resilience.npd.data.dataSourceImp.database.SectionDatabaseSourceImpl
import edu.eco.resilience.npd.data.dataSourceImp.database.UserDatabaseSourceImpl
import edu.eco.resilience.npd.data.repositoryImp.LoginRepositoryImpl
import edu.eco.resilience.npd.data.repositoryImp.SectionRepositoryImpl
import edu.eco.resilience.npd.data.repositoryImp.UserRepositoryImpl
import edu.eco.resilience.npd.data.service.api.LoginService
import edu.eco.resilience.npd.data.service.daos.SectionDao
import edu.eco.resilience.npd.data.service.daos.UserDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.LoginDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.LoginRepository
import edu.eco.resilience.npd.domain.repositiryAbstractions.SectionRepository
import edu.eco.resilience.npd.domain.repositiryAbstractions.UserRepository
import retrofit2.Retrofit

@Module
class LoginModule {

    @Provides
    @MainScope
    fun provideLoginService(retrofit: Retrofit): LoginService = retrofit.create(LoginService::class.java)

    @Provides
    @MainScope
    fun provideUserDao(ecoResilienceDataBase: EcoResilienceDataBase): UserDao = ecoResilienceDataBase.getUserDao()

    @Provides
    @MainScope
    fun provideSectionDao(ecoResilienceDataBase: EcoResilienceDataBase): SectionDao =
        ecoResilienceDataBase.getSectionDao()

    @Provides
    @MainScope
    fun provideLoginDataSource(loginDataSourceImp: LoginDataSourceImp): LoginDataSource = loginDataSourceImp

    @Provides
    @MainScope
    fun provideLoginRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository = loginRepositoryImpl

    @Provides
    @MainScope
    fun provideUserDatabaseSource(userDatabaseSourceImpl: UserDatabaseSourceImpl): UserDataSource =
        userDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository = userRepositoryImpl

    @Provides
    @MainScope
    fun provideSectionDatabaseSource(sectionDatabaseSourceImpl: SectionDatabaseSourceImpl): SectionDataSource =
        sectionDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideSectionRepository(sectionRepositoryImpl: SectionRepositoryImpl): SectionRepository =
        sectionRepositoryImpl

}