package edu.eco.resilience.npd.presentation.ui.assessments

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.domain.models.HistoryAssessment

import kotlinx.android.synthetic.main.activity_assessment_detail.*

class AssessmentDetailActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_assessment_detail

    private var assessment: HistoryAssessment? = null

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.assessment = intent.getParcelableExtra("assessment")
        initFragment()
    }

    private fun initFragment() {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment, AssessmentDetailActivityFragment.newInstance(assessment))
            .commit()
    }

}
