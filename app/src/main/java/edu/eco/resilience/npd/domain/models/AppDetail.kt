package edu.eco.resilience.npd.domain.models

data class AppDetail(
    val versionApp: String,
    val contactSupportName: String,
    val contactSupportMail: String,
    val appPolicyName: String,
    val appPolicyLink: String,
    val socialMedia: List<SocialMedia>
)

data class SocialMedia(
    val name: String,
    val link: String
)


