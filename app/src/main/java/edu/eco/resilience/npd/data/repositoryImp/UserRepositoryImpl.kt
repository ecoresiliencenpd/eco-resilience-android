package edu.eco.resilience.npd.data.repositoryImp

import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.models.UserDetail
import edu.eco.resilience.npd.domain.repositiryAbstractions.UserRepository
import io.reactivex.Observable
import javax.inject.Inject

class UserRepositoryImpl
@Inject constructor(private val userDataSource: UserDataSource) :
    UserRepository {

    override fun getUser(): Observable<UserDetail> = this.userDataSource.getUser()


}