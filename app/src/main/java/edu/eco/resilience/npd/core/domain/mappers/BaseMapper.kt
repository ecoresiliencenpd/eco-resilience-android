package edu.eco.resilience.npd.core.domain.mappers

interface BaseMapper<Model, Api> {

    fun mapFromApi(responseObject: Api): Model

}