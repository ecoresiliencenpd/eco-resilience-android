<<<<<<< HEAD
package edu.eco.resilience.npd.presentation.ui.assessments


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.SeekBar
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.domain.models.BranchesItem
import edu.eco.resilience.npd.domain.models.ChoicesItemNewest
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.domain.models.QuestionsItem
import kotlinx.android.synthetic.main.fragment_detail_application_assessment_question.*


class DetailNewestAssessmentQuestionFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_detail_application_assessment_question

    private val question: NewestAssessmentDetail?
        get() = this.arguments?.getParcelable(ARG_QUESTION)


    var callback: Callback? = null
    private val pos: Int
        get() = this.arguments?.getInt(ARG_POS, 0) ?: 0

    companion object {
        private const val ARG_QUESTION = "arg_question"
        private const val ARG_POS = "arg_pos"
        @JvmStatic
        fun newInstance(question: NewestAssessmentDetail?, pos: Int) =
            DetailNewestAssessmentQuestionFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_QUESTION, question)
                    putInt(ARG_POS, pos)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.numberQuestion.text = "Question ${question?.idQuestion.toString()}"
        this.textQuestion.text = question?.question

        if (question?.choices?.size == 1) {
            this.branche_1.visibility = View.VISIBLE
            this.branche_2.visibility = View.GONE
            this.title_branche_1.visibility = View.GONE

            this.sb.setIntervals(getIntervals())

            this.sb.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    question?.choices!![0].branches[progress].value = progress
                    callback?.onSeekbarValuesChange(pos, question?.choices!![0].branches[progress])
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            })

        } else {
            this.branche_1.visibility = View.VISIBLE
            this.branche_2.visibility = View.VISIBLE

            this.title_branche_1.text =  question?.choices!![0].branches[0].branchName
            this.title_branche_2.text =  question?.choices!![1].branches[0].branchName


            this.sb.setIntervals(getIntervals())

            this.sb.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    question?.choices!![0].branches[progress].value = progress
                    callback?.onSeekbarValuesChange(pos, question?.choices!![0].branches[progress])
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            })

            this.sb_2.setIntervals(getIntervals())

            this.sb_2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    question?.choices!![1].branches[progress].value = progress
                    //callback?.onSeekbarValuesChange(pos, question?.choices!![0].branches[progress])
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

            })

        }



    }

    private fun getIntervals(): MutableList<String> = mutableListOf(
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
    )


    interface Callback {
        fun onSeekbarValuesChange(pos: Int, choice: BranchesItem)
    }
}

<<<<<<< HEAD
=======
package edu.eco.resilience.npd.presentation.ui.assessments


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.SeekBar
import android.widget.Switch
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.domain.models.ChoicesItemNewest
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.domain.models.QuestionsItem
import kotlinx.android.synthetic.main.fragment_detail_application_assessment_question.*


class DetailNewestAssessmentQuestionFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_detail_application_assessment_question

    private val question: NewestAssessmentDetail?
        get() = this.arguments?.getParcelable(ARG_QUESTION)


    var callback: Callback? = null
    private val pos: Int
        get() = this.arguments?.getInt(ARG_POS, 0) ?: 0

    companion object {
        private const val ARG_QUESTION = "arg_question"
        private const val ARG_POS = "arg_pos"
        @JvmStatic
        fun newInstance(question: NewestAssessmentDetail?, pos: Int) =
            DetailNewestAssessmentQuestionFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_QUESTION, question)
                    putInt(ARG_POS, pos)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.numberQuestion.text = "Question ${question?.idQuestion.toString()}"
        this.textQuestion.text = question?.question

         when (pos){
             in  0..9 ->   tv_title.text = "Knowlegde"
             in 10..19->   tv_title.text = "Skills"
         }

        this.sb.setIntervals(getIntervals())
        //this.sb.progress = question?.value ?: 0

        this.sb.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                question?.choices!![progress].value = progress+1
                callback?.onSeekbarValuesChange(pos, question?.choices!![progress])
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

    }

    private fun getIntervals(): MutableList<String> = mutableListOf(
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
    )


    interface Callback {
        fun onSeekbarValuesChange(pos: Int, choice: ChoicesItemNewest)
    }
}

>>>>>>> 465259e... Modificaciones View Pager New Detail y Detail en imageSteps, Progres con titulo de branch, flujo final de NewAssesment Correcto
=======
>>>>>>> 2ed632d... Revert "Modificaciones View Pager New Detail y Detail en imageSteps, Progres con titulo de branch, flujo final de NewAssesment Correcto"
