package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class Catalog(

    @SerializedName("sections")
    val _sections: List<SectionsItem>? = null
) {
    val sections: List<SectionsItem>
        get() = this._sections ?: emptyList()
}