package edu.eco.resilience.npd.presentation.ui.login

import edu.eco.resilience.npd.domain.models.Login

interface LoginContract {

    interface View {
        fun setProgressIndicator(visibility: Int)

        fun showSuccessLogin(login: Login)

        fun showError(error : String)

    }

    interface Presenter {

        fun attachView(view: View)

        fun detachView()

        fun login(email: String, password: String)

    }
}