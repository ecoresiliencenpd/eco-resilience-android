package edu.eco.resilience.npd.data.dataSourceImp.api

import edu.eco.resilience.npd.data.service.ApiResponseHandler
import edu.eco.resilience.npd.data.service.api.SignUpService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SignUpDataSource
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.*
import io.reactivex.Observable
import javax.inject.Inject

class SignUpDataSourceImp
@Inject constructor(
    private val signUpService: SignUpService,
    private val apiResponseHandler: ApiResponseHandler
) : SignUpDataSource {

    override fun getStates(): Observable<List<StateResponse>> =
        this.signUpService.getStates()

    override fun getDistricts(idState: String): Observable<List<DistrictsResponse>> =
        this.signUpService.getDistricts(idState)

    override fun getSchools(
        idState: String, idDistrict: String, idEducationLevel: String
    ): Observable<List<SchoolsResponse>> =
        this.signUpService.getSchools(idState, idDistrict, idEducationLevel)

    override fun signUp(signUpRequest: SignUpRequest): Observable<SignUpResponse> =
        this.signUpService.signUp(signUpRequest)
            .flatMap { response ->
                this.apiResponseHandler.handle(response)
            }

    override fun getEducationLevels(): Observable<List<EducationLevelsResponse>> =
        this.signUpService.getEducationLevels()

}