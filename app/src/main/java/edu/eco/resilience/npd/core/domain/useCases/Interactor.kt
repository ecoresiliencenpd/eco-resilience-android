package edu.eco.resilience.npd.core.domain.useCases

import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver

interface Interactor<Result> {

    fun execute(observer: DisposableObserver<Result>)

    fun execute(): Observable<Result>
}