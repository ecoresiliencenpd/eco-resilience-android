package edu.eco.resilience.npd.presentation.ui.questions


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.domain.models.Question
import kotlinx.android.synthetic.main.item_question.view.*
import kotlin.properties.Delegates

class QuestionAdapter : RecyclerView.Adapter<QuestionAdapter.ViewHolder>() {

    internal var list: List<Question> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (Question) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_question, parent, false
            )
        )

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], clickListener)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(question: Question, clickListener: (Question) -> Unit) = with(itemView) {
            setOnClickListener { clickListener(question) }
            textQuestion.text = question.name
        }
    }
}