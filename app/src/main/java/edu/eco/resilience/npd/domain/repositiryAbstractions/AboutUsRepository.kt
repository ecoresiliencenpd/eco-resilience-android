package edu.eco.resilience.npd.domain.repositiryAbstractions

import edu.eco.resilience.npd.domain.models.AppDetail
import io.reactivex.Observable

interface AboutUsRepository {
    fun getAppDetail(): Observable<AppDetail>
}