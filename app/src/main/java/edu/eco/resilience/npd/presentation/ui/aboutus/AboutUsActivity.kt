package edu.eco.resilience.npd.presentation.ui.aboutus

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Rect
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.domain.models.SocialMedia
import edu.eco.resilience.npd.presentation.ui.aboutus.di.AboutUsComponent
import edu.eco.resilience.npd.presentation.ui.aboutus.di.AboutUsModule
import edu.eco.resilience.npd.presentation.ui.aboutus.di.DaggerAboutUsComponent
import edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel.AboutUsViewModel
import edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel.AboutUsViewModelFactory
import edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel.AboutUsViewState
import kotlinx.android.synthetic.main.activity_about_us.*
import javax.inject.Inject

class AboutUsActivity : BaseActivity() {

    private val aboutUsComponent: AboutUsComponent by lazy {
        DaggerAboutUsComponent.builder()
            .applicationComponent(applicationComponent)
            .aboutUsModule(AboutUsModule())
            .build()
    }

    @Inject
    lateinit var factory: AboutUsViewModelFactory

    private lateinit var viewModel: AboutUsViewModel

    override fun getLayoutResId(): Int = R.layout.activity_about_us

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.aboutUsComponent.inject(this)
        this.viewModel = ViewModelProviders.of(this, this.factory).get(AboutUsViewModel::class.java)
        this.viewModel.getAppDetail()
        this.observeViewState()
    }

    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is AboutUsViewState.AppDetailState -> {
                    this.appVersion.text = "Versión ${viewState.appdetail.versionApp}"
                    val listAboutUs = mutableListOf<AboutUs>()
                    listAboutUs.add(AboutUs(viewState.appdetail.appPolicyName, viewState.appdetail.appPolicyLink))
                    for (socialItem: SocialMedia in viewState.appdetail.socialMedia) {
                        listAboutUs.add(AboutUs(socialItem.name, socialItem.link))
                    }
                    this.aboutDetail.layoutManager = LinearLayoutManager(this)
                    this.aboutDetail.adapter = AboutUsAdapter(listAboutUs, this)
                    val itemDecoration = object : RecyclerView.ItemDecoration() {
                        override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
                        }
                    }
                    this.aboutDetail.addItemDecoration(itemDecoration)
                }
                is AboutUsViewState.ProgressIndicator -> {

                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
