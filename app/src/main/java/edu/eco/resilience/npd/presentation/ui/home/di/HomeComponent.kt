package edu.eco.resilience.npd.presentation.ui.home.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.home.HomeActivity
import edu.eco.resilience.npd.presentation.ui.login.LoginActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [HomeModule::class])
interface HomeComponent {

    fun inject(homeActivity: HomeActivity)
}