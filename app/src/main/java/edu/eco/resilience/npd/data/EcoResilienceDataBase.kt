package edu.eco.resilience.npd.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import edu.eco.resilience.npd.data.service.daos.SectionDao
import edu.eco.resilience.npd.data.service.daos.UserDao
import edu.eco.resilience.npd.domain.entities.databaseEntities.SectionEntity
import edu.eco.resilience.npd.domain.entities.databaseEntities.UserEntity
import edu.eco.resilience.npd.domain.mappers.ValuesConverter

@Database(
    entities = [
        UserEntity::class,
        SectionEntity::class,
        SectionEntity.SectionModuleEntity::class,
        SectionEntity.SectionModuleEntity.SectionThemeEntity::class,
        SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity::class,
        SectionEntity.SectionModuleEntity.SectionThemeEntity.SectionQuestionsEntity.SectionAnswerEntity::class

    ], version = 2, exportSchema = false
)
@TypeConverters(ValuesConverter::class)
abstract class EcoResilienceDataBase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getSectionDao(): SectionDao

}