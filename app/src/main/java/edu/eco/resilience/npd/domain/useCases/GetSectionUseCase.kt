package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.repositiryAbstractions.SectionRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetSectionUseCase
@Inject constructor(
    private val sectionRepository: SectionRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<Section, Int>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Int): Observable<Section> =
        sectionRepository.getSection(params)

}