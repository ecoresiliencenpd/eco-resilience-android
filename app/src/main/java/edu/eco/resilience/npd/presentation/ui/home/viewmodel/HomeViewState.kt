package edu.eco.resilience.npd.presentation.ui.home.viewmodel

import edu.eco.resilience.npd.domain.models.Section
import edu.eco.resilience.npd.domain.models.UserDetail

sealed class HomeViewState {

    data class User(val user: UserDetail? = null) : HomeViewState()

    data class SectionsState(val sections: List<Section>) : HomeViewState()

    data class ProgressIndicator(val visibility: Int) : HomeViewState()
}