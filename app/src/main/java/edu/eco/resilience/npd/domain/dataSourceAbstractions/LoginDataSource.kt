package edu.eco.resilience.npd.domain.dataSourceAbstractions

import io.reactivex.Observable
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.login.SaveDeviceRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.login.SaveDeviceObjectResponse
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.models.SaveDevice
import retrofit2.Response
import retrofit2.http.Body

interface LoginDataSource {

    fun login(loginRequest: LoginRequest): Observable<Login>

    fun saveDevices(request: SaveDeviceRequest, basic: String): Observable<SaveDevice>

}