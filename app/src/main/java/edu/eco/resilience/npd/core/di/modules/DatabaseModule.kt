package edu.eco.resilience.npd.core.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import javax.inject.Singleton

@Module
class DatabaseModule(private val databaseName: String) {

    @Provides
    @Singleton
    fun provideEcoResilienceDataBase(applicationContext: Context): EcoResilienceDataBase =
        Room.databaseBuilder(applicationContext, EcoResilienceDataBase::class.java, this.databaseName)
            .fallbackToDestructiveMigration()
            .build()
}