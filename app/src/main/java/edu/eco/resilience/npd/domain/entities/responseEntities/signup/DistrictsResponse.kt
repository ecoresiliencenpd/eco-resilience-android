package edu.eco.resilience.npd.domain.entities.responseEntities.signup

import com.google.gson.annotations.SerializedName

data class DistrictsResponse(

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("id")
    val id: Int? = null
)