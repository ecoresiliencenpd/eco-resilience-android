package edu.eco.resilience.npd.domain.entities.responseEntities.aboutus

import com.google.gson.annotations.SerializedName

data class AppDetailResponse(

    @field:SerializedName("contactSupport")
    val contactSupport: ContactSupport? = null,

    @field:SerializedName("socialMedia")
    val _socialMedia: List<SocialMediaItem>? = null,

    @field:SerializedName("version")
    val version: Version? = null,

    @field:SerializedName("appPolicy")
    val appPolicy: AppPolicy? = null
) {
    val socialMedia: List<SocialMediaItem>
        get() = this._socialMedia ?: emptyList()
}


data class AppPolicy(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("link")
    val link: String? = null
)

data class ContactSupport(

    @field:SerializedName("mail")
    val mail: String? = null,

    @field:SerializedName("name")
    val name: String? = null
)

data class SocialMediaItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("link")
    val link: String? = null
)

data class Version(

    @field:SerializedName("versionApp")
    val versionApp: String? = null
)