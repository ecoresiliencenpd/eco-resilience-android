package edu.eco.resilience.npd.presentation.ui.myprofile.di

import dagger.Component
import edu.eco.resilience.npd.core.di.components.ApplicationComponent
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.presentation.ui.myprofile.MyProfileActivity

@MainScope
@Component(dependencies = [ApplicationComponent::class], modules = [MyProfileModule::class])
interface MyProfileComponent {

    fun inject(myProfileActivity: MyProfileActivity)
}