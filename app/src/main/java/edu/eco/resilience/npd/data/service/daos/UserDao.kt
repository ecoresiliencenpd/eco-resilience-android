package edu.eco.resilience.npd.data.service.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import edu.eco.resilience.npd.domain.entities.databaseEntities.UserEntity
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserEntity)

    @Query(value = "DELETE FROM user")
    fun deleteUsers()

    @Query(value = "SELECT * FROM user")
    fun getUser(): Maybe<UserEntity>
}