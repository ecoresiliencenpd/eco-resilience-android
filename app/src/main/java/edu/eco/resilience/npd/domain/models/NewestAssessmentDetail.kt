package edu.eco.resilience.npd.domain.models

import android.os.Parcel
import android.os.Parcelable

data class NewestAssessmentDetail(
    val idChoice: Int,
    val question: String,
    val idAssessment: Int,
    val questionType: String,
    val active: Int,
    val idType: Int,
    val creationDate: Long,
    val choices: List<ChoicesItemNewest>,
    val value: Int,
    val idQuestion: Int,
    val order: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.createTypedArrayList(ChoicesItemNewest),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idChoice)
        parcel.writeString(question)
        parcel.writeInt(idAssessment)
        parcel.writeString(questionType)
        parcel.writeInt(active)
        parcel.writeInt(idType)
        parcel.writeLong(creationDate)
        parcel.writeTypedList(choices)
        parcel.writeInt(value)
        parcel.writeInt(idQuestion)
        parcel.writeInt(order)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewestAssessmentDetail> {
        override fun createFromParcel(parcel: Parcel): NewestAssessmentDetail {
            return NewestAssessmentDetail(parcel)
        }

        override fun newArray(size: Int): Array<NewestAssessmentDetail?> {
            return arrayOfNulls(size)
        }
    }
}

data class ChoicesItemNewest(
    val idBranch: Int,
    val branches: List<BranchesItem>
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.createTypedArrayList(BranchesItem)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idBranch)
        parcel.writeTypedList(branches)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ChoicesItemNewest> {
        override fun createFromParcel(parcel: Parcel): ChoicesItemNewest {
            return ChoicesItemNewest(parcel)
        }

        override fun newArray(size: Int): Array<ChoicesItemNewest?> {
            return arrayOfNulls(size)
        }
    }
}

data class BranchesItem(
    val idChoice: Int,
    val branchName: String,
    val active: Int,
    val creationDate: Long,
    val choice: String,
    var value: Int,
    val branch: Int,
    val idQuestion: Int,
    val order: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idChoice)
        parcel.writeString(branchName)
        parcel.writeInt(active)
        parcel.writeLong(creationDate)
        parcel.writeString(choice)
        parcel.writeInt(value)
        parcel.writeInt(branch)
        parcel.writeInt(idQuestion)
        parcel.writeInt(order)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BranchesItem> {
        override fun createFromParcel(parcel: Parcel): BranchesItem {
            return BranchesItem(parcel)
        }

        override fun newArray(size: Int): Array<BranchesItem?> {
            return arrayOfNulls(size)
        }
    }
}


