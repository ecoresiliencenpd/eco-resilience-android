package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.responseEntities.assessment.DetailApplicationAssessmentObjectResponse
import edu.eco.resilience.npd.domain.models.ChoicesItem
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.QuestionsItem
import javax.inject.Inject

class DetailApplicationAssessmentMapper
@Inject constructor() : BaseMapper<DetailApplicationAssessment, DetailApplicationAssessmentObjectResponse> {

    override fun mapFromApi(responseObject: DetailApplicationAssessmentObjectResponse): DetailApplicationAssessment =
        DetailApplicationAssessment(
            responseObject.idBranch ?: 0,
            responseObject.score ?: 0.0f,
            responseObject.questions!!.map { questionsItem ->
                QuestionsItem(
                    questionsItem.idChoice ?: 0,
                    questionsItem.question ?: "",
                    questionsItem.idAssessment ?: 0,
                    questionsItem.questionType ?: "",
                    questionsItem.active ?: 0,
                    questionsItem.idType ?: 0,
                    questionsItem.creationDate ?: 0L,
                    questionsItem.choices!!.map { choicesItem ->
                        ChoicesItem(
                            choicesItem.idChoice ?: 0,
                            choicesItem.active ?: 0,
                            choicesItem.creationDate ?: 0L,
                            choicesItem.choice ?: "",
                            choicesItem.value ?: 0,
                            choicesItem.branch ?: 0,
                            choicesItem.idQuestion ?: 0,
                            questionsItem.order ?: 0
                        )

                    },
                    questionsItem.value ?: 0,
                    questionsItem.idQuestion ?: 0,
                    questionsItem.order ?: 0
                )
            },
            responseObject.idApplication ?: 0,
            responseObject.creationDate ?: 0L
        )
}