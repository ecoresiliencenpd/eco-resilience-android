package edu.eco.resilience.npd.presentation.ui.signup

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseActivity
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.GetSchoolsRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.User
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.UserProfile
import edu.eco.resilience.npd.presentation.ui.adapters.SpinnerDropDownAdapter
import edu.eco.resilience.npd.presentation.ui.signup.di.DaggerSignUpComponent
import edu.eco.resilience.npd.presentation.ui.signup.di.SignUpComponent
import edu.eco.resilience.npd.presentation.ui.signup.di.SignUpModule
import edu.eco.resilience.npd.presentation.ui.signup.viewmodel.SignUpViewModel
import edu.eco.resilience.npd.presentation.ui.signup.viewmodel.SignUpViewModelFactory
import edu.eco.resilience.npd.presentation.ui.signup.viewmodel.SignUpViewState
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.content_sign_up.*
import org.jetbrains.anko.design.snackbar
import java.util.*
import javax.inject.Inject

class SignUpActivity : BaseActivity() {

    private val signUpComponent: SignUpComponent by lazy {
        DaggerSignUpComponent.builder()
            .applicationComponent(applicationComponent)
            .signUpModule(SignUpModule())
            .build()
    }

    @Inject
    lateinit var factory: SignUpViewModelFactory

    private val viewModel: SignUpViewModel by lazy {
        ViewModelProviders.of(this, this.factory).get(SignUpViewModel::class.java)
    }

    @Inject
    internal lateinit var sharedPreferencesManager: SharedPreferencesManager

    private var isPasswordConfirmed = false
    private var idProfile = 1
    private var idLenguage = 1
    private var idState = 1
    private var idDistricts = 1
    private var idSchool = 1
    private var idEducationLevel = 3
    private var initDialog = false

    override fun getLayoutResId(): Int = R.layout.activity_sign_up

    override fun initView(savedInstanceState: Bundle?) {
        super.initView(savedInstanceState)
        this.signUpComponent.inject(this)
        this.viewModel.getStates()
        this.observeViewState()
        this.initButtons()
        this.initInputs()
        this.initData()

    }

    private fun initData() {
        val profiles = Arrays.asList(*resources.getStringArray(R.array.spinner_profile))
        this.spinnerProfiles.adapter = SpinnerDropDownAdapter(this, profiles)
        this.spinnerProfiles.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                this@SignUpActivity.idProfile = (position + 1)
                this@SignUpActivity.contentLanguages.visibility = if (position == 2) View.VISIBLE else View.GONE
            }
        }

        val lenguages = Arrays.asList(*resources.getStringArray(R.array.spinner_language))
        this.spinnerLanguages.adapter = SpinnerDropDownAdapter(this, lenguages)
        this.spinnerLanguages.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                this@SignUpActivity.idLenguage = (position + 1)
                this@SignUpActivity.sharedPreferencesManager.saveInt(SharedPreferencesManager.LENGUAGE, (position + 1))
            }
        }
    }

    private fun initDialogUniversities(universities: MutableList<String>) {
        if (!this.initDialog) {
            val dialog = DialogUniversities.newInstanse(ArrayList(universities))
            dialog.show(supportFragmentManager, DialogUniversities::class.java.name)
            this.initDialog = true
        }
    }

    private fun observeViewState() {
        this.viewModel.viewState.observe(this, Observer { viewState ->
            when (viewState) {
                is SignUpViewState.States -> {
                    val states = mutableListOf<String>()
                    viewState.states.forEach { states.add(it.name ?: "") }
                    this.spinnerStates.adapter = SpinnerDropDownAdapter(this, states)
                    this.viewModel.getDistricts(idState.toString())
                }
                is SignUpViewState.Districts -> {
                    val districts = mutableListOf<String>()
                    val districtsId = mutableListOf<Int>()
                    viewState.districts.forEach {
                        districts.add(it.name ?: "")
                        districtsId.add(it.id ?: 0)
                    }
                    this.spinnerDistricts.adapter = SpinnerDropDownAdapter(this, districts)
                    this.spinnerDistricts.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            this@SignUpActivity.idDistricts = districtsId[position]
                            viewModel.getSchools(
                                GetSchoolsRequest(idState.toString(), this@SignUpActivity.idDistricts.toString(), "3")
                            )
                        }
                    }
                }
                is SignUpViewState.Schools -> {
                    val schools = mutableListOf<String>()
                    val idSchools = mutableListOf<Int>()
                    viewState.schools.forEach {
                        schools.add(it.name ?: "")
                        idSchools.add(it.id ?: 0)
                    }
                    this.initDialogUniversities(schools)
                    this.spinnerUniversity.adapter = SpinnerDropDownAdapter(this, schools)
                    this.spinnerUniversity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                        override fun onNothingSelected(parent: AdapterView<*>?) {}

                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            this@SignUpActivity.idSchool = idSchools[position]
                        }
                    }
                }
                is SignUpViewState.SignUp -> {
                    viewState.let {
                        Toast.makeText(this, viewState.signUp.message, Toast.LENGTH_LONG).show()
                        val intent = Intent()
                        intent.putExtra("email", this.email.text.toString())
                        intent.putExtra("password", this.password.text.toString())
                        this.setResult(Activity.RESULT_OK, intent)
                        this.finish()
                    }
                }
                is SignUpViewState.ProgressIndicator -> {
                    this.loading.visibility = viewState.visibility
                }
            }
        })
        this.viewModel.errorState.observe(this, Observer { message ->
            message?.let {
                content.snackbar(it).show()
            }
        })
    }

    private fun initButtons() {
        this.btnSignupCancel.setOnClickListener {
            this.onBackPressed()
        }
        this.btnSignup.setOnClickListener {
            if (this.validateFormSignUp()) {
                this.viewModel.signUp(
                    SignUpRequest(
                        UserProfile(1, this.idLenguage, this.idProfile),
                        User(
                            this.firstName.text.toString(),
                            this.lastName.text.toString(),
                            this.middleName.text.toString(),
                            this.idState,
                            this.idDistricts,
                            this.idSchool,
                            this.email.text.toString(),
                            this.password.text.toString(),
                            this.idEducationLevel
                        )
                    )
                )
            }

        }
    }

    private fun initInputs() {


        this.email.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.toString().filterNot { it.isWhitespace() }
        })
        this.password.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.toString().filterNot { it.isWhitespace() }
        })
        this.confirmPassword.filters = arrayOf(InputFilter { source, _, _, _, _, _ ->
            source.toString().filterNot { it.isWhitespace() }
        })

        this.confirmPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {

                if ((editable?.length ?: 0) > 4 && this@SignUpActivity.password.text.toString().length > 4) {
                    if (!confirmPassword.text.toString().equals(this@SignUpActivity.password.text.toString())) {
                        if (confirmPassword.text.toString().length <= password.text.toString().length ||
                            confirmPassword.text.toString().length >= password.text.toString().length
                        ) {
                            checkPasswordConfirm.visibility = View.VISIBLE
                            checkPasswordConfirm.setImageDrawable(getDrawable(R.drawable.ic_account_cross))
                            isPasswordConfirmed = false

                        } else if (confirmPassword.text.toString().isEmpty()) {
                            checkPasswordConfirm.visibility = View.GONE
                            isPasswordConfirmed = false
                        }

                    } else {
                        if (confirmPassword.text.toString().length == password.text.toString().length) {
                            checkPasswordConfirm.visibility = View.VISIBLE
                            checkPasswordConfirm.setImageDrawable(getDrawable(R.drawable.ic_account_check))
                            isPasswordConfirmed = true
                        } else {
                            checkPasswordConfirm.visibility = View.GONE
                            isPasswordConfirmed = false
                        }
                    }

                } else {
                    checkPasswordConfirm.visibility = View.GONE
                    isPasswordConfirmed = false
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    private fun validateFormSignUp(): Boolean {
        var valid = true

        if (firstName.text.toString().isEmpty() || firstName.text.toString().length < 3) {
            textFirstName.error = getString(R.string.error_name_length)
            valid = false
        } else {
            textFirstName.error = null
            textFirstName.isErrorEnabled = false
        }

        if (lastName.text.toString().isEmpty() || lastName.text.toString().length < 3) {
            textLastName.error = getString(R.string.error_name_length)
            valid = false
        } else {
            textLastName.error = null
            textLastName.isErrorEnabled = false
        }

        if (email.text.toString().isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            textEmail.error = getString(R.string.error_invalid_email)
            valid = false
        } else {
            textEmail.error = null
            textEmail.isErrorEnabled = false
        }

        if (password.text.toString().isEmpty() || password.text.toString().length < 4 || password.text.toString().length > 10) {
            textPassword.error = getString(R.string.error_password_length)
            valid = false
        } else {
            textPassword.error = null
            textPassword.isErrorEnabled = false
        }

        if (!isPasswordConfirmed) {
            textPassword.error = getString(R.string.error_password_match)
            valid = false
        } else {
            textPassword.error = null
            textPassword.isErrorEnabled = false
        }

        return valid
    }

}