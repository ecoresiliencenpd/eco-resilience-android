package edu.eco.resilience.npd.presentation.ui.aboutus.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.GetAppDetailUseCase
import javax.inject.Inject

class AboutUsViewModelFactory
@Inject constructor(
    private val getAppDetailUseCase: GetAppDetailUseCase

) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AboutUsViewModel(getAppDetailUseCase) as T
    }
}