package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class ThemesItem(

    @SerializedName("id_theme")
    val _idTheme: Int? = null,

    @SerializedName("name")
    val _name: String? = null,

    @SerializedName("questions")
    val _questions: List<QuestionsItem>? = null

) {
    val idTheme: Int
        get() = _idTheme ?: 0

    val name: String
        get() = this._name ?: ""

    val questions: List<QuestionsItem>
        get() = this._questions ?: emptyList()
}