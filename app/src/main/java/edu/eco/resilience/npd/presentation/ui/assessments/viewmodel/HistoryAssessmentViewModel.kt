package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.core.platform.SingleLiveEvent
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.useCases.GetHistoryAssessmentUseCase

class HistoryAssessmentViewModel(
    private val getHistoryAssessmentUseCase: GetHistoryAssessmentUseCase,
    private val sharedPreferencesManager: SharedPreferencesManager
) : BaseViewModel() {

    var viewState: MutableLiveData<HistoryAssessmentViewState> = MutableLiveData()

    var errorState: SingleLiveEvent<String> = SingleLiveEvent()

    fun getHistoryAssessment() {
        this.viewState.value = HistoryAssessmentViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            this.getHistoryAssessmentUseCase.execute(sharedPreferencesManager.getString(SharedPreferencesManager.USER_ID))
                .subscribe(::handleHistoryAssessment, ::handleError)
        )
    }

    private fun handleHistoryAssessment(historyAssessment: List<HistoryAssessment>) {
        this.viewState.value = HistoryAssessmentViewState.HistoryAssessmentState(historyAssessment)
        this.viewState.value = HistoryAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleError(it: Throwable) {
        this.errorState.value = it.message
        this.viewState.value = HistoryAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }
}