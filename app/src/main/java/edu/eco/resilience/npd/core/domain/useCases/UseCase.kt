package edu.eco.resilience.npd.core.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

abstract class UseCase<Result>(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) :
    Interactor<Result> {

    abstract fun createObservable(): Observable<Result>


    override fun execute(): Observable<Result> {
        return createObservable()
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.getScheduler())
    }

}