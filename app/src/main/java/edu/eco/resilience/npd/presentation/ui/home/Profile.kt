package edu.eco.resilience.npd.presentation.ui.home

data class Profile(val id: Int, val title: String, val image: Int)