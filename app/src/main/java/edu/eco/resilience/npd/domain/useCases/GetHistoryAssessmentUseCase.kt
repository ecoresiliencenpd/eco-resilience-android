package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.models.HistoryAssessment
import edu.eco.resilience.npd.domain.repositiryAbstractions.AssessmentRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetHistoryAssessmentUseCase
@Inject constructor(
    private val assessmentRepository: AssessmentRepository,
    private val sharedPreferencesManager: SharedPreferencesManager,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<List<HistoryAssessment>, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<List<HistoryAssessment>> =
        this.assessmentRepository.historyAssessment(
            params,
            sharedPreferencesManager.getString(SharedPreferencesManager.TOKEN_BASIC)
        )
}