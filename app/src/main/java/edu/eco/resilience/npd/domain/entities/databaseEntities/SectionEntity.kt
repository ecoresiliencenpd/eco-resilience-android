package edu.eco.resilience.npd.domain.entities.databaseEntities

import android.arch.persistence.room.*

@Entity(
    tableName = "section",
    indices = [Index("id_section"), Index("name")])
data class SectionEntity(
    @ColumnInfo(name = "id_section") val idSection: Int,
    @ColumnInfo(name = "name") val name: String
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "db_id")
    var dbId: Long? = null

    @Ignore
    var modules: List<SectionModuleEntity> = emptyList()

    @Entity(
        tableName = "section_modules",
        indices = [Index("section_db_id")],
        foreignKeys = [ForeignKey(entity = SectionEntity::class, parentColumns = ["db_id"], childColumns = ["section_db_id"], onDelete = ForeignKey.CASCADE)])
    data class SectionModuleEntity(
        @ColumnInfo(name = "id_module") val idModule: Int,
        @ColumnInfo(name = "name") val name: String
    ) {

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "db_id")
        var dbId: Long? = null

        @ColumnInfo(name = "section_db_id")
        var sectionDbId: Long? = null

        @Ignore
        var themes: List<SectionThemeEntity> = emptyList()

        @Entity(
            tableName = "section_themes",
            indices = [Index("module_db_id")],
            foreignKeys = [ForeignKey(entity = SectionModuleEntity::class, parentColumns = ["db_id"], childColumns = ["module_db_id"], onDelete = ForeignKey.CASCADE)])
        data class SectionThemeEntity(
            @ColumnInfo(name = "id_theme") val idTheme: Int,
            @ColumnInfo(name = "name") val name: String
        ) {
            @PrimaryKey(autoGenerate = true)
            @ColumnInfo(name = "db_id")
            var dbId: Long? = null

            @ColumnInfo(name = "module_db_id")
            var moduleDbId: Long? = null

            @Ignore
            var questions: List<SectionQuestionsEntity> = emptyList()

            @Entity(
                tableName = "section_questions",
                indices = [Index("theme_db_id")],
                foreignKeys = [ForeignKey(entity = SectionThemeEntity::class, parentColumns = ["db_id"], childColumns = ["theme_db_id"], onDelete = ForeignKey.CASCADE)])
            data class SectionQuestionsEntity(
                @ColumnInfo(name = "id_question") val idQuestion: Int,
                @ColumnInfo(name = "name") val name: String
            ) {

                @PrimaryKey(autoGenerate = true)
                @ColumnInfo(name = "db_id")
                var dbId: Long? = null

                @ColumnInfo(name = "theme_db_id")
                var themeDbId: Long? = null

                @Ignore
                var answers: List<SectionAnswerEntity> = emptyList()

                @Entity(
                    tableName = "section_answers",
                    indices = [Index("question_db_id")],
                    foreignKeys = [ForeignKey(entity = SectionQuestionsEntity::class, parentColumns = ["db_id"], childColumns = ["question_db_id"], onDelete = ForeignKey.CASCADE)])
                data class SectionAnswerEntity(
                    @ColumnInfo(name = "id_answer") val idAnswer: Int,
                    @ColumnInfo(name = "name") val name: String,
                    @ColumnInfo(name = "lastUpdate") val lastUpdate: String,
                    @ColumnInfo(name = "urlImage") val urlImage: String,
                    @ColumnInfo(name = "ulrDoc") val ulrDoc: String
                ) {
                    @PrimaryKey(autoGenerate = true)
                    @ColumnInfo(name = "db_id")
                    var dbId: Long? = null

                    @ColumnInfo(name = "question_db_id")
                    var questionDbId: Long? = null

                }
            }
        }
    }
}








