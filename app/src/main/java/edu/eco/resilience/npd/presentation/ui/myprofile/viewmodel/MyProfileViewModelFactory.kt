package edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.domain.useCases.GetUserUseCase
import javax.inject.Inject

class MyProfileViewModelFactory
@Inject constructor(
    private val getUserUseCase: GetUserUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyProfileViewModel(getUserUseCase) as T
    }
}