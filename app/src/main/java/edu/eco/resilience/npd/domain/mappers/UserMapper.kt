package edu.eco.resilience.npd.domain.mappers

import edu.eco.resilience.npd.core.domain.mappers.BaseMapper
import edu.eco.resilience.npd.domain.entities.databaseEntities.UserEntity
import edu.eco.resilience.npd.domain.models.UserDetail
import javax.inject.Inject

class UserMapper @Inject constructor() : BaseMapper<UserEntity, UserDetail> {

    override fun mapFromApi(responseObject: UserDetail): UserEntity =
        UserEntity(
            responseObject.idUser,
            responseObject.mail,
            responseObject.firstName,
            responseObject.lastName,
            responseObject.middleName,
            responseObject.idEducationLevel,
            responseObject.educationLevel,
            responseObject.idLanguage,
            responseObject.language,
            responseObject.idProfile,
            responseObject.profile,
            responseObject.idState,
            responseObject.state,
            responseObject.idDistrict,
            responseObject.district,
            responseObject.idSchool,
            responseObject.school,
            responseObject.creationDate
        )
}