package edu.eco.resilience.npd.presentation.ui.signup.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.dataSourceImp.api.SignUpDataSourceImp
import edu.eco.resilience.npd.data.repositoryImp.SignUpRepositoryImpl
import edu.eco.resilience.npd.data.service.api.SignUpService
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SignUpDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import retrofit2.Retrofit

@Module
class SignUpModule {

    @Provides
    @MainScope
    fun provideSignUpService(retrofit: Retrofit): SignUpService = retrofit.create(SignUpService::class.java)

    @Provides
    @MainScope
    fun provideSignUpDataSource(signUpDataSourceImp: SignUpDataSourceImp): SignUpDataSource = signUpDataSourceImp

    @Provides
    @MainScope
    fun provideSignUpRepository(signUpRepositoryImpl: SignUpRepositoryImpl): SignUpRepository = signUpRepositoryImpl

}