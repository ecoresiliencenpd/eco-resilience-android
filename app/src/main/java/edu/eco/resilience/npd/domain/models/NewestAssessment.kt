package edu.eco.resilience.npd.domain.models

data class NewestAssessment(
    val daysOfLife: Int,
    val passScore: String,
    val description: String,
    val active: Boolean,
    val idLanguage: Int,
    val idParent: Int,
    val multipleApplications: Int,
    val repeatable: Int,
    val lastUpdate: String,
    val name: String,
    val publicationDate: String,
    val id: Int,
    val idProfile: Int
)
