package edu.eco.resilience.npd.presentation.utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Base64
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import android.hardware.usb.UsbDevice.getDeviceId
import android.content.Context.TELEPHONY_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.telephony.TelephonyManager
import android.hardware.usb.UsbDevice.getDeviceId
import android.os.Build
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import android.os.Build.MANUFACTURER


fun encodeString(s: String): String {
    var data = ByteArray(0)

    try {
        data = s.toByteArray(charset("UTF-8"))

    } catch (e: UnsupportedEncodingException) {
        e.printStackTrace()
    } finally {

        return Base64.encodeToString(data, Base64.NO_WRAP)

    }
}

fun parseDate(dateString: String, datePattern: String) =
    SimpleDateFormat(datePattern, Locale.getDefault()).parse(dateString)

fun getDateString(date: Date): String {
    var tmp = SimpleDateFormat("MMM d", Locale.US)

    var str = tmp.format(date)
    str = str.substring(0, 1).toUpperCase() + str.substring(1)

    if (date.date in 11..13)
        str += "th, "
    else {
        str += when {
            str.endsWith("1") -> "st, "
            str.endsWith("2") -> "nd, "
            str.endsWith("3") -> "rd, "
            else -> "th, "
        }
    }
    tmp = SimpleDateFormat("yyyy")
    str += tmp.format(date)
    return str
}

fun getUUID(): String {
    return UUID.randomUUID().toString()
}


fun getDeviceName(): String {
    val manufacturer = MANUFACTURER
    val model = Build.MODEL
    return if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
        capitalize(model)
    } else {
        capitalize(manufacturer) + " " + model
    }
}


private fun capitalize(s: String?): String {
    if (s == null || s.length == 0) {
        return ""
    }
    val first = s[0]
    return if (Character.isUpperCase(first)) {
        s
    } else {
        Character.toUpperCase(first) + s.substring(1)
    }
}