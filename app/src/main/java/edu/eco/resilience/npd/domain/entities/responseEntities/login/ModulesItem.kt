package edu.eco.resilience.npd.domain.entities.responseEntities.login

import com.google.gson.annotations.SerializedName

data class ModulesItem(

    @SerializedName("themes")
    val _themes: List<ThemesItem>? = null,

    @SerializedName("id_module")
    val _idModule: Int? = null,

    @SerializedName("name")
    val _name: String? = null
) {
    val themes: List<ThemesItem>
        get() = this._themes ?: emptyList()

    val idModule: Int
        get() = this._idModule ?: 0

    val name: String
        get() = _name ?: ""
}