package edu.eco.resilience.npd.presentation.ui.assessments


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.core.platform.BaseFragment
import edu.eco.resilience.npd.domain.models.QuestionsItem
import kotlinx.android.synthetic.main.fragment_detail_application_assessment_question.*


private const val ARG_QUESTION = "arg_question"

class DetailApplicationAssessmentQuestionFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_detail_application_assessment_question

    private var question: QuestionsItem? = null

    companion object {

        @JvmStatic
        fun newInstance(question: QuestionsItem?) =
            DetailApplicationAssessmentQuestionFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_QUESTION, question)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.arguments?.let {
            this.question = it.getParcelable(ARG_QUESTION)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.numberQuestion.text = "Question ${question?.idQuestion.toString()}"
        this.textQuestion.text = question?.question
        this.sb.setIntervals(getIntervals())
        this.sb.progress = question?.value ?: 0

        this.sb.enable(false)

        this.sb.setOnTouchListener(object : View.OnTouchListener {
            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                return false
            }
        })
    }

    private fun getIntervals(): MutableList<String> = mutableListOf(
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
    )


}
