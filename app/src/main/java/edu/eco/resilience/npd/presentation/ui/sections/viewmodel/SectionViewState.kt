package edu.eco.resilience.npd.presentation.ui.sections.viewmodel

import edu.eco.resilience.npd.presentation.ui.sections.SectionModel

sealed class SectionViewState {

    data class SectionState(val section: List<SectionModel>) : SectionViewState()

    data class ProgressIndicator(val visibility: Int) : SectionViewState()
}