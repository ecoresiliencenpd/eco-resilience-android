package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.view.View
import edu.eco.resilience.npd.core.platform.BaseViewModel
import edu.eco.resilience.npd.core.platform.SingleLiveEvent
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.models.DetailApplicationAssessment
import edu.eco.resilience.npd.domain.models.Evaluation
import edu.eco.resilience.npd.domain.models.NewestAssessmentDetail
import edu.eco.resilience.npd.domain.useCases.EvaluationUseCase
import edu.eco.resilience.npd.domain.useCases.GetDetailApplicationAssessmentUseCase
import edu.eco.resilience.npd.domain.useCases.GetNewestAssessmentDetailUseCase

class DetailApplicationAssessmentViewModel(
    private val getDetailApplicationAssessmentUseCase: GetDetailApplicationAssessmentUseCase,
    private val getNewestAssessmentDetailUseCase: GetNewestAssessmentDetailUseCase,
    private val evaluationUseCase: EvaluationUseCase
) : BaseViewModel() {

    var viewState: MutableLiveData<DetailApplicationAssessmentViewState> = MutableLiveData()

    var errorState: SingleLiveEvent<String> = SingleLiveEvent()

    fun evaluation(evaluationObjectRequest: EvaluationObjectRequest) {
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            this.evaluationUseCase.execute(evaluationObjectRequest)
                .subscribe(::handleEvaluation, ::handleError)
        )
    }

    fun getDetailApplicationAssessment(idApplication: String) {
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            this.getDetailApplicationAssessmentUseCase.execute(idApplication)
                .subscribe(::handleDetailApplicationAssessment, ::handleError)
        )
    }

    fun getNewestAssessmentDetail(idAssessment: String) {
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.VISIBLE)
        this.addDisposable(
            this.getNewestAssessmentDetailUseCase.execute(idAssessment)
                .subscribe(::handleNewestAssessmentDetail, ::handleError)
        )
    }

    private fun handleEvaluation(evaluation: Evaluation) {
        this.viewState.value = DetailApplicationAssessmentViewState.EvaluationState(evaluation)
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleDetailApplicationAssessment(detailApplicationAssessment: DetailApplicationAssessment) {
        this.viewState.value =
            DetailApplicationAssessmentViewState.DetailApplicationAssessmentState(detailApplicationAssessment)
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleNewestAssessmentDetail(newestAssessmentDetail: List<NewestAssessmentDetail>) {
        this.viewState.value =
            DetailApplicationAssessmentViewState.NewestAsessmentDetailState(newestAssessmentDetail)
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }

    private fun handleError(it: Throwable) {
        this.errorState.value = it.message
        this.viewState.value = DetailApplicationAssessmentViewState.ProgressIndicator(View.INVISIBLE)
    }
}