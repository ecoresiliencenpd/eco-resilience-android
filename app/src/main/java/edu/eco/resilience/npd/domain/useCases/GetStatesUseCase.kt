package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.entities.requestEntities.login.LoginRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.StateResponse
import edu.eco.resilience.npd.domain.models.Login
import edu.eco.resilience.npd.domain.repositiryAbstractions.LoginRepository
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetStatesUseCase
@Inject constructor(
    private val signUpRepository: SignUpRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<List<StateResponse>, String>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: String): Observable<List<StateResponse>> =
        this.signUpRepository.getStates()


}