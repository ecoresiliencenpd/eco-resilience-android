package edu.eco.resilience.npd.presentation.ui.signup

import android.graphics.Point
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import edu.eco.resilience.npd.R
import kotlinx.android.synthetic.main.dialog_universities.*

class DialogUniversities : DialogFragment() {

    private var universities: ArrayList<String> = arrayListOf()

    companion object {
        fun newInstanse(universities: ArrayList<String>): DialogUniversities {
            val bundle = Bundle()
            bundle.putStringArrayList("ARG_UNIVERSITIES", universities)
            val dialogUniversities = DialogUniversities()
            dialogUniversities.arguments = bundle
            return dialogUniversities
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.universities = arguments?.getStringArrayList("ARG_UNIVERSITIES") ?: arrayListOf()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_universities, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initView()
    }

    override fun onResume() {
        val size = Point()
        val display = dialog.window.windowManager.defaultDisplay
        display.getSize(size)
        dialog.window.setLayout((size.x * 0.90).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        dialog.window.setGravity(Gravity.CENTER)
        super.onResume()

    }

    private fun initView() {
        this.okAlertButton.setOnClickListener { dismiss() }
        this.listColleges.layoutManager = LinearLayoutManager(context)
        this.listColleges.itemAnimator = DefaultItemAnimator()
        this.listColleges.adapter = UniversitiesAdapter(universities)
        val itemDecoration = object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
            }
        }
        this.listColleges.addItemDecoration(itemDecoration)
    }

}