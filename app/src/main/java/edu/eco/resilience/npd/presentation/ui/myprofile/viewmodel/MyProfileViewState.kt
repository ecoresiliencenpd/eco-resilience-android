package edu.eco.resilience.npd.presentation.ui.myprofile.viewmodel

import edu.eco.resilience.npd.domain.models.UserDetail

sealed class MyProfileViewState {

    data class User(val user: UserDetail? = null) : MyProfileViewState()

    data class ProgressIndicator(val visibility: Int) : MyProfileViewState()
}