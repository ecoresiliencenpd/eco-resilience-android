package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.entities.requestEntities.assessments.EvaluationObjectRequest
import edu.eco.resilience.npd.domain.models.Evaluation
import edu.eco.resilience.npd.domain.repositiryAbstractions.AssessmentRepository
import io.reactivex.Observable
import javax.inject.Inject

class EvaluationUseCase
@Inject constructor(
    private val assessmentRespository: AssessmentRepository,
    private val sharedPreferencesManager: SharedPreferencesManager,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<Evaluation, EvaluationObjectRequest>(threadExecutor, postExecutionThread) {
    override fun createObservable(params: EvaluationObjectRequest): Observable<Evaluation> =
        this.assessmentRespository.evaluation(
            params,
            sharedPreferencesManager.getString(SharedPreferencesManager.TOKEN_BASIC)
        )
}