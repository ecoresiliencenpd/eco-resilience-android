package edu.eco.resilience.npd.domain.entities.responseEntities.assessment

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

data class NewestAssessmentObjectResponse(

    @field:SerializedName("days_of_life")
    val daysOfLife: Int? = null,

    @field:SerializedName("pass_score")
    val passScore: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("active")
    val active: Boolean? = null,

    @field:SerializedName("id_language")
    val idLanguage: Int? = null,

    @field:SerializedName("id_parent")
    val idParent: Int? = null,

    @field:SerializedName("multiple_applications")
    val multipleApplications: Int? = null,

    @field:SerializedName("repeatable")
    val repeatable: Int? = null,

    @field:SerializedName("last_update")
    val lastUpdate: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("publication_date")
    val publicationDate: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_profile")
    val idProfile: Int? = null
)