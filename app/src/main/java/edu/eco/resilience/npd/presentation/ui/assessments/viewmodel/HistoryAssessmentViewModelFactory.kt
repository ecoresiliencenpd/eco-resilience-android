package edu.eco.resilience.npd.presentation.ui.assessments.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import edu.eco.resilience.npd.data.SharedPreferencesManager
import edu.eco.resilience.npd.domain.useCases.GetHistoryAssessmentUseCase
import javax.inject.Inject

class HistoryAssessmentViewModelFactory
@Inject constructor(
    private val getHistoryAssessmentUseCase: GetHistoryAssessmentUseCase,
    private val sharedPreferencesManager: SharedPreferencesManager
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(GetHistoryAssessmentUseCase::class.java, SharedPreferencesManager::class.java)
            .newInstance(getHistoryAssessmentUseCase, sharedPreferencesManager)
    }

}