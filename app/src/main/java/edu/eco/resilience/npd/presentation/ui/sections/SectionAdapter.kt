package edu.eco.resilience.npd.presentation.ui.sections

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.eco.resilience.npd.R
import edu.eco.resilience.npd.domain.models.Theme
import edu.eco.resilience.npd.presentation.ui.home.Profile
import edu.eco.resilience.npd.presentation.utils.SectionsAdapter
import kotlinx.android.synthetic.main.item_theme.view.*

class SectionAdapter(val context: Context) :
    SectionsAdapter<SectionModel, Theme, SectionAdapter.SectionViewHolder, SectionAdapter.ItemViewHolder>() {

    companion object {
        const val TYPE_HEADER = 10
        const val TYPE_ITEM = 11
    }

    private val inflater: LayoutInflater by lazy { LayoutInflater.from(this.context) }

    internal var clickListener: (Theme) -> Unit = { _ -> }

    override fun onBindHeader(headerViewHolder: SectionViewHolder, header: SectionModel) {
        headerViewHolder.textHeader.text = header.module
    }

    override fun onBindItem(itemViewHolder: ItemViewHolder, header: SectionModel, item: Theme) {
        itemViewHolder.bindItems(item, clickListener)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SectionsAdapter.SectionViewHolder<SectionViewHolder, ItemViewHolder> {
        return if (viewType == TYPE_HEADER) {
            SectionViewHolder(SectionViewHolder(this.inflater.inflate(R.layout.item_section_header, parent, false)))
        } else {
            SectionViewHolder(ItemViewHolder(this.inflater.inflate(R.layout.item_theme, parent, false)))
        }
    }


    class SectionViewHolder(itemView: View) : SectionsAdapter.HeaderViewHolder(itemView) {
        val textHeader: AppCompatTextView = this.itemView.findViewById(R.id.textHeader)
    }

    class ItemViewHolder(itemView: View) : SectionsAdapter.ItemViewHolder(itemView) {

        fun bindItems(theme: Theme, clickListener: (Theme) -> Unit) = with(itemView) {
            setOnClickListener { clickListener(theme) }
            moduleName.text = theme.name
        }

    }


}