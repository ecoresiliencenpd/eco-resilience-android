package edu.eco.resilience.npd.presentation.ui.home.di

import dagger.Module
import dagger.Provides
import edu.eco.resilience.npd.core.di.scopes.MainScope
import edu.eco.resilience.npd.data.EcoResilienceDataBase
import edu.eco.resilience.npd.data.dataSourceImp.database.SectionDatabaseSourceImpl
import edu.eco.resilience.npd.data.dataSourceImp.database.UserDatabaseSourceImpl
import edu.eco.resilience.npd.data.repositoryImp.SectionRepositoryImpl
import edu.eco.resilience.npd.data.repositoryImp.UserRepositoryImpl
import edu.eco.resilience.npd.data.service.daos.SectionDao
import edu.eco.resilience.npd.data.service.daos.UserDao
import edu.eco.resilience.npd.domain.dataSourceAbstractions.SectionDataSource
import edu.eco.resilience.npd.domain.dataSourceAbstractions.UserDataSource
import edu.eco.resilience.npd.domain.repositiryAbstractions.SectionRepository
import edu.eco.resilience.npd.domain.repositiryAbstractions.UserRepository

@Module
class HomeModule {

    @Provides
    @MainScope
    fun provideUserDao(ecoResilienceDataBase: EcoResilienceDataBase): UserDao = ecoResilienceDataBase.getUserDao()

    @Provides
    @MainScope
    fun provideUserDatabaseSource(userDatabaseSourceImpl: UserDatabaseSourceImpl): UserDataSource =
        userDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideUserRepository(userRepositoryImpl: UserRepositoryImpl): UserRepository = userRepositoryImpl

    @Provides
    @MainScope
    fun provideSectionDao(ecoResilienceDataBase: EcoResilienceDataBase): SectionDao =
        ecoResilienceDataBase.getSectionDao()

    @Provides
    @MainScope
    fun provideSectionDatabaseSource(sectionDatabaseSourceImpl: SectionDatabaseSourceImpl): SectionDataSource =
        sectionDatabaseSourceImpl

    @Provides
    @MainScope
    fun provideSectionRepository(sectionRepositoryImpl: SectionRepositoryImpl): SectionRepository =
        sectionRepositoryImpl

}