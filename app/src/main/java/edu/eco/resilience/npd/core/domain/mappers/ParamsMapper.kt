package edu.eco.resilience.npd.core.domain.mappers

interface ParamsMapper<Params, Model, Api, FromDatabase, ToDatabase> {

    fun mapFromApi(params: Params, responseObject: Api): Model

    fun mapFromDatabase(entity: FromDatabase): Model

    fun mapToDatabase(params: Params, model: Model): ToDatabase

    fun mapToDatabase(params: Params, models: List<Model>): List<ToDatabase>
}