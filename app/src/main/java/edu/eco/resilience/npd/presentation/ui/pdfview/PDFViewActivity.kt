package edu.eco.resilience.npd.presentation.ui.pdfview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import edu.eco.resilience.npd.R
import kotlinx.android.synthetic.main.activity_pdfview.*
import kotlinx.android.synthetic.main.content_pdfview.*

class PDFViewActivity : AppCompatActivity() {

    lateinit var url: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdfview)
        this.url = intent.getStringExtra("URL")
        this.toolbar.title = url.substring(url.lastIndexOf("/") + 1, url.length).replace("%20", "_")
        this.setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.showWebView()
    }

    private fun showWebView() {
        webview.settings.javaScriptEnabled = true
        webview.settings.builtInZoomControls = false
        webview.settings.setSupportZoom(true)
        webview.loadUrl("https://docs.google.com/viewer?url=$url&embedded=true")

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
