package edu.eco.resilience.npd.domain.useCases

import edu.eco.resilience.npd.core.domain.executors.PostExecutionThread
import edu.eco.resilience.npd.core.domain.executors.ThreadExecutor
import edu.eco.resilience.npd.core.domain.useCases.ParamsUseCase
import edu.eco.resilience.npd.domain.entities.requestEntities.signup.SignUpRequest
import edu.eco.resilience.npd.domain.entities.responseEntities.signup.SignUpResponse
import edu.eco.resilience.npd.domain.repositiryAbstractions.SignUpRepository
import io.reactivex.Observable
import javax.inject.Inject

class SignUpUseCase
@Inject constructor(
    private val signUpRepository: SignUpRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : ParamsUseCase<SignUpResponse, SignUpRequest>(threadExecutor, postExecutionThread) {


    override fun createObservable(params: SignUpRequest): Observable<SignUpResponse> =
        this.signUpRepository.signUp(params)


}